PROGRAM CP3 (Input, Output, StatFile, LogFile);
{$S-,R-,D-,B-,A+,L-,I-,V-,E+}
Uses DOS, CRT;
(*                                                                  *)
(********************************************************************)
(*                                                                  *)
(*                             H A L                                *)
(*                                                                  *)
(*             Heuristic Associative Linear-algorithm               *)
(*             -         -           -                              *)
(*                                                                  *)
(*==================================================================*)
(*                                                                  *)
(*                  C H E S S   P R O G R A M   3                   *)
(*                  -           -               -                   *)
(********************************************************************)
(*                                                                  *)
(*------------------------------------------------------------------*)
(*                                                                  *)
(* Author:  Stephen F. Wheeler, Ph.D.   A.I. Computer Scientist     *)
(*                                                                  *)
(* Language:  Turbo Pascal v6.0                                     *)
(*                                                                  *)
(*------------------------------------------------------------------*)
(*                                                                  *)
(*                                                                  *)
(* Date Written:                September 8, 1990                   *)
(*                                                                  *)
(* Date of Last Revision:       May 25, 2010                        *)
(*                                                                  *)
(* Revisions in This Version:                                       *)
(*                                                                  *)
(* 1)  In Move_Selector Procedure set -INF and +INF for             *) 
(*     re-searches for failed-low and failed-high when Level is < 3.*)
(*                                                       05/15/10   *)
(*                                                                  *)
(* 2)  In EVAL Procedure decremented level variable at P^.LX by 1   *)
(*     and stored it in an LX variable when level of play was < 3.  *)
(*     This corrected the missed checkmate declaration when playing *)
(*     at skill levels 1 and 2.                                     *)                          
(*                                                       05/25/10   *)
(*                                                                  *)
(*                                                                  *)
(*------------------------------------------------------------------*)
(*                  C P 3 V 8 . 9 . 9 . 7 . 2                       *)
(*------------------------------------------------------------------*)
(*                                                                  *)
(*==================================================================*)
(*                                                                  *)
(*        C O M P U T E R   C H E S S   A U T O M A T O N           *)
(*                                                                  *)
(*    THIS PROGRAM UTILIZES THE ITERATIVE-DEEPENING NEGAMAX         *)
(*    FAILSOFT ALPHA-BETA SEARCH ALGORITHM WITH QUIESCENCE, THE     *)
(*    KILLER HEURISTIC AND REFUTATION TABLES.                       *)
(*                                                                  *)
(*==================================================================*)
(*                                                                  *)
(********************************************************************)
(*                                                                  *)
(*              P R O G R A M   D E C L A R A T I V E S             *)
(*                                                                  *)
(*                       B E G I N    H E R E                       *)
(*                                                                  *)
(********************************************************************)




(********************************************************************)
(*              P R O G R A M   C O N S T A N T S                   *)
(********************************************************************)



CONST


   Version = '8.9.9.7.2';

   Pawn = 128;        (* Pawn Heuristic Weight *)
   White = 1;         (* Side Index for White *)
   Black = 2;         (* Side Index for Black *)
   Default_Level = 2;    (* Default Skill Level *)
   FW_Limit = 3;      (* Full Width Search Limit *)
   QS_Limit = 8;      (* Quiescent Search Depth Limit *)
   MaxSearch = 20;    (* Maximum Non_Tournament Search Limit *)
   MaxDepth = 30;     (* Maximum Tree Size *)
   MaxBreadth = 100;  (* Maximum Number of Elements in each Ply of Tree *)
   Check_Extension = 2;  (* Search Extension Limit for In_Check Condition *)
   Killers = 4;       (* Number of Killers at each Level of Active List *)
   Killer_Value = 2;  (* Killer Initiation Value *)
   Killer_Augment = 3;   (* Killer Statistical Augmentation Value *)
   Killer_Decay = 1;     (* Killer Statistical Decay Value *)
   Decay_Interval = 2;   (* Decay Interval for a-b Window Fail Control *)
   INF = MAXINT;         (* Pseudo Infinity *)
   High_Value = 19999;   (* High Value *)
   Very_High_Value = 25555;  (* Short Pseudo Infinity *)
   VHV = Very_High_Value;    (* Short Pseudo Infinity *)
   Marker1 = 21111;          (* Pseudo Infinity Marker Value *)
   Marker2 = 22222;          (* Pseudo Infinity Marker Value *)
   Marker3 = 23333;          (* Pseudo Infinity Marker Value *)
   Marker4 = 24444;          (* Pseudo Infinity Marker Value *)
   Marker5 = 25555;          (* Pseudo Infinity Marker Value *)
   Marker6 = 26666;          (* Pseudo Infinity Marker Value *)
   Marker7 = 27777;          (* Pseudo Infinity Marker Value *)
   Marker8 = 28888;          (* Pseudo Infinity Marker Value *)
   Marker9 = 29999;          (* Pseudo Infinity Marker Value *)
   MaxLines = 56;            (* Maximum number of lines per page *)
   Alarm = #7;               (* Audible alarm code *)
   LineFeed = #10;           (* Line feed printer control code *)
   FormFeed = #12;           (* Form feed printer control code *)
   Retry_Limit = 2;          (* I/O Error Retry Limit *)

   Printer = 'PRN';          (* System logical print device *)

   DrvPth = 'CHESS\';        (* System logical drive and path *)

   LogDrv = 'LOG.CP3';       (* Game move history file *)

   StatDrv = 'STAT.CP3';     (* Game statistics file *)

   ConfigDrv = 'CONFIG.CP3'; (* System configuration file *)


(********************************************************************)
(*              P R O G R A M   D A T A   T Y P E S                 *)
(********************************************************************)



TYPE


   (*  AUTOMATON STRING DATA TYPES  *)

   STR80 = STRING [80];
   STR40 = STRING [40];
   STR20 = STRING [20];
   STR11 = STRING [11];
   STR10 = STRING [10];
   STR9 = STRING [9];
   STR6 = STRING [6];
   STR5 = STRING [5];
   STR4 = STRING [4];
   STR3 = STRING [3];
   STR2 = STRING [2];
   STR1 = STRING [1];

   (*  AUTOMATON RANGE DATA TYPES  *)

   X_Vector = 'A'..'H';
   Y_Vector = 1..8;
   Depth = 1..MaxDepth;
   Breadth = 1..MaxBreadth;
   Side_Index = 1..2;
   Man_Index = 1..16;
   Man_Type = 1..6;
   Vector = 1..8;

   (*  AUTOMATON SET DATA TYPES  *)

   LetterSet = SET OF X_Vector;
   NumberSet = SET OF '1'..'8';
   AlphaSet = SET OF 'A'..'Z';
   NumeralSet = SET OF '0'..'9';
   CheckMateSet = SET OF 1 .. 1;
   StaleMateSet = SET OF 2 .. 2;
   CheckSet = SET OF 3 .. 3;
   JeopardySet = SET OF 1 .. 2;

   (*  AUTOMATON RECORD DATA TYPES  *)

   Square = RECORD
               File_Cell : INTEGER;
               Rank_Cell : INTEGER
            END;

   Node_Cell = RECORD
                  From_Cell : SQUARE;
                  To_Cell : SQUARE
               END;

   Position = RECORD
                 Move_Cell : NODE_CELL;
                 Mode_Cell : CHAR;
                 Score_Cell : INTEGER
              END;

   Table_Index = RECORD
                    Side_Cell : INTEGER;
                    Man_Cell : INTEGER
                 END;

   Board_Cell = RECORD
                   Piece_Cell : TABLE_INDEX;
                   Attack_Cell : CHAR
                END;

   TCA = RECORD
            D : INTEGER;
            T : INTEGER;
            M : INTEGER
         END;

   TCB = RECORD
            CP : INTEGER;
            DV : INTEGER;
            CK : INTEGER;
            AV : INTEGER;
            CS : INTEGER;
            PP : INTEGER;
            BP : INTEGER;
            SP : INTEGER;
            KS : INTEGER;
            PS : INTEGER
         END;

   Level_Cells = RECORD
                    To_Position : TABLE_INDEX;
                    LPM : TABLE_INDEX;
                    Move_Cnt : INTEGER;
                    TC2 : TCA;
                    TD2 : TCB;
			  LXS : INTEGER;        	
                    DEV : INTEGER;
                    DEV2 : INTEGER;
                    DEV3 : INTEGER;
                    DEV4 : INTEGER;
                    DEV5 : INTEGER;
                    Move_Ctrl : CHAR;
                    MSC : CHAR;
                    MPS : CHAR;
                    OSC : CHAR;
                    OPS : CHAR;
                    ENP : CHAR
                 END;

   Piece_Cells = RECORD
                    Position_Cell : SQUARE;
                    Move_Pattern : MAN_TYPE;
                    Status_Cell : CHAR;
                    Pseudo_Status : CHAR;
                    Move_Cntrl : CHAR;
                    Defending : INTEGER;
                    Threats : INTEGER;
                    Mobility : INTEGER;
                    Development : INTEGER;
                    Moves : INTEGER
                 END;

   Move_Vectors = RECORD
                     Inc1 : INTEGER;  (*  Y Increment for Piece  *)
                     Inc2 : INTEGER;  (*  X Increment for Piece  *)
                     Lim1 : INTEGER;  (*  Y Limit for Piece  *)
                     Lim2 : INTEGER   (*  X Limit for Piece  *)
                  END;

   Accumulators = RECORD
                     Gened : REAL;  (* Branches Generated at this Level *)
                     Cutoff : REAL  (* a-b Cutoffs at this Level *)
                  END;

   DateRcdType = RECORD
                    Mo : INTEGER;
                    S1 : CHAR;
                    Dy : INTEGER;
                    S2 : CHAR;
                    Yr : INTEGER;
                    DOW : INTEGER;
                    DAY : STRING[10]
                 END;

   TimeRcdType = RECORD
                    Hr : INTEGER;
                    C1 : CHAR;
                    Mn : INTEGER;
                    C2 : CHAR;
                    Sc : INTEGER;
                    C3 : CHAR;
                    Hn : INTEGER;
                    TSc : REAL
                 END;

   StatRcdType = RECORD
                    CompletionCode : 0..7;  (* WHERE: 0 = Interrupted   *)
                                            (*        1 = CheckMate     *)
                                            (*        2 = StaleMate     *)
                                            (*        3 = Resign        *)
                                            (*        4 = Draw          *)
                                            (*        5 = Terminated    *)
                                            (*        6 = Continue Game *)
                                            (*        7 = New Game      *)
                    OpntName : Str40;
                    OpntSide : INTEGER;
                    LogDate : DateRcdType;
                    NrMoves : INTEGER;
                    StartTime : TimeRcdType;
                    StopTime : TimeRcdType;
                    LevelInd : INTEGER;
                    OpntClock : REAL;
                    MchnClock : REAL;
                    ElapsedTime : REAL;
                    AllocTime : REAL;
                    AllocMoves : INTEGER;
                    Score : INTEGER;
                    TournFlag : BOOLEAN;
                    TimeFlag : BOOLEAN;
                    HardCopyFlag : BOOLEAN;
                    RptFlag : BOOLEAN;
                    HideFlag : BOOLEAN;
                    ChronFlag : BOOLEAN;
                    ShowFlag : BOOLEAN;
                    PlotFlag : BOOLEAN
                  END;

   LogRcdType = RECORD
                   MvCntr : INTEGER;
                   Move : Position;
                   MchnMvTime : TimeRcdType;
                   OpntMvTime : TimeRcdType;
                   NodesScored : REAL;
                   CutOffs : REAL;
                   NodesPerSec : REAL
                END;

   (*  AUTOMATON ARRAY DATA TYPES  *)

   RLList = ARRAY [Breadth, Depth] OF Position;  (*  Refutation List  *)
   PCList = ARRAY [Depth, Depth] OF Position; (* Principal Continuation *)
   Squares = ARRAY [Vector, Vector] OF BOARD_CELL;
   New_Ply = ARRAY [Breadth] OF POSITION;
   MvLstIndx = ARRAY [Breadth] OF INTEGER;
                                         (*  Piece Table  *)
   Piece_Data = ARRAY [Side_Index, Man_Index] OF PIECE_CELLS;
   PV = ARRAY [Man_Type] OF INTEGER;
   V_Data = ARRAY [Side_Index] OF PV;    (*  Material Array  *)
   TC_Data = ARRAY [Side_Index] OF TCA;  (*  Tactical Conflict Array  *)
   TD_Data = ARRAY [Side_Index] OF TCB;  (*  Tactical Defence Array  *)
   MvArrType = ARRAY [1..250] OF LogRcdType; (* Game Move History Array *)
   MoArrType = ARRAY [1..12] OF Str9;
   DayArrType = ARRAY [0..6] OF Str9;

   (*  AUTOMATON POINTER DATA TYPES  *)

   POINTER = ^Tree_Cells;

   Tree_Cells = RECORD
                   Move_List : NEW_PLY;       (* Game Tree *)
                   MLI : MvLstIndx;     (* Move List Sort Index *)
                   LX : INTEGER;  (* Level index within Game Tree *)
                   IX : INTEGER;  (* Node Index Within Game Tree *)
                   BX : INTEGER;  (* Node Index for Best Move *)
                   SX : INTEGER;  (* Side Index *)
                   PX : INTEGER;  (* Piece Index *)
                   WX : INTEGER;  (* Width of This Level of Game Tree *)
                   LVL : LEVEL_CELLS;  (* Level Statistics *)
                   Next : POINTER;  (* Pointer to Next Node (Level) *)
                   Last : POINTER   (* Pointer to Last Level *)
                END;

   (*  AUTOMATON FILE TYPES  *)

   LogFileType = FILE OF LogRcdType;
   StatFileType = FILE OF StatRcdType;


(********************************************************************)
(*              P R O G R A M   V A R I A B L E S                   *)
(********************************************************************)



VAR


   (*  AUTOMATON ARRAY DATA STRUCTURES  *)

   Material : ARRAY [1..6] OF INTEGER;       (* Piece Material Table *)
   Piece : ARRAY [1..6] OF STRING[6];        (* Piece Type Names *)
   Men : ARRAY [1..2] OF STRING[5];          (* Side Names: White and Black *)
   File_Vector : ARRAY [1..8] OF CHAR;       (* Board File Coordinates *)
   Order_Cell : ARRAY [1..16] OF INTEGER;    (* Order of Piece Move Gen *)
   MV : ARRAY [1..7, 1..9] OF MOVE_VECTORS;  (* Piece move patterns *)
   KL : ARRAY [1..MaxDepth, 1..Killers] OF Position;   (* Killer List *)
   SA : ARRAY [1..MaxDepth] OF ACCUMULATORS; (* Statistics Accumulators *)
   Piece_Table : PIECE_DATA;  (* Piece Vector Data *)
   V : V_DATA;           (* Number of Pieces of Given Type *)
   TC : TC_DATA;         (* Defending, Threating & Mobility *)
   TD : TD_DATA;         (* Table of Quantifing Heuristics *)
   Board : SQUARES;      (* Chess Board *)
   RL : RLList;          (* Refutation Table *)
   PC : PCList;          (* Principal Continuation Triangular Work Array *)
   MvArr : MvArrType;    (* Game Move History Array *)
   DayArr : DayArrType;  (* Array of Week Day Names *)
   MoArr : MoArrType;    (* Array of Month Names *)

   (*  AUTOMATON SET DATA STRUCTURES  *)

   Letters : LetterSet;       (* Set of Letters: 'A' .. 'H' *)
   Numbers : NumberSet;       (* Set of Numbers: '1' .. '8' *)
   Alphabet : AlphaSet;       (* Set of Letters: 'A' .. 'Z' *)
   Numerals : NumeralSet;     (* Set of Numbers: '0' .. '9' *)
   Jeopardy : JeopardySet;    (* Set of Numbers: 1 .. 2  (King Threatend) *)
   CheckMate : CheckMateSet;  (* Set of Number: 1  (King Mated) *)
   StaleMate : StaleMateSet;  (* Set of Number: 2  (King In StaleMate) *)
   Check : CheckSet;          (* Set of Number: 3  (King In Check) *)

   (*  AUTOMATON STATE VECTORS  *)

   Move : POSITION;                 (* Selected Move *)
   Opponent_Move : POSITION;        (* Opponent Move *)
   Machine_Move : POSITION;         (* Last Machine Move *)
   Void_Move : POSITION;            (* Empty Move *)
   Last_Piece_Moved : TABLE_INDEX;  (* The Last Piece Moved by MAKE *)
   W : PV;                          (* Piece Weight Array *)
   X_Input : X_VECTOR;
   Y_Input : Y_VECTOR;
   J, L, X, Y : INTEGER;   (* Indexes *)
   Nodes : INTEGER;
   MX : INTEGER;           (* Machine Index *)
   OX : INTEGER;           (* Oponent Index *)
   Pnct : CHAR;            (* Response string punctuation *)
   Full_Depth : INTEGER;   (* Maximum depth for full-width search *)
   Quiescence : INTEGER;   (* Max depth to which captures are expanded *)
   Horizon : INTEGER;      (* Maximum allowable levels of game-tree *)
   Half_Pawn : INTEGER;    (* Computed as PNV / 2 *)
   Third_Pawn : INTEGER;   (* Computed as PNV / 3 *)  
   Quarter_Pawn : INTEGER; (* Computed as PNV / 4 *)  
   Three_Quarter_Pawn : INTEGER; (* Computed as PNV / 4 * 3 *)
   Two_Third_Pawn : INTEGER; (* Computed as PNV / 3 * 2 *)    
   AI : INTEGER;           (* Value +2 *)
   Level : INTEGER;        (* Skill Level - selected by opponent: 1 - 9 *)
   Opponent : STR40;       (* Opponent's name *)
   Est : INTEGER;          (* Error estimate for setting the a-b window *)
   Err : INTEGER;        (* Dynamic Error Value for setting the a-b window *)
   Err_Adj : INTEGER;    (* Dynamic Error Adjustment to a-b Error Value *)
   Value : INTEGER;      (* a-b Score Returned by Last Iteration of Search *)
   CD : CHAR;     (* CHECK Code for reporting a "+" on the Game Log Report *)
   TimeRcd : TimeRcdType;  (* Time Record *)
   DateRcd : DateRcdType;  (* Date Record *)

   (*  AUTOMATON COMMUNICATION BUFFERS  *)

   Response : STR80;       (* Main Communication Buffer *)
   LastReply : STR80;      (* Last Communication Buffer *)
   Msg : STR40;            (* Machine's Messages to Operator *)

   (*  AUTOMATON ACCUMULATORS AND NUMERIC DATA STRUCTURES  *)

   Scored : REAL;         (* Total number of moves scored for current move *)
   Cutoffs : REAL;        (* Total number of cutoffs for current move *)
   RptScored : REAL;      (* Total number of moves scored for Reporting *)
   RptCutoffs : REAL;     (* Total number of cutoffs for Reporting *)
   TotalScored : REAL;    (* Total number of moves scored for game *)
   TotalCutoffs : REAL;   (* Total number of cutoffs for game *)
   Failed_High : INTEGER;  (* Total number of failed high researches *)
   Failed_Low : INTEGER;  (* Total number of failed low researches *)
   Fail_Cntr : INTEGER;   (* Number of double fails on a-b window *)
   Decay_Cntr : INTEGER;  (* Decay control for Fail_Cntr *)
   Move_Cntr : INTEGER;   (* Number of moves transacted in the game *)
   Iteration : INTEGER;   (* Iteration number for a-b quiescent search *)
   MI : INTEGER;        (* Move Index Points to the Last Move Within MvArr *)
   IC : INTEGER;      (* Iterations completed durring a-b quiescent search *)

   (*  AUTOMATON PRINTER CONTROL DATA STRUCTURES  *)

   Page_Cntr : INTEGER;   (* Number of pages printed while reporting *)
   Line_Cntr : INTEGER;   (* Number of Lines printed on current page *)
   Line : STR80;          (* Print line buffer for report output *)

   (*  AUTOMATON DYNAMIC DATA STRUCTURES  *)

   P : POINTER;           (* Heap Pointer *)
   Root_Node : POINTER;   (* Heap Top Pointer *)

   (*  HEURISTIC WEIGHTS FOLLOW  *)

   KV : INTEGER;          (* King Value *)
   QNV : INTEGER;         (* Queen Value *)
   RV : INTEGER;          (* Rook Value *)
   NV : INTEGER;          (* Knight Value *)
   BV : INTEGER;          (* Bishop Value *)
   PNV : INTEGER;         (* Pawn Value *)
   BHv : INTEGER;         (* Base Heuristic Weight *)
   DvHv : INTEGER;        (* Development Heuristic Weight *)
   DHv : INTEGER;         (* Defending Heuristic Weight *)
   THv : INTEGER;         (* Threatening Heuristic Weight *)
   MHv : INTEGER;         (* Mobility Heuristic Weight *)
   CpHv : INTEGER;        (* Capture Heuristic Weight *)
   CkHv : INTEGER;        (* Check Heuristic Weight *)
   AvHv : INTEGER;        (* Advancement Heuristic Weight *)
   KAHv : INTEGER;        (* King Side Attack Heuristic Weight *)
   Threshold : INTEGER;   (* Value to determine checkmate *)
                          (* Computed as KV - PNV * 3   *)

   (* END OF HEURISTICS *)

   (*  TIMING CONTROL DATA STRUCTURES  *)

   Hr : INTEGER;          (* Elapsed Time in Hours *)
   Mn : INTEGER;          (* Elapsed Time in Minutes *)
   Sec : INTEGER;         (* Elapsed Time in Seconds *)
   Hund : INTEGER;        (* Elapsed Time in Hundredths of a Second *)
   Et : REAL;             (* Elapsed Time in Seconds and Hundredths *)
   ElapsedClock : REAL;   (* Total Elapsed Time in Seconds and Hunds *)
   StartClock : REAL;     (* Start Time in Seconds and Hundredth Seconds *)
   StopClock : REAL;      (* Stop Time in Seconds and Hundredth Seconds *)
   MoveClock : REAL;      (* Allocated Time Computed for Current Move *)
   AllocatedTime : REAL;   (* Allocated Time for Moves *)
   AllocatedMoves : INTEGER;  (* Allocated Number of Moves Within Time *)
   Last_Score : INTEGER;  (* Score of Last Iteration for Timing Control *)
   Last_Move_Score : INTEGER;  (* Score of Last Move for Timing Control *)
   Shallow_Score : INTEGER;  (* Shallow search score for Timing Control *)
   ClearTime : TimeRcdType;  (* Variable for clearing time record *)

   (*  BOOLEAN VARIABLES  *)

   Done : BOOLEAN;
   Clock : BOOLEAN;
   Report : BOOLEAN;
   LstOpen : BOOLEAN;
   Trace : BOOLEAN;
   Time : BOOLEAN;
   Timed : BOOLEAN;
   Tournament : BOOLEAN;
   Alert : BOOLEAN;
   Alternate : BOOLEAN;
   HardCopy : BOOLEAN;
   Plot : BOOLEAN;
   Extended : BOOLEAN;
   Completed : BOOLEAN;
   Interupt : BOOLEAN;
   Cogitate : BOOLEAN;
   Predicted : BOOLEAN;
   Speculating : BOOLEAN;
   Active : BOOLEAN;
   Chronical : BOOLEAN;
   Hide : BOOLEAN;
   Save : BOOLEAN;
   Restore : BOOLEAN;
   Map : BOOLEAN;
   Show : BOOLEAN;
   ErrorFlag : BOOLEAN;
   Adjust : BOOLEAN;

   (*  GAME COMPLETION CODE  *)  (*  WHERE:   0 = Interrupted       *)
                                 (*           1 = CheckMate         *)
   TermCode : INTEGER;           (*           2 = StaleMate         *)
                                 (*           3 = Resign            *)
                                 (*           4 = Draw              *)
                                 (*           5 = Terminated        *)
                                 (*           6 = Continue Game     *)
                                 (*           7 = New Game          *)

   (*  SYSTEM EXTERNAL FILE ERROR CODE  *)

   IOstat : INTEGER;

   (*  SYSTEM EXTERNAL FILE BUFFERS FOLLOW  *)

   StatRcd : StatRcdType;     (* Record variable for statistics file *)
   LogRcd : LogRcdType;       (* Record variable for history file *)

   (*  SYSTEM EXTERNAL FILE DEFINITIONS FOLLOW  *)

   LST : TEXT;                (* Reporting printer output file *)
   StatFile : StatFileType;   (* System statistics file *)
   LogFile : LogFileType;     (* System move history file *)


(********************************************************************)
(*                                                                  *)
(*              P R O G R A M   L O G I C                           *)
(*                                                                  *)
(*                B E G I N S   H E R E                             *)
(*                                                                  *)
(********************************************************************)




(********************************************************************)

(*==================================================================*)
(*              L E V E L   7   A L G O R I T H M S                 *)
(*==================================================================*)

(********************************************************************)

PROCEDURE Initialize_Boolean;

   BEGIN

      (*  Initialize Automaton Boolean Terms  *)

      Active := FALSE;
      Done := FALSE;
      Clock := FALSE;
      Completed := FALSE;
      Report := FALSE;
      LstOpen := FALSE;
      Trace := FALSE;
      Time := FALSE;
      Timed := FALSE;
      Tournament := FALSE;
      Alternate := FALSE;
      HardCopy := FALSE;
      Alert := TRUE;
      Plot := TRUE;
      Extended := TRUE;
      Cogitate := TRUE;
      Interupt := FALSE;
      Predicted := FALSE;
      Speculating := FALSE;
      Chronical := TRUE;
      Save := FALSE;
      Restore := FALSE;
      Map := TRUE;
      Show := FALSE;
      Hide := TRUE;
      ErrorFlag := FALSE;
      Adjust := TRUE

   END;

(********************************************************************)

PROCEDURE Initialize_State_Vectors;

   BEGIN

      (*  Initialize Automaton State Variables  *)

      CD := ' ';
      TermCode := 0;
      Value := 0;
      Err := 0;
      MI := 0;
      AI := 2;
      MX := White;
      OX := Black;
      Fail_Cntr := 0;
      Move_Cntr := 0;
      TotalScored := 0;
      TotalCutoffs := 0;
      Page_Cntr := 0;
      Line_Cntr := MaxLines;
      ElapsedClock := 0;
      MoveClock := 0;
      StartClock := 0;
      StopClock := 0;
      IF NOT( Timed ) THEN
         AllocatedTime := 7200;
      AllocatedMoves := 40;
      Last_Score := -INF;
      Last_Move_Score := -INF;
      Shallow_Score := -INF;
      Msg := '';
      Line := '';

      (*  Initialize Automaton Heuristics  *)

      PNV := Pawn;
      Half_Pawn := PNV DIV 2;               	(*  1/2 Pawn Value  *)
      Third_Pawn := PNV DIV 3;              	(*  1/3 Pawn Value  *)
      Quarter_Pawn := PNV DIV 4;            	(*  1/4 Pawn Value  *)
	Three_Quarter_Pawn := Quarter_Pawn * 3;	(*  3/4 Pawn Value  *)
	Two_Third_Pawn := Third_Pawn * 2;		(*  3/4 Pawn Value  *)
      KV  := (PNV * 200);
      QNV := ((PNV * 9) + Half_Pawn);
      RV  := (PNV * 5);
      NV  := (PNV * 3);                                       
      BV  := (NV + Quarter_Pawn);  
      W[1] := KV;
      W[2] := QNV;
      W[3] := RV;
      W[4] := NV;
      W[5] := BV;
      W[6] := PNV;
      Threshold := KV - PNV * 3; (* CheckMate Threshold Heuristic Value *)
      BHv  := (PNV DIV 100);     (*  Base Heuristic Value  *)
      DvHv := BHv * 3;           (*  Development Heuristic Value  *)
      KAHv := BHv * 4;           (*  King Attack Heuristic Value  *)
      DHv  := BHv * 2;           (*  Defending Heuristic Value  *)
      THv  := BHv * 4;           (*  Threat Heuristic Value  *)              
      MHv  := BHv * 2;           (*  Mobility Heuristic Value  *)
      AvHv := BHv * 3;           (*  Advancement Heuristic Value  *)        
      CpHv := BHv * 5;           (*  Capture Heuristic Value  *)
      CkHv := BHv * 4;           (*  Check Heuristic Value  *)
      Est  := Half_Pawn;         (*  A-B Aspiration Window Error  *)
      Err_Adj := Est DIV 2;      (*  A-B Aspiration Window Error Adjustment *)

      (*  Initialize Material Table  *)

      Material[1] := 1;          (*  Number of Kings  *)
      Material[2] := 1;          (*  Number of Queens  *)
      Material[3] := 2;          (*  Number of Rooks  *)
      Material[4] := 2;          (*  Number of Knights  *)
      Material[5] := 2;          (*  Number of Bishops  *)
      Material[6] := 8;          (*  Number of Pawns  *)

      (*  Initialize Piece Name Table  *)

      Piece[1] := 'KING';        (*  King  *)
      Piece[2] := 'QUEEN';       (*  Queen  *)
      Piece[3] := 'ROOK';        (*  Rook  *)
      Piece[4] := 'KNIGHT';      (*  Knight  *)
      Piece[5] := 'BISHOP';      (*  Bishop  *)
      Piece[6] := 'PAWN';        (*  Pawn  *)

      (*  Initialize Color Side Table  *)

      Men[1] := 'WHITE';         (*  White  *)
      Men[2] := 'BLACK';         (*  Black  *)

      (*  Initialize Board File Vector Table  *)

      File_Vector[1] := 'A';
      File_Vector[2] := 'B';
      File_Vector[3] := 'C';
      File_Vector[4] := 'D';
      File_Vector[5] := 'E';
      File_Vector[6] := 'F';
      File_Vector[7] := 'G';
      File_Vector[8] := 'H';

      (*  Initialize Piece Move Order Generation  *)

      Order_Cell[1] := 5;
      Order_Cell[2] := 6;
      Order_Cell[3] := 7;
      Order_Cell[4] := 8;
      Order_Cell[5] := 2;
      Order_Cell[6] := 3;
      Order_Cell[7] := 4;
      Order_Cell[8] := 9;
      Order_Cell[9] := 10;
      Order_Cell[10] := 11;
      Order_Cell[11] := 12;
      Order_Cell[12] := 13;
      Order_Cell[13] := 14;
      Order_Cell[14] := 15;
      Order_Cell[15] := 16;
      Order_Cell[16] := 1;

      (*  Initialize Board Algebraic Notation Translation Sets  *)

      Letters := ['A'..'H'];
      Numbers := ['1'..'8'];
      Alphabet := ['A'..'Z'];
      Numerals := ['0'..'9'];

      (*  Initialize King Status Sets  *)

      CheckMate := [ 1 ];
      StaleMate := [ 2 ];
      Check := [ 3 ];
      Jeopardy := [ 1 .. 2 ];

      (*  Initialize Month Name Array  *)

      MoArr[1] := 'JANUARY';
      MoArr[2] := 'FEBRUARY';
      MoArr[3] := 'MARCH';
      MoArr[4] := 'APRIL';
      MoArr[5] := 'MAY';
      MoArr[6] := 'JUNE';
      MoArr[7] := 'JULY';
      MoArr[8] := 'AUGUST';
      MoArr[9] := 'SEPTEMBER';
      MoArr[10] := 'OCTOBER';
      MoArr[11] := 'NOVEMBER';
      MoArr[12] := 'DECEMBER';

      (*  Initialize Day Name Array  *)

      DayArr[0] := 'SUNDAY';
      DayArr[1] := 'MONDAY';
      DayArr[2] := 'TUESDAY';
      DayArr[3] := 'WEDNESDAY';
      DayArr[4] := 'THURSDAY';
      DayArr[5] := 'FRIDAY';
      DayArr[6] := 'SATURDAY';

      (*  Initialize Automaton Default Game States  *)

      IF ( NOT ( Active ) ) THEN
         BEGIN
            Opponent := '';
            Level := Default_Level;
            Full_Depth := FW_Limit;
            Quiescence := MaxSearch;
            Horizon := MaxSearch
         END;

      (*  Initialize Automaton Move Cells  *)

      WITH Void_Move DO
           BEGIN
              Move_Cell.From_Cell.File_Cell := 0;
              Move_Cell.From_Cell.Rank_Cell := 0;
              Move_Cell.To_Cell.File_Cell := 0;
              Move_Cell.To_Cell.Rank_Cell := 0;
              Score_Cell := 0;
              Mode_Cell := ' '
           END;
      Opponent_Move := Void_Move;
      Machine_Move := Void_Move;
      Move := Void_Move;

      (*  Initialize Automaton Time Cell  *)

      WITH ClearTime DO
           BEGIN
              Hr := 0;
              C1 := ':';
              Mn := 0;
              C2 := ':';
              Sc := 0;
              C3 := '.';
              Hn := 0;
              TSc := 0.0
           END

   END;

(********************************************************************)

PROCEDURE Clear_Move_Vectors;

   VAR   X, Y : INTEGER;

   BEGIN

      FOR X := 1 TO 7 DO
         FOR Y := 1 TO 9 DO
            BEGIN
               MV[X, Y].INC1 := 0;
               MV[X, Y].INC2 := 0;
               MV[X, Y].LIM1 := 0;
               MV[X, Y].LIM2 := 0
            END

   END;

(********************************************************************)

PROCEDURE Initialize_Move_Vectors;

   BEGIN

      (* Initialize King's Move Vectors *)

      MV[1, 1].INC1 := 9;
      MV[1, 1].INC2 := 1;
      MV[1, 2].INC2 := -1;
      MV[1, 3].INC1 := 1;
      MV[1, 3].INC2 := -1;
      MV[1, 3].LIM1 := 9;
      MV[1, 4].INC1 := 1;
      MV[1, 4].LIM1 := 9;
      MV[1, 5].INC1 := 1;
      MV[1, 5].INC2 := 1;
      MV[1, 5].LIM1 := 9;
      MV[1, 5].LIM2 := 9;
      MV[1, 6].INC2 := 1;
      MV[1, 6].LIM2 := 9;
      MV[1, 7].INC1 := -1;
      MV[1, 7].INC2 := 1;
      MV[1, 7].LIM2 := 9;
      MV[1, 8].INC1 := -1;
      MV[1, 9].INC1 := -1;
      MV[1, 9].INC2 := -1;

      (* Initialize Queen's Move Vectors *)

      MV[2, 1].INC1 := 9;
      MV[2, 1].INC2 := 7;
      MV[2, 2].INC2 := -1;
      MV[2, 3].INC1 := 1;
      MV[2, 3].INC2 := -1;
      MV[2, 3].LIM1 := 9;
      MV[2, 4].INC1 := 1;
      MV[2, 4].LIM1 := 9;
      MV[2, 5].INC1 := 1;
      MV[2, 5].INC2 := 1;
      MV[2, 5].LIM1 := 9;
      MV[2, 5].LIM2 := 9;
      MV[2, 6].INC2 := 1;
      MV[2, 6].LIM2 := 9;
      MV[2, 7].INC1 := -1;
      MV[2, 7].INC2 := 1;
      MV[2, 7].LIM2 := 9;
      MV[2, 8].INC1 := -1;
      MV[2, 9].INC1 := -1;
      MV[2, 9].INC2 := -1;

      (* Initialize Rook's Move Vectors *)

      MV[3, 1].INC1 := 5;
      MV[3, 1].INC2 := 7;
      MV[3, 2].INC2 := -1;
      MV[3, 3].INC1 := 1;
      MV[3, 3].LIM1 := 9;
      MV[3, 4].INC2 := 1;
      MV[3, 4].LIM2 := 9;
      MV[3, 5].INC1 := -1;

      (* Initialize Knight's Move Vectors *)

      MV[4, 1].INC1 := 9;
      MV[4, 1].INC2 := 1;
      MV[4, 2].INC1 := -1;
      MV[4, 2].INC2 := 2;
      MV[4, 2].LIM2 := 9;
      MV[4, 3].INC1 := 1;
      MV[4, 3].INC2 := 2;
      MV[4, 3].LIM1 := 9;
      MV[4, 3].LIM2 := 9;
      MV[4, 4].INC1 := -1;
      MV[4, 4].INC2 := -2;
      MV[4, 5].INC1 := 1;
      MV[4, 5].INC2 := -2;
      MV[4, 5].LIM1 := 9;
      MV[4, 6].INC1 := -2;
      MV[4, 6].INC2 := 1;
      MV[4, 6].LIM2 := 9;
      MV[4, 7].INC1 := 2;
      MV[4, 7].INC2 := 1;
      MV[4, 7].LIM1 := 9;
      MV[4, 7].LIM2 := 9;
      MV[4, 8].INC1 := -2;
      MV[4, 8].INC2 := -1;
      MV[4, 9].INC1 := 2;
      MV[4, 9].INC2 := -1;
      MV[4, 9].LIM1 := 9;

      (* Initialize Bishop's Move Vectors *)

      MV[5, 1].INC1 := 5;
      MV[5, 1].INC2 := 7;
      MV[5, 2].INC1 := 1;
      MV[5, 2].INC2 := -1;
      MV[5, 2].LIM1 := 9;
      MV[5, 3].INC1 := 1;
      MV[5, 3].INC2 := 1;
      MV[5, 3].LIM1 := 9;
      MV[5, 3].LIM2 := 9;
      MV[5, 4].INC1 := -1;
      MV[5, 4].INC2 := 1;
      MV[5, 4].LIM2 := 9;
      MV[5, 5].INC1 := -1;
      MV[5, 5].INC2 := -1;

      (* Initialize White Pawn's Vectors *)

      MV[6, 1].INC1 := 4;
      MV[6, 1].INC2 := 1;
      MV[6, 2].INC2 := 1;
      MV[6, 2].LIM2 := 9;
      MV[6, 3].INC1 := 1;
      MV[6, 3].INC2 := 1;
      MV[6, 3].LIM1 := 9;
      MV[6, 3].LIM2 := 9;
      MV[6, 4].INC1 := -1;
      MV[6, 4].INC2 := 1;
      MV[6, 4].LIM2 := 9;

      (* Initialize Black Pawn's Vectors *)

      MV[7, 1].INC1 := 4;
      MV[7, 1].INC2 := 1;
      MV[7, 2].INC2 := -1;
      MV[7, 3].INC1 := 1;
      MV[7, 3].INC2 := -1;
      MV[7, 3].LIM1 := 9;
      MV[7, 4].INC1 := -1;
      MV[7, 4].INC2 := -1

   END;

(********************************************************************)

PROCEDURE Clear_Piece_Table;

   VAR   X, Y : INTEGER;

   BEGIN

      FOR X := White TO Black DO
         FOR Y := 1 TO 16 DO
            BEGIN
               WITH Piece_Table[X, Y] DO
                  BEGIN
                     Position_Cell.File_Cell := 0;
                     Position_Cell.Rank_Cell := 0;
                     Status_Cell := ' ';
                     Pseudo_Status := ' ';
                     Move_Cntrl := ' ';
                     Defending := 0;
                     Threats := 0;
                     Mobility := 0;
                     Development := 0;
                     Moves := 0
                  END
            END

   END;

(********************************************************************)

PROCEDURE Initialize_Piece_Table;

   VAR   X, Y, Z : INTEGER;

   BEGIN

      FOR X := White TO Black DO
         BEGIN
            IF ( X = White ) THEN
               BEGIN
                  Y := 1;
                  Z := +1
               END
            ELSE
               BEGIN
                  Y := 8;
                  Z := -1
               END;
            Piece_Table[X, 1].Position_Cell.File_Cell := 5;
            Piece_Table[X, 1].Position_Cell.Rank_Cell := Y;
            Piece_Table[X, 1].Move_Pattern := 1;
            Piece_Table[X, 1].Development := -7;                               
            Piece_Table[X, 2].Position_Cell.File_Cell := 4;
            Piece_Table[X, 2].Position_Cell.Rank_Cell := Y;
            Piece_Table[X, 2].Move_Pattern := 2;
            Piece_Table[X, 2].Development := -8;                              
            Piece_Table[X, 3].Position_Cell.File_Cell := 8;
            Piece_Table[X, 3].Position_Cell.Rank_Cell := Y;
            Piece_Table[X, 3].Move_Pattern := 3;
            Piece_Table[X, 3].Development := 0;                                
            Piece_Table[X, 4].Position_Cell.File_Cell := 1;
            Piece_Table[X, 4].Position_Cell.Rank_Cell := Y;
            Piece_Table[X, 4].Move_Pattern := 3;
            Piece_Table[X, 4].Development := 2;
            Piece_Table[X, 5].Position_Cell.File_Cell := 7;
            Piece_Table[X, 5].Position_Cell.Rank_Cell := Y;
            Piece_Table[X, 5].Move_Pattern := 4;
            Piece_Table[X, 5].Development := 12;                            
            Piece_Table[X, 6].Position_Cell.File_Cell := 2;
            Piece_Table[X, 6].Position_Cell.Rank_Cell := Y;
            Piece_Table[X, 6].Move_pattern := 4;
            Piece_Table[X, 6].Development := 11;                   
            Piece_Table[X, 7].Position_Cell.File_Cell := 6;
            Piece_Table[X, 7].Position_Cell.Rank_Cell := Y;
            Piece_Table[X, 7].Move_Pattern := 5;
            Piece_Table[X, 7].Development := 8;                            
            Piece_Table[X, 8].Position_Cell.File_Cell := 3;
            Piece_Table[X, 8].Position_Cell.Rank_Cell := Y;
            Piece_Table[X, 8].Move_Pattern := 5;
            Piece_Table[X, 8].Development := 8;                           
            Y := Y + Z;
            Piece_Table[X, 9].Position_Cell.File_Cell := 5;
            Piece_Table[X, 9].Position_Cell.Rank_Cell := Y;
            Piece_Table[X, 9].Move_Pattern := 6;
            Piece_Table[X, 9].Development := 25;             
            Piece_Table[X, 10].Position_Cell.File_Cell := 4;
            Piece_Table[X, 10].Position_Cell.Rank_Cell := Y;
            Piece_Table[X, 10].Move_Pattern := 6;
            Piece_Table[X, 10].Development := 11;                                    
            Piece_Table[X, 11].Position_Cell.File_Cell := 6;
            Piece_Table[X, 11].Position_Cell.Rank_Cell := Y;
            Piece_Table[X, 11].Move_Pattern := 6;
            Piece_Table[X, 11].Development  := 0;                        
            Piece_Table[X, 12].Position_Cell.File_Cell := 3;
            Piece_Table[X, 12].Position_Cell.Rank_Cell := Y;
            Piece_Table[X, 12].Move_Pattern := 6;
            Piece_Table[X, 12].Development := 1;  
            Piece_Table[X, 13].Position_Cell.File_Cell := 7;
            Piece_Table[X, 13].Position_Cell.Rank_Cell := Y;
            Piece_Table[X, 13].Move_Pattern := 6;
            Piece_Table[X, 13].Development := 0;                 
            Piece_Table[X, 14].Position_Cell.File_Cell := 2;
            Piece_Table[X, 14].Position_Cell.Rank_Cell := Y;
            Piece_Table[X, 14].Move_Pattern := 6;
            Piece_Table[X, 14].Development := 4;                              
            Piece_Table[X, 15].Position_Cell.File_Cell := 1;
            Piece_Table[X, 15].Position_Cell.Rank_Cell := Y;
            Piece_Table[X, 15].Move_Pattern := 6;
            Piece_Table[X, 15].Development := 0;
            Piece_Table[X, 16].Position_Cell.File_Cell := 8;
            Piece_Table[X, 16].Position_Cell.Rank_Cell := Y;
            Piece_Table[X, 16].Move_Pattern := 6;
            Piece_Table[X, 16].Development := 0
         END

   END;

(********************************************************************)

PROCEDURE Clear_Board;

   VAR   X, Y : INTEGER;

   BEGIN

      FOR X := 1 TO 8 DO
         FOR Y := 1 TO 8 DO
            BEGIN
               Board[X, Y].Piece_Cell.Side_Cell := 0;
               Board[X, Y].Piece_Cell.Man_Cell := 0;
               Board[X, Y].Attack_Cell := ' '
            END

   END;

(********************************************************************)

PROCEDURE Initialize_Board;

   VAR   I, J, X, Y : INTEGER;

   BEGIN

      FOR X := White TO Black DO
         FOR Y := 1 TO 16 DO
            BEGIN
               I := Piece_Table[X, Y].Position_Cell.File_Cell;
               J := Piece_Table[X, Y].Position_Cell.Rank_Cell;
               Board[I, J].Piece_Cell.Side_Cell := X;
               Board[I, J].Piece_Cell.Man_Cell := Y
            END

   END;

(********************************************************************)

PROCEDURE Reset_Accumulators;

   VAR   I : INTEGER;

   BEGIN

      Scored := 0;
      Cutoffs := 0;
      Failed_High := 0;
      Failed_Low := 0;
      FOR I := 1 TO MaxDepth DO
          BEGIN
             SA[I].Gened := 0;
             SA[I].Cutoff := 0
          END

   END;

(********************************************************************)

PROCEDURE Clear_Attack_Cells;

   VAR   F, R, S : INTEGER;

   BEGIN

      FOR S := White TO Black DO
          BEGIN
             IF ( S = White ) THEN
                R := 1
             ELSE
                R := 8;
             FOR F := 1 TO 8 DO
                 BEGIN
                    Board[F, R].Attack_Cell := ' '
                 END
          END

   END;

(********************************************************************)

PROCEDURE Clear_Killers;

   VAR   X, Y : INTEGER;

   BEGIN

      FOR X := 1 TO MaxDepth DO
          FOR Y := 1 TO Killers DO
              BEGIN
                 KL[X, Y] := Void_Move
              END

   END;

(********************************************************************)

PROCEDURE Clear_Refutation_List;

   VAR   X, Y : INTEGER;

   BEGIN

      FOR X := 1 TO MaxBreadth DO
          FOR Y := 1 TO MaxDepth DO
              BEGIN
                 RL[X, Y] := Void_Move
              END

   END;

(********************************************************************)

PROCEDURE Clear_PC_List;

   VAR   X, Y : INTEGER;

   BEGIN

      FOR X := 1 TO MaxDepth DO
          FOR Y := 1 TO MaxDepth DO
              BEGIN
                 PC[X, Y] := Void_Move
              END

   END;

(********************************************************************)

PROCEDURE GetDateTime ( VAR Dt : DateRcdType; VAR Tm : TimeRcdType );

   VAR

      H, M, S, C : WORD;
      Y, Mn, D, DW : WORD;

   BEGIN

      GETTIME( H, M, S, C );
      WITH Tm DO
         BEGIN
            Hr := H;
            Mn := M;
            Sc := S;
            Hn := C;
            TSc := ( H * 3600 ) + ( M * 60 ) + S + ( C / 100 )
         END;
      GETDATE( Y, Mn, D, DW );
      WITH Dt DO
         BEGIN
            Mo := Mn;
            Dy := D;
            Yr := Y;
            DOW := DW;
            DAY := ''
         END

   END;

(********************************************************************)

PROCEDURE Initialize_StatRcd( VAR StatRcd : StatRcdType );

   VAR

         D : DateRcdType;
         T : TimeRcdType;

   BEGIN

      GetDateTime( D, T );
      WITH StatRcd DO
         BEGIN
            CompletionCode := TermCode;
            StartTime := T
         END

   END;

(*********************************************************************)

PROCEDURE MapToMvArr( Mv : POSITION );

   VAR

      LogRec : LogRcdType;

   BEGIN

      MI := MI + 1;
      WITH LogRec DO
         BEGIN
            MvCntr := Move_Cntr;
            Move := Mv;
            NodesScored := 0.0;
            CutOffs := 0.0;
            NodesPerSec := 0.0;
            OpntMvTime := ClearTime;
            MchnMvTime := ClearTime;
            IF ( ( Time ) OR ( Tournament ) ) THEN
               BEGIN
                  WITH MchnMvTime DO
                     BEGIN
                        MchnMvTime.TSc := ET;
                        MchnMvTime.Hr := Hr;
                        MchnMvTime.Mn := Mn;
                        MchnMvTime.Sc := Sec;
                        MchnMvTime.Hn := Hund
                     END
               END
         END;
      MvArr[ MI ] := LogRec

   END;

(********************************************************************)

FUNCTION CheckPtr : INTEGER;

   VAR

      StatCode : INTEGER;

   BEGIN

      StatCode := IORESULT;
      IF ( NOT ( StatCode = 0 ) ) THEN
         BEGIN
            WRITELN;
            WRITELN( 'ATTENTION: THE PRINTER IS NOT READY.');
            WRITELN( 'CHECK PRINTER, THEN PRESS':36);
            WRITELN( 'ANY KEY TO CONTINUE.':31, Alarm);
            WRITELN;
            REPEAT UNTIL ( KEYPRESSED );
            Response := READKEY
         END;
      CheckPtr := StatCode

   END;

(********************************************************************)

FUNCTION CheckIO( Msg : Str20 ) : INTEGER;

   VAR

      StatCode : INTEGER;

   BEGIN

      StatCode := IORESULT;
      IF ( NOT ( StatCode IN [ 0, 2, 3, 15 ] ) AND ( Length( Msg ) > 0 ) )
      THEN
         BEGIN
            WRITELN;
            WRITE( 'ATTENTION: DISK ERROR ' );
            WRITELN( StatCode, ' OCCURED WHILE ', Msg, '.' );
            WRITELN( 'PRESS ANY KEY TO CONTINUE.':37, Alarm );
            WRITELN;
            REPEAT UNTIL ( KEYPRESSED );
            Response := READKEY
         END;
      CheckIO := StatCode

   END;

(********************************************************************)

PROCEDURE Open_Printer;

   BEGIN

      LstOpen := TRUE;
      ASSIGN( LST, Printer );
      REPEAT
         REWRITE( LST );
         IOstat := CheckPtr
      UNTIL ( IOstat = 0 )

   END;

(********************************************************************)

PROCEDURE Close_Printer;

   BEGIN

      LstOpen := FALSE;
      REPEAT
         CLOSE( LST );
         IOstat := CheckPtr
      UNTIL ( IOstat = 0 )

   END;

(********************************************************************)

PROCEDURE Erase_Files;

   BEGIN

      ERASE( StatFile );
      IOstat := CheckIO( 'ERASING STATFILE' );       
      ERASE( LogFile );
      IOstat := CheckIO( 'ERASING LOGFILE' );        
      IOstat := 0

   END;

(********************************************************************)

PROCEDURE Open_StatFile( VAR StatFile : StatFileType;
                             IOMode : CHAR );

   VAR

      Root : Str1;
      Path : Str6;
      Msg : Str20;
      N : INTEGER;
      
   BEGIN

      N := 0;
      Msg := '';
      Root := '\';
      Path := DrvPth;
      REPEAT
          ASSIGN( StatFile, Root+Path+StatDrv );
          IF ( IOMode IN [ 'I', 'O' ] ) THEN
            BEGIN
               CASE IOMode OF
                  'I' : RESET( StatFile );
                  'O' : REWRITE( StatFile )
               END
            END
         ELSE
            BEGIN
               WRITE( ' WARNING: UNRECOGNIZED CODE ', IOMode);
               WRITELN( ' FOR OPENING STATFILE.', Alarm );
               WRITELN;
               REWRITE( StatFile )
            END;
         IOstat := CheckIO( Msg );
         IF ( IOstat = 0 ) THEN
            BEGIN
               IF ( IOMode = 'O' ) THEN
                  BEGIN
                     TermCode := 0
                  END
            END
         ELSE
            BEGIN
               IF ( Path = '' ) THEN
                  BEGIN
                     IF ( IOMode = 'I' ) THEN
                        BEGIN
                           TermCode := 7;
                           StatRcd.CompletionCode := 7;
                           IOstat := 0
                        END
                     ELSE
                        BEGIN
                           IOstat := 0
                        END
                  END
               ELSE
                  BEGIN
                     IF ( Root = '' ) THEN
                        BEGIN
                           N := N + 1;
                           Path := '';
                           Msg := 'OPENING STATFILE'
                        END
                     ELSE
                        BEGIN
                           Root := ''
                        END
                  END
            END

      UNTIL (( IOstat = 0 ) OR ( N = Retry_Limit ))

   END;

(********************************************************************)

PROCEDURE Close_StatFile( VAR StatFile : StatFileType );

   VAR

      N : INTEGER;
      
   BEGIN

      N := 0;
      IF NOT( TermCode = 7 ) THEN
         REPEAT
            N := N + 1;
            CLOSE( StatFile );
            IOstat := CheckIO( 'CLOSING STATFILE' )
         UNTIL (( IOstat IN [ 0, 2, 3 ] ) OR ( N = Retry_Limit ))

   END;

(********************************************************************)

PROCEDURE Open_LogFile( VAR LogFile : LogFileType;
                            IOMode : char );

   VAR

      Root : Str1;
      Path : Str6;
      Msg : Str20;
      N : INTEGER;
      
   BEGIN

      N := 0;
      Msg := '';
      Root := '\';
      Path := DrvPth;
      REPEAT
         ASSIGN( LogFile, Root+Path+LogDrv );
         IF ( IOMode IN [ 'I', 'O' ] ) THEN
            BEGIN
               CASE IOMode OF
                  'I' : RESET( LogFile );
                  'O' : REWRITE( LogFile )
               END
            END
         ELSE
            BEGIN
               WRITE( ' WARNING: UNRECOGNIZED CODE ', IOMode);
               WRITELN( ' FOR OPENING LOGFILE.', Alarm );
               WRITELN;
               REWRITE( LogFile )
            END;
         IOstat := CheckIO( Msg );
         IF ( NOT( IOstat = 0 ) ) THEN
            BEGIN
               IF ( Path = '' ) THEN
                  BEGIN
                     IOstat := 0
                  END
               ELSE
                  BEGIN
                     IF ( Root = '' ) THEN
                        BEGIN
                           N := N + 1;
                           Path := '';
                           Msg := 'OPENING LOGFILE'
                        END
                     ELSE
                        BEGIN
                           Root := ''
                        END
                  END
            END

      UNTIL (( IOstat = 0 ) OR ( N = Retry_Limit ))

   END;

(********************************************************************)

PROCEDURE Close_LogFile( VAR LogFile : LogFileType );

   VAR

      N : INTEGER;
      
   BEGIN

      N := 0;
      REPEAT
         N := N + 1;
         CLOSE( LogFile );
         IOstat := CheckIO( 'CLOSING LOGFILE' )
      UNTIL (( IOstat IN [ 0, 2, 3 ] ) OR ( N = Retry_Limit ))

   END;

(********************************************************************)

PROCEDURE TIMER(Ch : CHAR; VAR Hr, Mn, Sc, Hnd : INTEGER; VAR Et : REAL);

VAR

   H, M, S, C : WORD;
   ElapsedTime : REAL;

BEGIN

   CASE Ch OF

        'B', 'b' : BEGIN
                      GetTime( H, M, S, C );
                      StartClock := ( H * 3600 ) + ( M * 60 ) +
                                    S + ( C / 100 );
                      Et := 0;
                      Hr := 0;
                      Mn := 0;
                      Sc := 0;
                      Hnd := 0
                   END;

        'E', 'e' : BEGIN
                      GetTime( H, M, S, C );
                      StopClock := ( H * 3600 ) + ( M * 60 ) +
                                    S + ( C / 100 );
                      ElapsedTime := ( StopClock - StartClock );
                      IF (ElapsedTime < 0) THEN
                         ElapsedTime := ( ( 86400 - StartClock ) +
                                                   StopClock );
                      Et := ElapsedTime;
                      Hr := TRUNC( ElapsedTime / 3600 );
                      ElapsedTime := ( ElapsedTime - ( Hr * 3600 ));
                      Mn := TRUNC( ElapsedTime / 60 );
                      ElapsedTime := ( ElapsedTime - ( Mn * 60 ));
                      Sc := TRUNC( ElapsedTime );
                      ElapsedTime := ( ElapsedTime - ( Sc ));
                      Hnd := TRUNC( ElapsedTime * 100 )
                   END

   END

END;

(********************************************************************)

PROCEDURE Print_Headings;

   VAR

      OPNAME : STR10;

   BEGIN

      IF NOT( LstOpen ) THEN
         BEGIN
            Open_Printer
         END;
      OPNAME := COPY( OPPONENT, 3, 10 );
      WRITELN( LST, FormFeed );
      IOstat := CheckPtr;
      WRITELN( LST, 'COMPUTER CHESS:  CP3V', Version,
                    '       MOVE HISTORY OF THE GAME' );
      IOstat := CheckPtr;
      WRITELN( LST, ' ' );
      IOstat := CheckPtr;
      WRITE( LST, 'SEARCH DEPTH: ', Full_Depth:1, '   ',
              'SEARCH WIDTH: ', MaxBreadth:1, '    ',
              'ORDERING INTERVAL: ' );
      IOstat := CheckPtr;
      IF ( Alternate ) THEN
         BEGIN
            WRITELN( LST, 'ALTERNATING' );
            IOstat := CheckPtr
         END
      ELSE
         BEGIN
            WRITELN( LST, 'CONVENTIONAL' );
            IOstat := CheckPtr
         END;
      WRITELN( LST );
      IOstat := CheckPtr;
      IF ( MX = Black ) THEN
         BEGIN
            WRITE( LST, '(OPPONENT)':18 );
            IOstat := CheckPtr;
            WRITELN( LST, '(MACHINE)':52 );
            IOstat := CheckPtr;
            WRITE( LST, OPNAME:17 );
            IOstat := CheckPtr;
            WRITELN( LST, 'HAL':50 );
            IOstat := CheckPtr
         END
      ELSE
         BEGIN
            WRITE( LST, '(MACHINE)':18 );
            IOstat := CheckPtr;
            WRITELN( LST, '(OPPONENT)':52 );
            IOstat := CheckPtr;
            WRITE( LST, 'HAL':15 );
            IOstat := CheckPtr;
            WRITELN( LST, OPNAME:54 );
            IOstat := CheckPtr
         END;
      WRITELN( LST );
      IOstat := CheckPtr;
      WRITELN( LST, '  MOVE     WHITE     MOVE TIME',
              '       NODES       BRANCHES      BLACK' );
      IOstat := CheckPtr;
      WRITELN( LST, ' NUMBER   FROM TO   MIN:SEC.HNS',
              '      SCORED       PRUNED      FROM TO' );
      IOstat := CheckPtr;
      WRITELN( LST, '-----------------------------------------------',
              '----------------------' );
      IOstat := CheckPtr;
      WRITELN( LST );
      IOstat := CheckPtr;
      Line_Cntr := 11;
      Page_Cntr := Page_Cntr + 1

   END;

(********************************************************************)

PROCEDURE Report_Statistics;

   VAR   
         
       ET, NPS : REAL;
       I, H, M, S, C, SD : INTEGER;

   BEGIN

      TIMER( 'E', H, M, S, C, ET );
      IF ( ET = 0.0 ) THEN
         NPS := 0.0
      ELSE
         NPS := ( Scored / ET );
      WRITELN;
      WRITE( 'MOVES SCORED = ', Scored:1:0, '   CUTOFFS = ', Cutoffs:1:0 );
      WRITE( '   FAILED HIGH = ', Failed_High  );
      WRITELN( '   FAILED LOW = ', Failed_Low );
      WRITE( 'ITERATIONS COMPLETED = ', IC - 1);
      IF ( NOT ( Tournament ) ) THEN
         WRITE( '   MOVES COMPLETED = ', ( Move_Cntr + 1 ) );
      SD := 0;
      FOR I := 1 TO Horizon DO                
          BEGIN
             IF ( SA[I].Gened > 0 ) THEN
                SD := I
          END;
      WRITE( '   DEPTH = ', SD );
      WRITE( '   NPS = ', NPS:1:0 );
      IF ( Tournament ) THEN
         WRITE( '   ' )
      ELSE
         WRITELN;
      WRITELN( 'SCORE = ',
              Root_Node^.Move_List[Root_Node^.MLI[1]].Score_Cell );
      WRITELN;
      WRITE( 'CUTOFFS OCCURING AT LEVELS ' );
      FOR I := 1 TO Horizon DO                 
          BEGIN
             WRITE( '  L', I:1, '=', SA[I].Cutoff:1:0 )
          END;
      WRITELN( '.' );                         
      WRITELN;
      WRITE( 'MOVE LISTS GENERATED AT LEVELS ' );
      FOR I := 1 TO Horizon DO                 
          BEGIN
             WRITE( '  L', I:1, '=', SA[I].Gened:1:0 )
          END;
      WRITELN( '.' );
      WRITELN

   END;

(********************************************************************)

(*==================================================================*)
(*              L E V E L   6   A L G O R I T H M S                 *)
(*==================================================================*)

(********************************************************************)

PROCEDURE Generate_Move_Vectors;

   BEGIN

      Clear_Move_Vectors;
      Initialize_Move_Vectors

   END;

(********************************************************************)

PROCEDURE Setup_Board;

   BEGIN

      Clear_Board;
      Initialize_Board

   END;

(********************************************************************)

PROCEDURE Print( CC : CHAR; Line : STR80 );

   BEGIN

      IF NOT( LstOpen ) THEN
         BEGIN
            Open_Printer
         END;
      IF ( Line_Cntr > ( MaxLines - 1 ) ) THEN
         BEGIN
            Print_Headings
         END;
      CASE CC OF

           ' ' : BEGIN
                    WRITELN( LST, Line );
                    IOstat := CheckPtr;
                    Line_Cntr := Line_Cntr + 1
                 END;

           '-' : BEGIN
                    WRITELN( LST, ' ' );
                    IOstat := CheckPtr;
                    WRITELN( LST, Line );
                    IOstat := CheckPtr;
                    Line_Cntr := Line_Cntr + 2
                 END;

           '+' : BEGIN
                    WRITELN( LST, ' ' );
                    IOstat := CheckPtr;
                    WRITELN( LST, ' ' );
                    IOstat := CheckPtr;
                    WRITELN( LST, Line );
                    IOstat := CheckPtr;
                    Line_Cntr := Line_Cntr + 3
                 END;

           '0' : BEGIN
                    WRITE( LST, Line );
                    IOstat := CheckPtr
                 END;

           '1' : BEGIN
                    WRITELN( LST, FormFeed );
                    IOstat := CheckPtr;
                    WRITELN( LST, Line );
                    IOstat := CheckPtr;
                    Page_Cntr := Page_Cntr + 1;
                    Line_Cntr := 0
                 END

      END

   END;

(********************************************************************)

(*==================================================================*)
(*              L E V E L   5   A L G O R I T H M S                 *)
(*==================================================================*)

(********************************************************************)

PROCEDURE Clear_TC_Cells;

   VAR    I : INTEGER;

   BEGIN

      FOR I := White TO Black DO
         BEGIN
            WITH TC[I] DO
               BEGIN
                  D := 0;
                  T := 0;
                  M := 0
               END
         END

   END;

(********************************************************************)

PROCEDURE Clear_TD_Cells;

   VAR   I : INTEGER;

   BEGIN

      FOR I := White TO Black DO
         BEGIN
            WITH TD[I] DO
               BEGIN
                  CP := 0;
                  DV := 0;
                  CK := 0;
                  AV := 0
               END
         END

   END;

(********************************************************************)

PROCEDURE Clear_LVL_Cells( P : POINTER );

   BEGIN

      WITH P^, LVL DO
         BEGIN
            To_Position.Side_Cell := 0;
            To_Position.Man_Cell := 0;
            Move_Ctrl := ' ';
            MPS := ' ';
            MSC := ' ';
            OSC := ' ';
            OPS := ' ';
            Move_Cnt := 0;
            DEV := 0;
            DEV2 := 0;
            WITH TC2 DO
               BEGIN
                  D := 0;
                  M := 0;
                  T := 0
               END;
            WITH TD2 DO
               BEGIN
                  AV := 0;
                  DV := 0;
                  CP := 0;
                  CK := 0
               END
         END

   END;

(********************************************************************)

PROCEDURE Initialize_Pieces;

   VAR   I : INTEGER;

   BEGIN

      FOR I := White TO Black DO
         BEGIN
            V[I, 1] := 1;
            V[I, 2] := 1;
            V[I, 3] := 2;
            V[I, 4] := 2;
            V[I, 5] := 2;
            V[I, 6] := 8
         END

   END;

(********************************************************************)

PROCEDURE Build_Piece_Table;

   BEGIN

      Clear_Piece_Table;
      Initialize_Piece_Table

   END;

(********************************************************************)

FUNCTION List_Gen( P : POINTER; CH : CHAR ) : INTEGER;

   VAR

      I, S, M, N, OS, I1, I2, I3, I4, I5, I6, I7, I8, I9,
      FR, FF, TR, TF, WS, WM, V1, V2 : INTEGER;


   FUNCTION Castling_Gen( P : POINTER ) : INTEGER;

      VAR

         I, R, S : INTEGER;

      BEGIN

         N := 0;
         S := P^.SX;
         R := Piece_Table[S, 1].Position_Cell.Rank_Cell;
         IF ( ( Piece_Table[S, 1].Moves = 0 ) AND
              ( Board[5, R].Attack_Cell = ' ' ) AND
              ( NOT(( Piece_Table[S, 1].Pseudo_Status IN ['C', '!'] ) OR
                    ( Piece_Table[S, 1].Status_Cell IN ['C', '!'] ))) )
         THEN
            BEGIN
               IF ( ( Piece_Table[S, 3].Moves = 0 ) AND
                    ( NOT(( Piece_Table[S, 3].Status_Cell = 'C' ) OR
                          ( Piece_Table[S, 3].Pseudo_Status = 'C' )))
                    AND ((Board[8, R].Piece_Cell.Side_Cell = S) AND
                         (Board[8, R].Piece_Cell.Man_Cell = 3)) )
               THEN
                  BEGIN
                     IF ( (Board[6, R].Piece_Cell.Side_Cell = 0) AND
                          (Board[6, R].Attack_Cell = ' ') AND
                          (Board[7, R].Piece_Cell.Side_Cell = 0) AND
                          (Board[7, R].Attack_Cell = ' ') )
                     THEN
                        BEGIN
                           N := N + 1;
                           I := P^.IX;
                           I := I + 1;
                           P^.IX := I;
                           P^.MLI[I] := I;
                           P^.Move_List[I].Move_Cell.From_Cell.
                                                File_Cell := 5;
                           P^.Move_List[I].Move_Cell.From_Cell.
                                                Rank_Cell := R;
                           P^.Move_List[I].Move_Cell.To_Cell.
                                                File_Cell := 7;
                           P^.Move_List[I].Move_Cell.To_Cell.
                                                Rank_Cell := R;
                           P^.Move_List[I].Score_Cell :=
                                                ( PNV + Half_Pawn );
                           P^.Move_List[I].Mode_Cell := 'K'
                        END
                  END;
               IF ( ( Piece_Table[S, 4].Moves = 0 ) AND
                    ( NOT(( Piece_Table[S, 4].Status_Cell = 'C' ) OR
                         ( Piece_Table[S, 4].Pseudo_Status = 'C' )))
                    AND ((Board[1, R].Piece_Cell.Side_Cell = S) AND
                         (Board[1, R].Piece_Cell.Man_Cell = 4)) )
               THEN
                  BEGIN
                     IF ( (Board[2, R].Piece_Cell.Side_Cell = 0) AND
                          (Board[2, R].Attack_Cell = ' ') AND
                          (Board[3, R].Piece_Cell.Side_Cell = 0) AND
                          (Board[3, R].Attack_Cell = ' ') AND
                          (Board[4, R].Piece_Cell.Side_Cell = 0) AND
                          (Board[4, R].Attack_Cell = ' ') )
                     THEN
                        BEGIN
                           N := N + 1;
                           I := P^.IX;
                           I := I + 1;
                           P^.IX := I;
                           P^.MLI[I] := I;
                           P^.Move_List[I].Move_Cell.From_Cell.
                                                 File_Cell := 5;
                           P^.Move_List[I].Move_Cell.From_Cell.
                                                 Rank_Cell := R;
                           P^.Move_List[I].Move_Cell.To_Cell.
                                                 File_Cell := 3;
                           P^.Move_List[I].Move_Cell.To_Cell.
                                                 Rank_Cell := R;
                           P^.Move_List[I].Score_Cell := PNV;
                           P^.Move_List[I].Mode_Cell := 'Q'
                        END
                  END
            END;
         Castling_Gen := N
      END;


   FUNCTION Enpassant : BOOLEAN;

      VAR OS, OM, OMP : INTEGER;
          PCT : TABLE_INDEX;

      BEGIN

         Enpassant := FALSE;
         PCT := Board[TF, FR].Piece_Cell;
         OS := PCT.Side_Cell;
         OM := PCT.Man_Cell;
         IF ( ( I1 > 5 ) AND ( OS > 0 ) ) THEN
            BEGIN
               OMP := Piece_Table[OS, OM].Move_Pattern;
               IF ( (I3 > 2) AND ( OMP > 5 ) AND NOT ( OS = S ) AND
                    (FR = (4 + (2 - S))) )
               THEN
                  BEGIN
                     IF ( ( Piece_Table[OS, OM].Moves = 1 ) AND
                          ( OS = Last_Piece_Moved.Side_Cell ) AND
                          ( OM = Last_Piece_Moved.Man_Cell ) )
                     THEN
                        BEGIN
                           WS := OS;
                           WM := OM;
                           Enpassant := TRUE
                        END
                  END
            END

      END;


   BEGIN

      N := 0;
      I := P^.IX;
      S := P^.SX;
      M := P^.PX;
      I := I + 1;
      Piece_Table[S, M].Mobility := 0;
      Piece_Table[S, M].Defending := 0;
      Piece_Table[S, M].Threats := 0;
      IF ( Piece_Table[S, M].Status_Cell <> 'C' ) THEN
         BEGIN
            I1 := Piece_Table[S, M].Move_Pattern;
            IF ( ( I1 = 1 ) AND ( Piece_Table[S, M].Moves = 0 ) )
            THEN
               BEGIN
                  N := Castling_Gen( P );
                  I := I + N
               END;
            Piece_Table[S, M].Pseudo_Status := ' ';
            I3 := 2;
            IF ( I1 > 5 ) THEN
               BEGIN
                  I1 := I1 + (S - 1);
                  I4 := MV[I1, 1].Inc1;
                  IF ( Piece_Table[S, M].Moves = 0 ) THEN
                     BEGIN
                        I2 := 2
                     END
                  ELSE
                     BEGIN
                        I2 := MV[I1, 1].Inc2
                     END
               END
            ELSE
               BEGIN
                  I4 := MV[I1, 1].Inc1;
                  I2 := MV[I1, 1].Inc2
               END;
            WHILE ( I3 <= I4 ) DO
               BEGIN
                  I6 := MV[I1, I3].Inc1;
                  I7 := MV[I1, I3].Inc2;
                  I8 := MV[I1, I3].Lim1;
                  I9 := MV[I1, I3].Lim2;
                  FF := Piece_Table[S, M].Position_Cell.File_Cell;
                  FR := Piece_Table[S, M].Position_Cell.Rank_Cell;
                  TF := FF + I6;
                  TR := FR + I7;
                  WHILE ( NOT(( TF = I8 ) OR ( TR = I9 )) AND
                         ( I2 > 0 ) ) DO
                     BEGIN
                        WS := 0;
                        WM := 0;
                        P^.Move_List[I].Mode_Cell := ' ';
                        P^.Move_List[I].Score_Cell := 0;
                        IF ( ( I1 <> 4 ) OR
                           NOT((( TF < 0 ) OR ( TF > 8 )) OR
                               (( TR < 0 ) OR ( TR > 8 ))) )
                        THEN
                           BEGIN
                              WS := Board[TF, TR].Piece_Cell.Side_Cell;
                              WM := Board[TF, TR].Piece_Cell.Man_Cell;
                              IF ( (WS <> 0) OR (Enpassant) )
                              THEN
                                 BEGIN
                                    IF ( NOT(( I1 > 5 ) AND
                                             ( I3 = 2 )) )
                                    THEN
                                       BEGIN
                                          IF ( NOT( WS = S ) )
                                          THEN
                                             BEGIN
                                                Piece_Table[S, M].
                                                   Threats :=
                                                   Piece_Table[S, M].
                                                      Threats + 1;
                                                IF ( WM = 1 ) THEN
                                                   BEGIN
                                                   Piece_Table[S, M].
                                                     Pseudo_Status := 'K';
                                                   Piece_Table[WS, WM].
                                                     Pseudo_Status := '!';
                                                   P^.Move_List[I].
                                                      Mode_Cell := 'M';
                                                   IF ( (( TR = 1 ) OR
                                                        ( TR = 8 )) AND
                                                        ( TR =
                                                        Piece_Table[WS, 1].
                                                        Position_Cell.
                                                        Rank_Cell ) )
                                                   THEN
                                                      BEGIN
                                                         Board[TF, TR].
                                                         Attack_Cell := 'A'
                                                      END
                                                   END
                                                ELSE
                                                   BEGIN
                                                   P^.Move_List[I].
                                                     Mode_Cell := 'C';
                                                   Piece_Table[S, M].
                                                     Pseudo_Status := 'A';
                                                   Piece_Table[WS, WM].
                                                     Pseudo_Status := 'T'
                                                   END;
                                                V1 := Piece_Table[WS, WM].
                                                      Move_Pattern;
                                                IF (I1 > 5) THEN
                                                   V2 := (I1-(S-1))
                                                ELSE
                                                   V2 := I1;
                                                IF ( (W[V1] - W[V2]) < 0 )
                                                THEN
                                                 BEGIN
                                                     P^.Move_List[I].
                                                          Score_Cell :=
                                                         (W[V1] - W[V2])
                                                 END
                                                ELSE
                                                 BEGIN
                                                  IF ( (Piece_Table[WS, WM].
                                                        Status_Cell = 'G') OR
                                                       (Piece_Table[WS, WM].
                                                        Pseudo_Status = 'G') )
                                                  THEN
                                                     BEGIN
                                                        P^.Move_List[I].
                                                           Score_Cell :=
                                                           (W[V1] - W[V2])
                                                           + Half_Pawn
                                                     END
                                                  ELSE
                                                     BEGIN
                                                       P^.Move_List[I].
                                                       Score_Cell := W[V1]
                                                     END;
                                                  P^.Move_List[I].
                                                  Score_Cell :=
                                                  P^.Move_List[I].
                                                     Score_Cell +
                                                     ( W[2] * 3 )
                                                 END
                                             END
                                       END
                                 END;
                                 IF ( WS = S ) THEN
                                    BEGIN
                                       IF ( NOT(( I1 > 5 ) AND
                                            ( I3 = 2 )) )
                                       THEN
                                          BEGIN
                                          Piece_Table[S, M].
                                                    Defending :=
                                          Piece_Table[S, M].
                                                    Defending + 1;
                                          Piece_Table[S, M].
                                                    Pseudo_Status := 'D';
                                          IF ( NOT( WM = 1 ) )
                                          THEN
                                             BEGIN
                                                Piece_Table[WS, WM].
                                                   Pseudo_Status := 'G'
                                             END
                                          END
                                    END
                                 ELSE
                                    BEGIN
                                    IF ( ( I1 < 6 ) OR ((I1 > 5) AND
                                         ( I3 = 2 ) AND ( WS = 0 )) OR
                                         (( I1 > 5 ) AND ( I3 > 2 ) AND
                                          ( WS <> 0 )) ) THEN
                                      BEGIN
                                      IF ( ( CH = ' ' ) OR (( CH = 'C' )
                                         AND (P^.Move_List[I].
                                              Mode_Cell = 'C')) )
                                      THEN
                                         BEGIN
                                            P^.IX := I;
                                            P^.MLI[I] := I;
                                            P^.Move_List[I].
                                               Move_Cell.From_Cell.
                                               File_Cell := FF;
                                            P^.Move_List[I].
                                               Move_Cell.From_Cell.
                                               Rank_Cell := FR;
                                            P^.Move_List[I].
                                               Move_Cell.To_Cell.
                                               File_Cell := TF;
                                            P^.Move_List[I].
                                               Move_Cell.To_Cell.
                                               Rank_Cell := TR;
                                            IF ( ( TR = 1 ) OR
                                                 ( TR = 8 ) ) THEN
                                               BEGIN
                                                  OS := S + (( S - 1 )
                                                        * -2 ) + 1;
                                                  IF ( TR =
                                                       Piece_Table[OS, 1].
                                                       Position_Cell.
                                                       Rank_Cell ) THEN
                                                     BEGIN
                                                        Board[TF, TR].
                                                        Attack_Cell
                                                               := 'A'
                                                     END
                                               END;
                                            Piece_Table[S, M].
                                                        Mobility :=
                                               Piece_Table[S, M].
                                                        Mobility + 1;
                                            P^.Move_List[I].
                                               Score_Cell :=
                                               P^.Move_List[I].
                                               Score_Cell
                                               + ((TR - FR) * (((S - 1)
                                               * -2 ) + 1) * 2);
                                            IF ( (Piece_Table[S, M].
                                                  Status_Cell = 'T') OR
                                                 (Piece_Table[S, M].
                                                  Pseudo_Status = 'T') )
                                            THEN
                                               BEGIN
                                                  P^.Move_List[I].
                                                            Score_Cell :=
                                                  P^.Move_List[I].
                                                            Score_Cell +
                                                            W[V1]
                                               END;
                                            IF ( (Piece_Table[S, M].
                                                  Status_Cell = '!') OR
                                                 (Piece_Table[S, M].
                                                  Pseudo_Status = '!') )
                                            THEN
                                               BEGIN
                                                  P^.Move_List[I].
                                                            Score_Cell :=
                                                  P^.Move_List[I].
                                                            Score_Cell +
                                                            ( Half_Pawn )
                                               END;
                                            P^.Move_List[I].Score_Cell :=
                                               P^.Move_List[I].Score_Cell
                                               + Piece_Table[S, M].
                                                        Development * 10;
                                            I := I + 1;
                                            N := N + 1;
                                            IF ( N > MaxBreadth ) THEN
                                               BEGIN
                                                  WRITELN('WARNING - ',
                                                  'BREADTH EXCEEDED ***',
                                                  Alarm);
                                                  I3 := I4 + 1
                                               END
                                         END
                                      END
                                    END
                           END;
                        I2 := I2 - 1;
                        IF ( Board[TF, TR].Piece_Cell.Side_Cell = 0 )
                           THEN
                              BEGIN
                                 TF := TF + I6;
                                 TR := TR + I7
                              END
                           ELSE
                              BEGIN
                                 TF := I8;
                                 TR := I9
                              END
                     END;
                  I2 := MV[I1, 1].Inc2;
                  I3 := I3 + 1
               END
         END;
      List_Gen := N

   END;

(********************************************************************)

PROCEDURE Dynamic_Eval( P : POINTER; Y : INTEGER );

   BEGIN

      WITH Piece_Table[ P^.SX, Y ] DO
         BEGIN
            IF ( ( Move_Cntrl = 'M' ) OR ( Move_Cntrl = 'm' ) )
            THEN
               Move_Cntrl := 'R'
            ELSE
               Move_Cntrl := ' ';
            IF ( Defending > 1 ) THEN
               Defending := Defending + (( Defending - 1 ) DIV 2);
            IF ( Threats > 1 ) THEN
               Threats := Threats + (( Threats - 1 ) DIV 2);                 
            IF ( ( Pseudo_Status = 'K' ) OR ( Pseudo_Status = 'k' ) )
            THEN
               BEGIN
                  TD[ P^.SX ].CK := TD[ P^.SX ].CK + 1
               END;
            TC[ P^.SX ].M := TC[ P^.SX ].M + Mobility;
            TC[ P^.SX ].D := TC[ P^.SX ].D + Defending;
            TC[ P^.SX ].T := TC[ P^.SX ].T + Threats;
            Mobility := 0;
            Defending := 0;
            Threats := 0       
         END
   END;

(********************************************************************)

PROCEDURE Sort(P:POINTER; W:INTEGER);

   VAR

      HI, LO : INTEGER;

procedure qsort(l,r: integer);

   var

     i, j : integer;
     x : integer;
     Hold : integer;

   begin

     i := l;
     j := r;
     x := P^.Move_List[P^.MLI[(l+r) DIV 2]].Score_Cell;
     repeat
        while ( P^.Move_List[P^.MLI[i]].Score_Cell > x ) do
           i := i + 1;
        while ( x > P^.Move_List[P^.MLI[j]].Score_Cell ) do
           j := j - 1;
        if ( i <= j ) then
           begin
              Hold := P^.MLI[i];
              P^.MLI[i] := P^.MLI[j];
              P^.MLI[j] := Hold;
              i := i + 1;
              j := j - 1
           end
     until ( i > j );
     if ( l < j ) then
        qsort(l, j);
     if ( i < r ) then
        qsort(i, r)

   end;

   BEGIN

      Lo := 1;
      Hi := W;
      Qsort(Lo, Hi)

   END;

(********************************************************************)

PROCEDURE Find_Refutation( P:POINTER );

   VAR

      I, J, K, L, N, W : INTEGER;
      V1, V2, V3, V4 : INTEGER;
      Found : BOOLEAN;
      PM, AM : POSITION;

   BEGIN

      Found := FALSE;
      V1 := Marker6;
      V2 := Marker4;
      V3 := Marker3;
      V4 := Marker2;
      L := P^.LX;
      W := P^.WX;
      J := 0;
      K := Root_Node^.IX;
      WHILE ( ( K > J ) AND NOT ( Found ) ) DO
         BEGIN
            N := Root_Node^.MLI[K];
            IF ( NOT ( RL[N, L].Move_Cell.From_Cell.File_Cell = 0 ) )
            THEN
               BEGIN
                  I := 0;                                                 
                  AM := RL[N, L];
                  WHILE ( ( I < W ) AND NOT(Found) ) DO
                     BEGIN
                        I := I + 1;
                        PM := P^.Move_List[P^.MLI[I]];
                        IF ( ( PM.Move_Cell.From_Cell.File_Cell =
                               AM.Move_Cell.From_Cell.File_Cell ) AND
                             ( PM.Move_Cell.From_Cell.Rank_Cell =
                               AM.Move_Cell.From_Cell.Rank_Cell ) AND
                             ( PM.Move_Cell.To_Cell.File_Cell =
                               AM.Move_Cell.To_Cell.File_Cell ) AND
                             ( PM.Move_Cell.To_Cell.Rank_Cell =
                               AM.Move_Cell.To_Cell.Rank_Cell ) )
                        THEN
                           BEGIN
                              IF ( K = Root_Node^.IX ) THEN
                                 BEGIN
                                    IF ( Root_Node^.IX = Root_Node^.BX )
                                    THEN
                                       BEGIN
                                          P^.Move_List[P^.MLI[I]].
                                             Score_Cell := 
                                             ((P^.Move_List[P^.MLI[I]].
                                               Score_Cell DIV 10) + V1)
                                       END
                                    ELSE
                                       BEGIN
                                          P^.Move_List[P^.MLI[I]].
                                             Score_Cell :=
                                             ((P^.Move_List[P^.MLI[I]].
                                               Score_Cell DIV 10) + V2)
                                       END
                                 END
                              ELSE
                                 BEGIN
                                    P^.Move_List[P^.MLI[I]].
                                       Score_Cell := 
                                       ((P^.Move_List[P^.MLI[I]].
                                         Score_Cell DIV 10) + V3)
                                 END;
                              Found := TRUE
                           END
                     END
               END;
            K := K - 1;
            IF ( ( K = 0 ) AND NOT( Found ) ) THEN
               BEGIN
                  J := Root_Node^.IX;
                  K := Root_Node^.WX
               END
         END;
      J := 0;
      K := MaxDepth;
      WHILE ( ( K > J ) AND NOT ( Found ) ) DO
         BEGIN
            J := J + 1;
            IF ( NOT ( PC[J, L].Move_Cell.From_Cell.File_Cell = 0 ) )
            THEN
               BEGIN
                  I := 0;
                  AM := PC[J, L];
                  WHILE (( I < W ) AND NOT(Found)) DO
                     BEGIN
                        I := I + 1;
                        PM := P^.Move_List[P^.MLI[I]];
                        IF ( ( PM.Move_Cell.From_Cell.File_Cell =
                               AM.Move_Cell.From_Cell.File_Cell ) AND
                             ( PM.Move_Cell.From_Cell.Rank_Cell =
                               AM.Move_Cell.From_Cell.Rank_Cell ) AND
                             ( PM.Move_Cell.To_Cell.File_Cell =
                               AM.Move_Cell.To_Cell.File_Cell ) AND
                             ( PM.Move_Cell.To_Cell.Rank_Cell =
                               AM.Move_Cell.To_Cell.Rank_Cell ) )
                        THEN
                           BEGIN
                              P^.Move_List[P^.MLI[I]].Score_Cell := 
                                 ((P^.Move_List[P^.MLI[I]].Score_Cell
                                   DIV 10) + V4);
                              Found := TRUE
                           END
                     END
               END
         END

   END;

(********************************************************************)

PROCEDURE Locate_Killer( P:POINTER );

   VAR

      I, L, X, Y, W : INTEGER;
      V1, V2, V3 : INTEGER;
      Found : BOOLEAN;
      MP, PW : POSITION;

   BEGIN

      V1 := Marker7;
      V2 := Marker6;
      V3 := Marker5;
      W := P^.WX;
      L := P^.LX;
      Y := 0;
      WHILE ( Y < Killers ) DO
          BEGIN
             Y := Y + 1;
             MP := KL[L, Y];
             Found := NOT( MP.Move_Cell.From_Cell.File_Cell > 0 );
             I := 0;
             WHILE ( ( I < W ) AND NOT(Found) ) DO
                BEGIN
                   I := I + 1;
                   PW := P^.Move_List[P^.MLI[I]];
                   Found := ( PW.Move_Cell.From_Cell.File_Cell =
                              MP.Move_Cell.From_Cell.File_Cell ) AND
                            ( PW.Move_Cell.From_Cell.Rank_Cell =
                              MP.Move_Cell.From_Cell.Rank_Cell ) AND
                            ( PW.Move_Cell.To_Cell.File_Cell =
                              MP.Move_Cell.To_Cell.File_Cell ) AND
                            ( PW.Move_Cell.To_Cell.Rank_Cell =
                              MP.Move_Cell.To_Cell.Rank_Cell );
                   IF Found THEN
                      BEGIN
                         IF ( MP.Mode_Cell = 'K' ) 
                         THEN
                            BEGIN
                               P^.Move_List[P^.MLI[I]].Score_Cell := 
                                  ((P^.Move_List[P^.MLI[I]].Score_Cell
                                    DIV 10) + V1)
                            END
                         ELSE
                            BEGIN
                               IF ( P^.Move_List[P^.MLI[I]].Score_Cell < V2 )
                               THEN
                                  BEGIN
                                     IF ( MP.Score_Cell < 200 ) THEN
                                        BEGIN
                                        P^.Move_List[P^.MLI[I]].Score_Cell :=
                                          ((P^.Move_List[P^.MLI[I]].
                                            Score_Cell DIV 10) + 
                                            ((V3 - 200) + MP.Score_Cell))
                                        END
                                     ELSE
                                        BEGIN
                                        P^.Move_List[P^.MLI[I]].Score_Cell :=
                                          ((P^.Move_List[P^.MLI[I]].
                                            Score_Cell DIV 10) + (V3 - 1))
                                        END
                                  END
                            END
                      END
                END;
             KL[L, Y].Mode_Cell := ' '
          END

   END;

(********************************************************************)

PROCEDURE Build_Refutation_Table( P : POINTER );

   VAR

      I, L : INTEGER;

   BEGIN

      I := P^.MLI[P^.IX];
      FOR L := 1 TO MaxDepth DO
          BEGIN
             RL[I, L] := PC[1, L]
          END;
      Clear_PC_List

   END;

(********************************************************************)

PROCEDURE Produce_Report( MM, OM : POSITION; ET : REAL; CD : CHAR );

   VAR

      MFF, MFR, MTF, MTR, OFF, OFR, OTF, OTR, MC : INTEGER;
      Sc, Hnd : INTEGER;
      Mn : INTEGER;
      ElapsedTime : REAL;
      S1, S2, S3, S4 : STR1;
      S03 : STR3;
      S11 : STR11;
      Line : STR80;
      M1, M2 : POSITION;

   BEGIN

      MC := Move_Cntr;
      IF ( MX = Black ) THEN
         BEGIN
            MC := MC + 1;
            M1 := MM;
            M2 := OM
         END
      ELSE
         BEGIN
            M1 := OM;
            M2 := MM
         END;
      Line := ' ';
      S11 := ' ';
      ElapsedTime := ET;
      Mn := TRUNC( ElapsedTime / 60 );
      ElapsedTime := ( ElapsedTime - ( Mn * 60 ));
      Sc := TRUNC( ElapsedTime );
      ElapsedTime := ( ElapsedTime - ( Sc ));
      Hnd := TRUNC( ElapsedTime * 100 );
      MFF := M1.Move_Cell.From_Cell.File_Cell;
      MFR := M1.Move_Cell.From_Cell.Rank_Cell;
      MTF := M1.Move_Cell.To_Cell.File_Cell;
      MTR := M1.Move_Cell.To_Cell.Rank_Cell;
      OFF := M2.Move_Cell.From_Cell.File_Cell;
      OFR := M2.Move_Cell.From_Cell.Rank_Cell;
      OTF := M2.Move_Cell.To_Cell.File_Cell;
      OTR := M2.Move_Cell.To_Cell.Rank_Cell;
      IF ( ( MFR = 0 ) AND ( OFR = 0 ) ) THEN
         BEGIN
            IF ( CD IN [ 'D', 'M', 'R', 'S', 'T' ] ) THEN
               BEGIN
                  STR( MC:2, S03 );
                  Line := '   '+S03;
                  CASE CD OF
                     'D' : S11 := '       DRAW';
                     'M' : S11 := '     MATED!';
                     'R' : S11 := '   RESIGNED';
                     'S' : S11 := '  STALEMATE';
                     'T' : S11 := ' TERMINATED'
                  END;
                  Line := Line+' '+S11
               END
         END
      ELSE
         BEGIN
            STR( MC:2, S03 );
            Line := '   '+S03;
            IF ( ( OFR = 0 ) AND ( MFR <> 0 ) ) THEN
               BEGIN
                  Line := Line+'   CONTINUED'
               END
            ELSE
               BEGIN
                  STR( OFR:1, S1 );
                  STR( OTR:1, S2 );
                  Line := Line+'     '+File_Vector[OFF]+S1+' ';
                  IF ( M2.Mode_Cell = 'C' ) THEN
                     BEGIN
                        Line := Line+'X '
                     END
                  ELSE
                     BEGIN
                        Line := Line+'- '
                     END;
                  Line := Line+File_Vector[OTF]+S2
               END;
            S11 := ' ';
            STR( Mn:4, S03 );
            S11 := S03+':';
            STR( Sc:2, S03 );
            S11 := S11+S03+'.';
            STR( Hnd:2, S03 );
            S11 := S11+S03;
            IF ( ( MFR = 0 ) AND ( CD = 'M' ) ) THEN
               BEGIN
                  Line := Line+'++  '+S11
               END
            ELSE
               BEGIN
                  IF ( CD = '1' ) THEN
                     BEGIN
                        Line := Line+'+   '+S11
                     END
                  ELSE
                     BEGIN
                        Line := Line+'    '+S11
                     END
               END;
            S11 := ' ';
            STR( RptScored:11:0, S11 );
            Line := Line+' '+S11+' ';
            S11 := ' ';
            STR( RptCutoffs:11:0, S11 );
            Line := Line+'  '+S11+'  ';
            IF ( MFR = 0 ) THEN
               BEGIN
                  IF ( CD IN [ 'D', 'M', 'R', 'S', 'T' ] ) THEN
                     BEGIN
                        CASE CD OF
                           'D' : S11 := '       DRAW';
                           'M' : S11 := '     MATED!';
                           'R' : S11 := '   RESIGNED';
                           'S' : S11 := '  STALEMATE';
                           'T' : S11 := ' TERMINATED'
                        END
                     END
                  ELSE
                     BEGIN
                        S11 := '    ## - ##'
                     END;
                  Line := Line+S11
               END
            ELSE
               BEGIN
                  STR( MFR:1, S3 );
                  STR( MTR:1, S4 );
                  Line := Line+'    '+File_Vector[MFF]+S3+' ';
                  IF ( M1.Mode_Cell = 'C' ) THEN
                     BEGIN
                        Line := Line+'X '
                     END
                  ELSE
                     BEGIN
                        Line := Line+'- '
                     END;
                  Line := Line+File_Vector[MTF]+S4;
                  IF ( CD = 'M' ) THEN
                     BEGIN
                        Line := Line+'++'
                     END
                  ELSE
                     BEGIN
                        IF ( CD = '2' ) THEN
                           BEGIN
                              Line := Line+'+'
                           END
                     END
               END
         END;
      Print( ' ', Line )

   END;

(********************************************************************)

PROCEDURE EOP;

   BEGIN

      Print( '1', ' ' );
      Print( '+', '          ***  END OF PROCESSING  ***' );
      Print( '1', ' ' )
											  
   END;

(********************************************************************)

(*==================================================================*)
(*              L E V E L   4   A L G O R I T H M S                 *)
(*==================================================================*)

(********************************************************************

FUNCTION Min( A, B : INTEGER ) : INTEGER;

   BEGIN

      IF ( A < B ) THEN
         Min := A
      ELSE
         Min := B

   END;

 ********************************************************************)

FUNCTION Max( A, B : INTEGER ) : INTEGER;

   BEGIN

      IF ( A > B ) THEN
         Max := A
      ELSE
         Max := B

   END;

(********************************************************************)

FUNCTION Create( P : POINTER ) : POINTER;

   VAR

      P1, P2 : POINTER;
      I : INTEGER;

   BEGIN

      NEW ( P1 );
      P1^.MLI[1] := 1;
      IF ( P = NIL ) THEN
         BEGIN
            P2 := NIL;
            P1^.Last := NIL;
            P1^.LX := 1;
            P1^.SX := MX;
            Clear_LVL_Cells ( P1 );
            P1^.Move_List[P1^.MLI[1]] := Void_Move
         END
      ELSE
         BEGIN
            P1^.LX := P^.LX + 1;
            P2 := P^.Next;
            P1^.Last := P;
            P^.Next := P1;
            P1^.SX := P^.SX + (( P^.SX - 1 ) * -2 ) + 1
         END;
      P1^.Next := P2;
      P1^.IX := 0;
      P1^.BX := 0;
      P1^.WX := 0;
      Create := P1

   END;

(********************************************************************)

PROCEDURE Drop( VAR P : POINTER );

   VAR

      P1 : POINTER;

   BEGIN

      IF ( P^.Last = NIL ) THEN
         BEGIN
            DISPOSE ( P )
         END
      ELSE
         BEGIN
            P1 := P^.Last;
            P1^.Next := P^.Next;
            DISPOSE ( P );
            P := P1
         END
         
   END;

(********************************************************************)

PROCEDURE Create_Root_Node( VAR P : POINTER );

   BEGIN

      Root_Node := NIL;
      P := Create( Root_Node );
      Root_Node := P

   END;

(********************************************************************)

FUNCTION Generate( P : POINTER ) : INTEGER;

   VAR

      C : CHAR;
      F, I, L, N, R, W, X, Y : INTEGER;

   BEGIN

      IF ( ( V[1, 1] - V[2, 1] ) = 0 )  
      THEN
         BEGIN
            N := 0;
            W := 0;
            C := ' ';
            X := P^.SX;
            L := P^.LX;
            P^.IX := 0;
            WITH TC[X] DO
               BEGIN
                  D := 0;
                  T := 0;
                  M := 0
               END;                                
            IF ( X = White ) THEN
               R := 8
            ELSE
               R := 1;
            FOR F := 1 TO 8 DO
                BEGIN
                   Board[F, R].Attack_Cell := ' '
                END;

    (* Generate moves for all peices *)

            FOR I := 1 to 16 DO
                BEGIN
                   Y := Order_Cell[I];
                   Piece_Table[ X, Y ].Mobility := 0;
                   Piece_Table[ X, Y ].Defending := 0;
                   Piece_Table[ X, Y ].Threats := 0;                          
                   IF ( NOT( (Piece_Table[X, Y].
                              Status_Cell = 'C') OR
                             (Piece_Table[X, Y].Position_Cell.
                              File_Cell = 0) ) ) THEN
                      BEGIN
                         P^.PX := Y;
                         N := List_Gen ( P, C );
                         IF ( ( N > 0 ) AND ( L > 0 ) ) THEN
                            Dynamic_Eval ( P, Y );
                         Piece_Table[X, Y].Status_Cell := ' ';
                         W := W + N
                      END
                END
         END
      ELSE
         BEGIN
   		P^.LX := P^.LX - 1; 	
            W := 0
         END;
     P^.IX := 1;
     P^.BX := 1;
     P^.WX := W;
     Generate := W

   END;

(********************************************************************)

PROCEDURE Make( P : POINTER );

   VAR

      WK, FM, TM : SQUARE;
      MV : POSITION;
      PCF, PCT : TABLE_INDEX;
      A, F, I, J, M, N, R, S, X, Y, Z : INTEGER;

   BEGIN

      I := P^.IX;
	P^.LVL.LXS := P^.LX;			
      MV := P^.Move_List[P^.MLI[I]];
      IF ( MV.Move_Cell.From_Cell.File_Cell > 0 ) THEN
         BEGIN

            (*  Save State Vectors  *)

            FM := MV.Move_Cell.From_Cell;
            TM := MV.Move_Cell.To_Cell;
            PCF := Board[FM.File_Cell, FM.Rank_Cell].Piece_Cell;
            PCT := Board[TM.File_Cell, TM.Rank_Cell].Piece_Cell;
            S := PCF.Side_Cell;
            M := PCF.Man_Cell;
            J := Piece_Table[S, M].Move_Pattern;
            P^.LVL.LPM := Last_Piece_Moved;
            P^.LVL.Move_Ctrl := Piece_Table[S, M].Move_Cntrl;
            P^.LVL.MPS := Piece_Table[S, M].Pseudo_Status;
            P^.LVL.MSC := Piece_Table[S, M].Status_Cell;
            P^.LVL.DEV := Piece_Table[S, M].Development;
            P^.LVL.TC2 := TC[S];
            P^.LVL.TD2 := TD[S];
		IF ( S = 0 ) THEN
               BEGIN
                  WRITE(' @@@@ MAKE WARNING - ');
                  WRITELN('FROM BOARD CELL EMPTY @@@@ ');
                  ErrorFlag := TRUE
               END;

            (*  Check for Capture En Passant  *)

            IF ( ( J > 5 ) AND (( MV.Mode_Cell = 'C' ) OR
                 ( NOT( FM.File_Cell = TM.File_Cell ))) AND
                 ( PCT.Side_Cell = 0 ) )
            THEN
               BEGIN
                  PCT := Board[TM.File_Cell, FM.Rank_Cell].Piece_Cell;
                  Board[TM.File_Cell, FM.Rank_Cell].
                                      Piece_Cell.Side_Cell := 0;
                  Board[TM.File_Cell, FM.Rank_Cell].
                                      Piece_Cell.Man_Cell := 0;
                  P^.LVL.ENP := 'E'
               END
            ELSE
               BEGIN
                  P^.LVL.ENP := ' '
               END;

            (*  Compute Advancement Value for a Major Piece  *)

            IF ( J < 6 ) THEN
               BEGIN
                  N := S + (( S - 1 ) * -2 ) + 1;
                  WK := Piece_Table[N, 1].Position_Cell;
                  A := ( WK.Rank_Cell - FM.Rank_Cell ) - ( WK.Rank_Cell -
                         TM.Rank_Cell );
                  IF ( FM.Rank_Cell > WK.Rank_Cell ) THEN
                     A := A * -1;
                  TD[S].AV := TD[S].AV + A;
                  A := ( WK.File_Cell - FM.File_Cell ) - ( WK.File_Cell -
                         TM.File_Cell );
                  IF ( FM.File_Cell > WK.File_Cell ) THEN
                     A := A * -1;
                  TD[S].AV := TD[S].AV + A
               END;

            (*  Update Piece Table and Development Value  *)

            TD[S].DV := TD[S].DV + Piece_Table[S, M].Development;
            IF ( Piece_Table[S, M].Development > 0 ) THEN
               Piece_Table[S, M].Development := 0;
            Piece_Table[S, M].Moves := Piece_Table[S, M].Moves + 1;
            Piece_Table[S, M].Status_Cell := Piece_Table[S, M].
                                                        Pseudo_Status;
            IF ( Piece_Table[S, M].Move_Cntrl = 'R' ) THEN
               BEGIN
                  TD[S].DV := TD[S].DV - AI       
               END;

            (*  Check for Capture  *)

            IF ( PCT.Side_Cell > 0 )
            THEN
               BEGIN

            (*  Remove Captured Piece  *)

                  TD[S].CP := TD[S].CP + 1;
                  X := PCT.Side_Cell;
                  Y := PCT.Man_Cell;
                  P^.LVL.To_Position := PCT;
                  P^.LVL.OPS := Piece_Table[X, Y].Pseudo_Status;
                  P^.LVL.OSC := Piece_Table[X, Y].Status_Cell;
                  Piece_Table[X, Y].Status_Cell := 'C';
                  Piece_Table[X, Y].Pseudo_Status := 'C';
                  Piece_Table[X, Y].Position_Cell.File_Cell := 0;
                  Piece_Table[X, Y].Position_Cell.Rank_Cell := 0;
                  Z := Piece_Table[X, Y].Move_Pattern;
                  V[X, Z] := V[X, Z] - 1
               END
            ELSE
               BEGIN
                  P^.LVL.To_Position.Side_Cell := 0;
                  P^.LVL.To_Position.Man_Cell := 0
               END;

            (*  If Pawn, Compute Pawn Advancement Value  *)

            IF ( J > 5 ) THEN
               BEGIN
                  IF ( S = 2 ) THEN
                     TD[S].AV := TD[S].AV + (( FM.Rank_Cell -
                                               TM.Rank_Cell ) * AI)
                  ELSE
                     TD[S].AV := TD[S].AV + (( TM.Rank_Cell -
                                               FM.Rank_Cell ) * AI);

            (*  Promote Piece if Pawn is on Last Rank  *)

                  IF ( ( TM.Rank_Cell = 1 ) OR ( TM.Rank_Cell = 8 ) )
                  THEN
                     BEGIN
                        Piece_Table[S, M].Move_Pattern := 2;
                        Piece_Table[S, M].Status_Cell := ' ';
                        Piece_Table[S, M].Pseudo_Status := 'P';
                        P^.Move_List[P^.MLI[I]].Mode_Cell := 'P';
                        V[S, 6] := V[S, 6] - 1;
                        V[S, 2] := V[S, 2] + 1
                     END
               END;

            (*  Make Primary Move on Board  *)

            Board[TM.File_Cell, TM.Rank_Cell].Piece_Cell := PCF;
            Piece_Table[S, M].Position_Cell := TM;
            Board[FM.File_Cell, FM.Rank_Cell].Piece_Cell.Side_Cell := 0;
            Board[FM.File_Cell, FM.Rank_Cell].Piece_Cell.Man_Cell := 0;
            Board[FM.File_Cell, FM.Rank_Cell].Attack_Cell := ' ';
            Last_Piece_Moved := PCF;

            (*  Perform Castling Move  *)

            IF (( M = 1 ) AND ( Piece_Table[S, M].Moves = 1 ) AND
                (( P^.Move_List[P^.MLI[I]].Mode_Cell IN ['K', 'Q'] ) OR
                 (( FM.File_Cell = 5 ) AND
                 (( TM.File_Cell = 3 ) OR ( TM.File_Cell = 7 )))))
            THEN
               BEGIN
                  IF ((NOT(Piece_Table[S, 3].Status_Cell = 'C')) AND
                     (Board[8, FM.Rank_Cell].Piece_Cell.Side_Cell = S) AND
                     (Board[8, FM.Rank_Cell].Piece_Cell.Man_Cell = 3) AND
                     (Piece_Table[S, 3].Moves = 0) AND
                     (( P^.Move_List[P^.MLI[I]].Mode_Cell = 'K' ) OR
                     ( TM.File_Cell = 7 )))
                  THEN
                     BEGIN
                        Board[6, TM.Rank_Cell].Piece_Cell :=
                                      Board[8, FM.Rank_Cell].Piece_Cell;
                        Board[6, FM.Rank_Cell].Attack_Cell := ' ';
                        Piece_Table[S, 3].Position_Cell.File_Cell := 6;
                        Piece_Table[S, 3].Position_Cell.Rank_Cell :=
                                                           TM.Rank_Cell;
                        Board[8, FM.Rank_Cell].Piece_Cell.Side_Cell := 0;
                        Board[8, FM.Rank_Cell].Piece_Cell.Man_Cell := 0;
                        Board[8, FM.Rank_Cell].Attack_Cell := ' ';
                        Piece_Table[S, 3].Moves :=
                                             Piece_Table[S, 3].Moves + 1;
                        P^.LVL.DEV2 := Piece_Table[S, 3].Development;
                        P^.LVL.DEV3 := Piece_Table[S, 16].Development;
                        P^.LVL.DEV4 := Piece_Table[S, 13].Development;
                        P^.LVL.DEV5 := Piece_Table[S, 11].Development;
                        Piece_Table[S, 3].Development := -1;
                        Piece_Table[S, 16].Development := -1;
                        Piece_Table[S, 13].Development := -5;
                        Piece_Table[S, 11].Development := -3;
                        TD[S].DV := TD[S].DV + Three_Quarter_Pawn
                     END
                  ELSE
                     IF ((NOT(Piece_Table[S, 4].Status_Cell = 'C')) AND
                        (Board[1,FM.Rank_Cell].Piece_Cell.Side_Cell=S) AND
                        (Board[1,FM.Rank_Cell].Piece_Cell.Man_Cell=4) AND
                        (Piece_Table[S, 4].Moves = 0) AND
                        (( P^.Move_List[P^.MLI[I]].Mode_Cell = 'Q' ) OR
                        ( TM.File_Cell = 3 )))
                     THEN
                        BEGIN
                           Board[4, TM.Rank_Cell].Piece_Cell :=
                                      Board[1, FM.Rank_Cell].Piece_Cell;
                           Board[4, FM.Rank_Cell].Attack_Cell := ' ';
                           Piece_Table[S, 4].Position_Cell.File_Cell := 4;
                           Piece_Table[S, 4].Position_Cell.Rank_Cell :=
                                                           TM.Rank_Cell;
                           Board[1, FM.Rank_Cell].Piece_Cell.
                                                           Side_Cell := 0;
                           Board[1, FM.Rank_Cell].Piece_Cell.
                                                           Man_Cell := 0;
                           Board[1, FM.Rank_Cell].Attack_Cell := ' ';
                           Piece_Table[S, 4].Moves :=
                                            Piece_Table[S, 4].Moves + 1;
                           P^.LVL.DEV2 := Piece_Table[S, 4].Development;
                           P^.LVL.DEV3 := Piece_Table[S, 15].Development;
                           P^.LVL.DEV4 := Piece_Table[S, 14].Development;
                           P^.LVL.DEV5 := Piece_Table[S, 12].Development;
                           Piece_Table[S, 4].Development := -1;
                           Piece_Table[S, 15].Development := -1;
                           Piece_Table[S, 14].Development := -3;
                           Piece_Table[S, 12].Development := -3;
                           TD[S].DV := TD[S].DV + Half_Pawn
                       END
                    ELSE
                       BEGIN
                          WRITELN(' @@@@ MAKE WARNING - ',
                                  'ROOK CASTLING FAILURE @@@@ '+Alarm)
                       END
               END
         END
      ELSE
         BEGIN
            WRITELN(' @@@@ MAKE WARNING - MOVE CELL EMPTY @@@@ ');
            ErrorFlag := TRUE
         END

   END;

(********************************************************************)

PROCEDURE Undo( P : POINTER );

   VAR

      WK, FM, TM : SQUARE;
      MV : POSITION;
      PCW, PCF, PCT : TABLE_INDEX;
      N, I, S, M, X, Y : INTEGER;

   BEGIN

	I := P^.IX;
	P^.LX := P^.LVL.LXS;		
      MV := P^.Move_List[P^.MLI[I]];
      IF ( MV.Move_Cell.From_Cell.File_Cell > 0 ) THEN
         BEGIN

            (*  Restore State Vectors  *)

            FM := MV.Move_Cell.From_Cell;
            TM := MV.Move_Cell.To_Cell;
            PCT := Board[TM.File_Cell, TM.Rank_Cell].Piece_Cell;
            PCW := P^.LVL.To_Position;
            Board[TM.File_Cell, TM.Rank_Cell].Piece_Cell.Side_Cell := 0;
            Board[TM.File_Cell, TM.Rank_Cell].Piece_Cell.Man_Cell := 0;
            Board[TM.File_Cell, TM.Rank_Cell].Attack_Cell := ' ';
            S := PCT.Side_Cell;
            M := PCT.Man_Cell;
            X := PCW.Side_Cell;
            Y := PCW.Man_Cell;
            IF ( S = 0 ) THEN
               BEGIN
                  WRITE(' @@@@ UNDO WARNING - ');
                  WRITELN('TO BOARD CELL EMPTY @@@@ ');
                  ErrorFlag := TRUE
               END;
            Piece_Table[S, M].Status_Cell := P^.LVL.MSC;
            Piece_Table[S, M].Move_Cntrl := P^.LVL.Move_Ctrl;
            TC[S] := P^.LVL.TC2;
            TD[S] := P^.LVL.TD2;
            Piece_Table[S, M].Moves := Piece_Table[S, M].Moves - 1;
            Piece_Table[S, M].Development := P^.LVL.DEV;
            Last_Piece_Moved := P^.LVL.LPM;

            (*  Test for Captured Piece  *)

            IF ( X > 0 ) THEN
               BEGIN
                  N := Piece_Table[X, Y].Move_Pattern;
                  V[X, N] := V[X, N] + 1;
                  Piece_Table[X, Y].Pseudo_Status := P^.LVL.OPS;
                  Piece_Table[X, Y].Status_Cell := P^.LVL.OSC;

            (*  Test for Captured En Passant  *)

                  IF ( P^.LVL.ENP = 'E' )
                  THEN
                     BEGIN
                        TM.Rank_Cell := FM.Rank_Cell;
                        P^.LVL.ENP := ' '
                     END;

            (*  Restore Captured Piece  *)

                  Piece_Table[X, Y].Position_Cell := TM;
                  Board[TM.File_Cell, TM.Rank_Cell].
                                                Piece_Cell := PCW;
                  Board[TM.File_Cell, TM.Rank_Cell].
                                                Attack_Cell := ' ';
                  P^.LVL.To_Position.Side_Cell := 0;
                  P^.LVL.To_Position.Man_Cell := 0
               END;

            (*  Restore Promoted Piece  *)

            IF ( ( MV.Mode_Cell = 'P' ) OR
                 ( Piece_Table[S, M].Pseudo_Status = 'P' ) ) THEN
               BEGIN
                  Piece_Table[S, M].Move_Pattern := 6;
                  V[S, 2] := V[S, 2] - 1;
                  V[S, 6] := V[S, 6] + 1
               END;

            (*  Restore Primary Move on Board  *)

            Piece_Table[S, M].Pseudo_Status := ' ';
            Piece_Table[S, M].Position_Cell := FM;
            Board[FM.File_Cell, FM.Rank_Cell].Piece_Cell := PCT;

            (*  Restore Castling Move  *)

            IF (( M = 1 ) AND ( Piece_Table[S, M].Moves = 0 ) AND
                (( P^.Move_List[P^.MLI[I]].Mode_Cell IN ['K', 'Q'] ) OR
                 (( FM.File_Cell = 5 ) AND
                 (( TM.File_Cell = 3 ) OR ( TM.File_Cell = 7 )))))
            THEN
               BEGIN
                  IF ((NOT(Piece_Table[S, 3].Status_Cell = 'C')) AND
                     (Board[6, TM.Rank_Cell].Piece_Cell.Side_Cell = S) AND
                     (Board[6, TM.Rank_Cell].Piece_Cell.Man_Cell = 3) AND
                     (Piece_Table[S, 3].Moves = 1) AND
                     (( P^.Move_List[P^.MLI[I]].Mode_Cell = 'K' ) OR
                     ( TM.File_Cell = 7 )))
                  THEN
                     BEGIN
                        Board[8, FM.Rank_Cell].Piece_Cell :=
                                      Board[6, TM.Rank_Cell].Piece_Cell;
                        Board[8, FM.Rank_Cell].Attack_Cell := ' ';
                        Piece_Table[S, 3].Position_Cell.File_Cell := 8;
                        Piece_Table[S, 3].Position_Cell.Rank_Cell :=
                                                           FM.Rank_Cell;
                        Board[6, TM.Rank_Cell].Piece_Cell.Side_Cell := 0;
                        Board[6, TM.Rank_Cell].Piece_Cell.Man_Cell := 0;
                        Board[6, TM.Rank_Cell].Attack_Cell := ' ';
                        Piece_Table[S, 3].Status_Cell := ' ';
                        Piece_Table[S, 3].Pseudo_Status := ' ';
                        Piece_Table[S, 3].Development := P^.LVL.DEV2;
                        Piece_Table[S, 16].Development := P^.LVL.DEV3;
                        Piece_Table[S, 13].Development := P^.LVL.DEV4;
                        Piece_Table[S, 11].Development := P^.LVL.DEV5;
                        Piece_Table[S, 3].Moves :=
                                             Piece_Table[S, 3].Moves - 1
                     END
                  ELSE
                     IF ((NOT(Piece_Table[S, 4].Status_Cell = 'C')) AND
                        (Board[4,TM.Rank_Cell].Piece_Cell.Side_Cell=S) AND
                        (Board[4,TM.Rank_Cell].Piece_Cell.Man_Cell=4) AND
                        (Piece_Table[S, 4].Moves = 1) AND
                        (( P^.Move_List[P^.MLI[I]].Mode_Cell = 'Q' ) OR
                        ( TM.File_Cell = 3 )))
                     THEN
                        BEGIN
                           Board[1, FM.Rank_Cell].Piece_Cell :=
                                      Board[4, TM.Rank_Cell].Piece_Cell;
                           Board[1, FM.Rank_Cell].Attack_Cell := ' ';
                           Piece_Table[S, 4].Position_Cell.File_Cell := 1;
                           Piece_Table[S, 4].Position_Cell.Rank_Cell :=
                                                          FM.Rank_Cell;
                           Board[4, TM.Rank_Cell].Piece_Cell.
                                                          Side_Cell := 0;
                           Board[4, TM.Rank_Cell].Piece_Cell.
                                                          Man_Cell := 0;
                           Board[4, TM.Rank_Cell].Attack_Cell := ' ';
                           Piece_Table[S, 4].Status_Cell := ' ';
                           Piece_Table[S, 4].Pseudo_Status := ' ';
                           Piece_Table[S, 4].Development := P^.LVL.DEV2;
                           Piece_Table[S, 15].Development := P^.LVL.DEV3;
                           Piece_Table[S, 14].Development := P^.LVL.DEV4;
                           Piece_Table[S, 12].Development := P^.LVL.DEV5;
                           Piece_Table[S, 4].Moves :=
                                             Piece_Table[S, 4].Moves - 1
                       END
                    ELSE
                       BEGIN
                          WRITELN(' @@@@ UNDO WARNING - ',
                                  'ROOK CASTLING FAILURE @@@@ '+Alarm)
                       END
               END
         END
      ELSE
         BEGIN
            WRITELN(' @@@@ UNDO WARNING - MOVE CELL EMPTY @@@@ ');
            ErrorFlag := TRUE
         END

   END;

(********************************************************************)

FUNCTION Eval( P : POINTER ) : INTEGER;

   VAR   LX, V1, V2, V3, X1, X2 : INTEGER;

   BEGIN

      LX := P^.LX;
      IF ( Level < 3 ) THEN
         BEGIN
            LX := LX - 1
         END;

      (*  Establish Index of Side on the Move  *)

      X1 := P^.SX;                                                     
      X2 := X1 + (( X1 - 1 ) * -2 ) + 1;
      
	(*  Determine King Status  *)

	IF ( ( ( V[X1, 1] - V[X2, 1] ) = 0 ) OR ( LX > 3 ) ) THEN

	(*  If Game has not been won or lost yet then: *)

	   BEGIN                              

      (*  Compute Material Balance  *)

     		V1 := (( KV DIV 2 ) * ( V[X1, 1] - V[X2, 1] )) +
            	(QNV * ( V[X1, 2] - V[X2, 2] )) +
            	(RV * ( V[X1, 3] - V[X2, 3] )) +
            	(NV * ( V[X1, 4] - V[X2, 4] )) +
            	(BV * ( V[X1, 5] - V[X2, 5] )) +
            	(PNV * ( V[X1, 6] - V[X2, 6] ));

      (*  Apply Material Exchange Heuristic  *)

      	V2 := (CpHv * ( TD[X1].CP - TD[X2].CP ));
      	IF ( ( V1 < 0 ) AND ( V2 > 0 ) ) THEN
         	   V2 := V2 * -1;

      (*  Apply Positional Heuristics  *)

      	V3 := ((DvHv * ( TD[X1].DV - TD[X2].DV )) +
             	 (CkHv * ( TD[X1].CK - TD[X2].CK )) +
             	 (AvHv * ( TD[X1].AV - TD[X2].AV )) +
             	 (DHv * ( TC[X1].D - TC[X2].D )) +
             	 (THv * ( TC[X1].T - TC[X2].T )) +
              	 (MHv * ( TC[X1].M - TC[X2].M )))

	   END

	ELSE

	(*  King has Fallen  *)

	   BEGIN

		V1 := ( ( V[X1, 1] - V[X2, 1] ) * ( KV  - ( LX * PNV ) ) );
		V2 := 0;
		V3 := 0

	   END;
 

      (*  Combine Static Heuristic Values into Score  *)

      Eval := V1 + V2 + V3;
      Scored := Scored + 1

   END;

(********************************************************************)

PROCEDURE StripBlanks( VAR S : STR80 );

BEGIN

   WHILE ( S [ LENGTH( S ) ] = ' ' ) DO
      DELETE ( S, LENGTH( S ), 1 )

END;

(********************************************************************)

PROCEDURE UpCaseStr( VAR S : STR80 );

VAR

   I : INTEGER;

BEGIN

   FOR I := 1 TO LENGTH( S ) DO
       S[ I ] := UpCase( S [ I ] )

END;

(********************************************************************)

PROCEDURE Save_Killer( P:POINTER );

   VAR

      K, I, J, L : INTEGER;
      Index_Cell : INTEGER;
      Hold : POSITION;
      Found : BOOLEAN;

   BEGIN

      Found := FALSE;
      K := Killers;
      L := P^.LX;
      J := P^.IX;
      I := 0;
      WHILE ( I < K ) DO
         BEGIN
            I := I + 1;
            KL[L, I].Mode_Cell := ' ';
            IF NOT( KL[L, I].Score_Cell < Killer_Decay ) THEN
               BEGIN        (* Cause Statistical Decay *)
                  KL[L, I].Score_Cell := KL[L, I].Score_Cell -
                                         Killer_Decay
               END;
            IF ( NOT (Found) ) THEN
               BEGIN
                  IF (KL[L, I].Move_Cell.From_Cell.File_Cell = 0)
                  THEN
                     BEGIN  (* If Empty Element, Add New Killer to List *)
                        KL[L, I] := P^.Move_List[P^.MLI[J]];
                        KL[L, I].Score_Cell := Killer_Value;
                        KL[L, I].Mode_Cell := 'K';
                        Found := TRUE
                     END
                  ELSE
                     BEGIN (* Match killer to move in killer list *)
                        IF ((KL[L, I].Move_Cell.From_Cell.File_Cell =
                           P^.Move_List[P^.MLI[J]].
                           Move_Cell.From_Cell.File_Cell)
                           AND ( KL[L, I].Move_Cell.From_Cell.Rank_Cell =
                           P^.Move_List[P^.MLI[J]].
                           Move_Cell.From_Cell.Rank_Cell)
                           AND ( KL[L, I].Move_Cell.To_Cell.File_Cell =
                           P^.Move_List[P^.MLI[J]].
                           Move_Cell.To_Cell.File_Cell)
                           AND ( KL[L, I].Move_Cell.To_Cell.Rank_Cell =
                           P^.Move_List[P^.MLI[J]].
                           Move_Cell.To_Cell.Rank_Cell))
                        THEN
                           BEGIN   (* Increase Statistical Killer Value *)
                              KL[L, I].Score_Cell :=
                                       KL[L, I].Score_Cell +
                                       Killer_Augment;
                              KL[L, I].Mode_Cell := 'K';
                              Found := TRUE
                           END
                     END
               END
         END;
      I := 1;
      IF ( NOT(Found) ) THEN
         BEGIN  (* If Not Found, Replace Least Active Killer With New *)
            Hold := KL[L, I];
            Index_Cell := I;
            FOR I := 1 TO K DO
                BEGIN
                   IF ( KL[L, I].Score_Cell < Hold.Score_Cell ) THEN
                      BEGIN
                         Hold := KL[L, I];
                         Index_Cell := I
                      END
                END;
            I := Index_Cell;
            KL[L, I] := P^.Move_List[P^.MLI[J]];
            KL[L, I].Score_Cell := Killer_Value;
            KL[L, I].Mode_Cell := 'K';
            Found := TRUE
         END

   END;

(********************************************************************)

PROCEDURE Save_Refutation( P : POINTER );

   VAR

      I, L, R : INTEGER;

   BEGIN

      L := P^.LX;
      I := P^.IX;
      R := L;     (* Add New Refutation to Triangular Work Array *)
      PC[R, L] := P^.Move_List[P^.MLI[I]];
      FOR L := (L + 1) TO MaxDepth DO      
          BEGIN   (* At Level + 1, Move Elements Up 1 Row to MaxDepth *)
             PC[R, L] := PC[(R + 1), L]
          END

   END;

(********************************************************************)

PROCEDURE Locate_Refutations( P:POINTER );

   BEGIN

      Find_Refutation( P );
      Locate_Killer( P )

   END;

(********************************************************************)

FUNCTION Compute_Clock : REAL;

VAR

   RemainingMoves : INTEGER;
   RemainingTime, Base_Time : REAL;

BEGIN

   IF (( ( AllocatedTime - ElapsedClock ) <= 0 ) OR
       ( ( ( AllocatedMoves ) - Move_Cntr ) <= 0 )) THEN
      BEGIN
         AllocatedTime := AllocatedTime + 3600;
         AllocatedMoves := AllocatedMoves + 20
      END;
   RemainingTime := AllocatedTime - ElapsedClock;
   RemainingMoves := ( AllocatedMoves + 1 ) - Move_Cntr;
   IF ( RemainingMoves = 0 ) THEN
      RemainingMoves := 1;
   Base_Time := RemainingTime / RemainingMoves;
   IF ( Base_Time > 0 ) THEN
      BEGIN
         Compute_Clock := Base_Time;
         IF ( Report ) THEN
            BEGIN
               WRITELN( 'MOVE CLOCK = ', Base_Time:1:2,
                        '   REMAINING TIME = ', RemainingTime:1:2,
                        '   REMAINING MOVES = ', RemainingMoves:1 );
               WRITELN( 'ELAPSED CLOCK = ', ElapsedClock:1:2,
                        '   MOVES EXECUTED = ', Move_Cntr:1 );
               IF ( Plot ) THEN
                  WRITELN
            END
      END
   ELSE
      BEGIN
         Compute_Clock := 180;
         IF ( Report ) THEN
            WRITELN( 'WARNING:  MOVE CLOCK SET AT THREE MINUTES' )
      END

END;

(********************************************************************)

FUNCTION Check_Clock( T : REAL; SCORE : INTEGER ) : BOOLEAN;

VAR

   H, M, S, C : INTEGER;
   ET : REAL;
   Logic_Value : BOOLEAN;

BEGIN

   Timer('E', H, M, S, C, Et);
   IF ( Et < ( T / 2 ) )
   THEN
      BEGIN
         Logic_Value := (( Move_Cntr = 0 ) AND ( Iteration > 1 ) AND
                         (( MX = White ) OR ( Iteration > 3 )) AND
                         (NOT((Piece_Table[MX, 1].Pseudo_Status = '!')
                          OR (Piece_Table[MX, 1].Status_Cell = '!')
                          OR (Piece_Table[OX, 1].Pseudo_Status = '!')
                          OR (Piece_Table[OX, 1].Status_Cell = '!')
                          OR (Piece_Table[OX, 1].Pseudo_Status = 'C'))))
      END
   ELSE
      BEGIN
         IF ( Et < T ) THEN
            BEGIN
               Logic_Value := (((Root_Node^.Move_List
                              [Root_Node^.MLI[Root_Node^.BX]].Mode_Cell
                              IN ['C', 'P'] ) AND
                              (Root_Node^.Move_List
                              [Root_Node^.MLI[1]].Mode_Cell
                              IN ['C', 'P'] ) AND ( Iteration > 1 ))
                              OR (( Move_Cntr < 5 ) AND
                              ( Iteration > 2 )) AND
                              (NOT((Piece_Table[MX,1].Pseudo_Status = '!')
                               OR (Piece_Table[MX, 1].Status_Cell = '!')
                               OR (Piece_Table[OX, 1].Pseudo_Status = '!')
                               OR (Piece_Table[OX, 1].Status_Cell = '!')
                               OR (Piece_Table[OX,1].Pseudo_Status='C'))))
            END
         ELSE
            BEGIN
               IF ( ( Et < ( T * 2 ) ) AND
                    (( T * 2 ) < ( AllocatedTime - ElapsedClock )) )
               THEN
                  BEGIN
                     IF ( ( Score = Last_Score ) OR
                          ( Move_Cntr < 1 ) )
                     THEN
                        BEGIN
                           Logic_Value := TRUE
                        END
                     ELSE
                        BEGIN
                           Logic_Value := (((( Root_Node^.Move_List
                             [Root_Node^.MLI[Root_Node^.BX]].Score_Cell
                             + PNV ) >
                             Max( Shallow_Score,
                                  Max( Last_Score, Last_Move_Score ))) AND
                             NOT((Piece_Table[MX,1].Pseudo_Status = '!')
                             OR (Piece_Table[MX, 1].Status_Cell = '!')))
                             OR (T < 90.0))
                        END
                  END
               ELSE
                  BEGIN
                     IF ( ( Et < ( T * 4 ) ) AND
                          (( T * 4 ) < ( AllocatedTime - ElapsedClock )) )
                     THEN
                        BEGIN
                           Logic_Value := (((( Root_Node^.Move_List
                             [Root_Node^.MLI[Root_Node^.BX]].Score_Cell
                             + PNV ) >
                             Max( Shallow_Score,
                                  Max( Last_Score, Last_Move_Score ))) AND
                             NOT((Piece_Table[MX,1].Pseudo_Status = '!')
                             OR (Piece_Table[MX, 1].Status_Cell = '!')))
                             OR (T < 90.0))
                        END
                     ELSE
                        BEGIN
                           Logic_Value := TRUE
                        END
                  END
            END
      END;
   Check_Clock := Logic_Value

END;

(********************************************************************)

(*  Decrementor for Depth Value in Quiescent NFAB Search Function  *)

FUNCTION Decrement( P:POINTER; N:INTEGER ) : INTEGER;

   VAR

      D, I, L, S, X : INTEGER;

   BEGIN

      IF ( N > Iteration ) THEN
         BEGIN
            WRITELN( ' !!!!  WARNING - NFAB ERROR: (N > Iteration) !!!! ',
                     Alarm )
         END;

      S := P^.SX;
      I := P^.IX;
      L := P^.LX;
      D := N - 1;
      X := S + (( S - 1 ) * -2 ) + 1;

   (* Test for Quiescent Move Generation *)

      IF ( ( Extended ) AND ( N = 1 ) ) THEN
         BEGIN
            IF ( ( L = Horizon ) OR ( L = Quiescence ) OR
                 ( P^.Move_List[P^.MLI[I]].Mode_Cell = 'M' ) )
            THEN

               (* Mated or Search Horizon Reached *)

               BEGIN
                  D := 0
               END
            ELSE
               BEGIN
                  IF ( P^.Move_List[P^.MLI[I]].Mode_Cell IN ['C', 'P'] )
                  THEN
                                                               
                     (* Captures and Promotions *)

                     BEGIN
                        D := N
                     END
                  ELSE
                     BEGIN

                     (* Checks *)

                        IF ( (Piece_Table[X, 1].Pseudo_Status = '!') OR
                             (Piece_Table[S, 1].Pseudo_Status = '!') )
                        THEN
                           BEGIN
                              IF ( L > (Iteration + Check_Extension) )
                              THEN
                                 BEGIN
                                    D := N - 1
                                 END
                              ELSE
                                 BEGIN
                                    D := N
                                 END
                           END
                        ELSE
                           BEGIN
                              D := N - 1
                           END
                     END
               END
         END
      ELSE
         BEGIN
            D := N - 1
         END;

      Decrement := D

   END;

(*********************************************************************)

FUNCTION King( P : POINTER; S : INTEGER ) : INTEGER;

   CONST

        Safe = 0;
        CheckMate = 1;
        StaleMate = 2;
        Check = 3;
        InCheck = '!';
       
   VAR

        P1 : POINTER;
        Ply, Score, OS, Status, Width, Decision_Value  : INTEGER;
        Defeated : BOOLEAN;

   BEGIN

      IF ( S = MX ) THEN
         BEGIN                                  
            Ply := 2
         END
      ELSE
         BEGIN                          
            Ply := 3
         END;
                       
      Score := 0;

	Decision_Value := KV - ( Ply * PNV);

      IF ( NOT ( P = NIL ) ) THEN
         BEGIN
            Score := P^.Move_List[P^.MLI[1]].Score_Cell                 
         END;

      OS := S + (( S - 1 ) * -2 ) + 1;
      Status := Safe;
      P1 := Create( NIL );
      P1^.SX := OS;
      P1^.LX := 0;
      Piece_Table[S, 1].Pseudo_Status := ' ';
      Width := Generate( P1 );
      Drop( P1 );

      Defeated := ( ( NOT ( P = NIL ) ) AND ( P^.LX = 1 ) AND
                    ( ABS ( Score ) = Decision_Value ) );


      IF ( Piece_Table[S, 1].Pseudo_Status = InCheck ) THEN
         BEGIN
            IF ( ( Defeated ) AND
                 ( (( S = MX ) AND ( Score < 0 )) OR
                   (( S = OX ) AND ( Score > 0 )) ) ) THEN
               BEGIN
                  Status := CheckMate
               END
            ELSE
               BEGIN
                  Status := Check
               END
         END
      ELSE
         BEGIN
            IF ( ( Defeated ) AND
                 ( (( S = MX ) AND ( Score < 0 )) OR
                   (( S = OX ) AND ( Score > 0 )) ) ) THEN
               BEGIN
                  Status := StaleMate
               END
         END;
      King := Status

   END;

(********************************************************************)

(*==================================================================*)
(*              L E V E L   3   A L G O R I T H M S                 *)
(*==================================================================*)

(********************************************************************)

PROCEDURE Set_Level( L : INTEGER );

   BEGIN

      Level := L;
      Extended := TRUE;
      Tournament := FALSE;

      IF ( L IN [ 1 .. 9 ] ) THEN
         BEGIN

            Horizon := MaxSearch;         
            Quiescence := MaxSearch;
            Full_Depth := L;
      
            CASE L OF

            1 .. 4, 
               8 : BEGIN
                        
                      IF ( L < 5 ) THEN
                         BEGIN 
                            Full_Depth := ( L + 1 )
                         END
                      ELSE
                         BEGIN 
                            Full_Depth := ( L - 1 )
                         END;   
                      Plot := ( Full_Depth = FW_Limit )

                   END;

            
            5, 7 : BEGIN

                      Horizon := MaxDepth;
                      Quiescence := MaxDepth;
                      IF ( L = 7 ) THEN
                         BEGIN
                            Full_Depth := ( L - 1 )
                         END
                      
                   END;


               9 : BEGIN

                      Plot := FALSE;
                      Tournament := TRUE;
                      Time := TRUE;
                      Horizon := MaxDepth;
                      Quiescence := MaxDepth;
                      Full_Depth := MaxDepth          
         
                   END

            END;

            WRITELN;
            IF ( L = 9 ) THEN
               BEGIN
                  WRITELN( 'SKILL SET AT TOURNAMENT LEVEL.' )
               END
            ELSE
               BEGIN
                  WRITELN( 'SKILL LEVEL SET AT ', L, '.' )
               END;
            WRITELN

         END

      ELSE

         BEGIN

            WRITELN;
            WRITELN( 'SKILL LEVEL ', L, ' IS INVALID.' );
            WRITELN( 'SKILL LEVEL SET AT ', Default_Level : 1 );
            WRITELN;
            Level := Default_Level;
            Full_Depth := FW_Limit;
            Horizon := MaxSearch;
            Quiescence := MaxSearch;
            Plot := TRUE

         END

   END;

(********************************************************************)

PROCEDURE Get_Move( R : STR80; VAR MC : POSITION );

   VAR

      FF, FR, MS, MM, MP, I, J : INTEGER;
      A : ARRAY [1..4] OF INTEGER;
      CD : INTEGER;
      CH : CHAR;

   BEGIN

      FOR I := 1 TO 4 DO
         A[I] := 0;
      I := 0;
      J := 1;
      WHILE ( ( I < Length ( R ) ) AND ( J <= 4 ) ) DO
         BEGIN
            I := I + 1;
            CH := UpCase( R[I] );
            IF ( ( CH IN Letters ) AND ( R[I+1] IN Numbers ) ) THEN
               BEGIN
                  A[J] := ( ORD( CH ) - (ORD( 'A' ) - 1) );
                  J := J + 1
               END;
            IF ( ( CH IN Numbers ) AND ( UpCase(R[I-1]) IN Letters ) ) THEN
               BEGIN
                  Val ( CH, A[J], CD );
                  IF ( CD <> 0 ) THEN
                     A[J] := 0;
                  J := J + 1
               END
         END;
      IF ( ((A[1] > 0) AND (A[2] > 0)) AND
           ((A[3] = 0) AND (A[4] = 0)) )
      THEN
         BEGIN
            MC.Move_Cell.From_Cell.File_Cell := A[1];
            MC.Move_Cell.From_Cell.Rank_Cell := A[2];
            MC.Move_Cell.To_Cell.File_Cell := 0;
            MC.Move_Cell.To_Cell.Rank_Cell := 0;
            MC.Score_Cell := 0;
            MC.Mode_Cell := 'I';
            WRITELN;
            WRITELN( 'ACKNOWLEDGED.' );
            WRITELN;
            MS := Board[A[1], A[2]].Piece_Cell.Side_Cell;
            IF ( MS = 0 ) THEN
               BEGIN
                  WRITELN( 'THERE IS NO PIECE ON BOARD CELL ',
                            File_Vector[A[1]], A[2]:1, '.' )
               END
            ELSE
               BEGIN
                  MM := Board[A[1], A[2]].Piece_Cell.Man_Cell;
                  MP := Piece_Table[MS, MM].Move_Pattern;
                  WRITELN( 'THERE IS A ', Men[MS], ' ',
                           Piece[MP], ' ON BOARD CELL ',
                           File_Vector[A[1]], A[2]:1, '.' )
               END
         END
      ELSE
         BEGIN
            FOR I := 1 TO 4 DO
                BEGIN
                   IF ( A[I] = 0 ) THEN
                      FOR J := 1 TO 4 DO
                          A[J] := 0
                END;
            MC.Move_Cell.From_Cell.File_Cell := A[1];
            MC.Move_Cell.From_Cell.Rank_Cell := A[2];
            MC.Move_Cell.To_Cell.File_Cell := A[3];
            MC.Move_Cell.To_Cell.Rank_Cell := A[4];
            MC.Score_Cell := 0;
            MC.Mode_Cell := ' '
         END;

      IF ( A[1] = 0 ) THEN
         BEGIN
            I := POS( '-', R );
            IF ( ( I > 0 ) AND (Piece_Table[OX, 1].Position_Cell.
                               File_Cell = 5 ) ) THEN
               BEGIN
                  IF ( R[I - 1] IN ['O', '0'] ) THEN
                     BEGIN
                        MC.Score_Cell := 0;
                        MC.Mode_Cell := ' ';
                        MC.Move_Cell.From_Cell.File_Cell := 5;
                        MC.Move_Cell.From_Cell.Rank_Cell :=
                           Piece_Table[OX, 1].Position_Cell.Rank_Cell;
                        MC.Move_Cell.To_Cell.Rank_Cell :=
                           Piece_Table[OX, 1].Position_Cell.Rank_Cell;
                        I := POS( '-', COPY( R, (I + 1),
                                ( LENGTH( R ) - (I + 1) ) ) );
                        IF ( I > 0 ) THEN
                           BEGIN
                              IF ( R[I - 1] IN ['O', '0'] ) THEN
                                 BEGIN
                                    MC.Move_Cell.To_Cell.File_Cell := 3
                                 END
                              ELSE
                                 BEGIN
                                    MC.Move_Cell.To_Cell.File_Cell := 7
                                 END
                           END
                        ELSE
                           BEGIN
                              MC.Move_Cell.To_Cell.File_Cell := 7
                           END
                     END
               END
         END

   END;

(********************************************************************)

FUNCTION Validate_Move( MP:POSITION; P:POINTER; M:INTEGER ) : BOOLEAN;

   VAR

      W, I, IH, WH, SH, MH, BH : INTEGER;
      Found : BOOLEAN;
      PH, PW : POSITION;

   BEGIN

      Found := FALSE;
      IH := P^.IX;
      BH := P^.BX;                     
      SH := P^.SX;
      WH := P^.WX;
      MH := P^.PX;
      PH := P^.Move_List[P^.MLI[1]];
      P^.IX := 0;
      P^.PX := M;
      P^.WX := 0;
      CLEAR_TC_CELLS;

      W := List_Gen( P, ' ' );

      I := 0;
      WHILE ( ( I < W ) AND NOT Found ) DO
         BEGIN
            I := I + 1;
            PW := P^.Move_List[P^.MLI[I]];
            Found := (( PW.Move_Cell.From_Cell.File_Cell =
                        MP.Move_Cell.From_Cell.File_Cell ) AND
                      ( PW.Move_Cell.From_Cell.Rank_Cell =
                        MP.Move_Cell.From_Cell.Rank_Cell ) AND
                      ( PW.Move_Cell.To_Cell.File_Cell =
                        MP.Move_Cell.To_Cell.File_Cell ) AND
                      ( PW.Move_Cell.To_Cell.Rank_Cell =
                        MP.Move_Cell.To_Cell.Rank_Cell ))
         END;
      P^.Move_List[P^.MLI[1]] := PH;
      P^.IX := IH;
      P^.BX := BH;                    
      P^.SX := SH;
      P^.PX := MH;
      P^.WX := WH;
      Validate_Move := Found

   END;

(******************************************************************** 

PROCEDURE Pre_Trace( P : POINTER );

   VAR I, N : INTEGER;

   BEGIN
       
      I := P^.IX;                       
      FOR N := 1 TO (P^.LX - 1) DO
          Write('  ');
      Write('ITR = ', Iteration, '  ');
      Writeln('L = ', P^.LX, ' I = ',
      I, ' Ordering Score = ',
      P^.Move_List[P^.MLI[I]].Score_Cell);
      IF ( P^.Move_List[P^.MLI[I]].Score_Cell = +INF ) THEN
         BEGIN
            WRITE('***********************':75);
            IF ( Alert ) THEN
               WRITELN( Alarm )
            ELSE
               WRITELN
         END;
      IF ( P^.Move_List[P^.MLI[I]].Score_Cell = +Marker7 ) THEN
         BEGIN
            WRITE('-!-!-!-!-!-!-!-!-!-!-!-':75);
            IF ( Alert ) THEN
               WRITELN( Alarm )
            ELSE
               WRITELN
         END
   END;
     ******************************************************************** 

PROCEDURE Post_Trace( P : POINTER );

   VAR I, N : INTEGER;

   BEGIN

      I := P^.IX;                       
      FOR N := 1 TO (P^.LX - 1) DO
          Write('  ');
      Writeln('                     Move Score = ',
               P^.Move_List[P^.MLI[I]].Score_Cell)          
   END;
     ********************************************************************)

   (* NegaMax FailSoft Alpha_Beta Begins Here *)

FUNCTION NFAB( P : POINTER; Alpha, Beta, Depth : INTEGER) : INTEGER;

   VAR

         I, X, Width : INTEGER;
         Score, Value : INTEGER;
         NewPly : POINTER;

   PROCEDURE Order( P : POINTER );

      VAR

         H, I : INTEGER;

      BEGIN

         Sort( P, P^.WX );

      (* Test for Capture or Promotion *)

         IF ( ( Extended ) AND ( P^.LX > 1 ) AND ( Depth = 1 ) AND
              ( P^.Move_List[P^.MLI[1]].Mode_Cell IN ['C', 'P'] ) )
         THEN
            BEGIN
               I := 1;
               WHILE ( (P^.Move_List[P^.MLI[I]].Mode_Cell IN ['C', 'P'] ) AND
                       ( I < P^.WX ) ) DO
                  BEGIN
                     I := I + 1
                  END;
               IF ( NOT( P^.Move_List[P^.MLI[I]].Mode_Cell IN ['C', 'P'] ) AND
                    ( I <= P^.WX ) )
               THEN
                  BEGIN
                     H := P^.MLI[ I ];
                     FOR I := ( I - 1 ) DOWNTO 1 DO
                        BEGIN
                           P^.MLI[ I + 1 ] := P^.MLI[ I ]
                        END;
                     P^.MLI[ I ] := H
                  END
            END
      END;


   BEGIN

      IF ( Depth <= 0 ) THEN                                      
         BEGIN
 		NFAB := Eval( P )                       
         END
      ELSE
         BEGIN
            IF ( ( P^.LX > 1 ) OR ( SA[1].GENED = 0 ) ) THEN
               BEGIN
                  Width := Generate( P );
                  IF ( Width > 0 ) THEN
                     BEGIN
                        SA[P^.LX].GENED := SA[P^.LX].GENED + 1;
                        IF ( P^.LX = 1 ) THEN
                           BEGIN
                              Order( P )
                           END
                     END
               END
            ELSE
               BEGIN
                  Width := P^.WX
               END;
            IF ( NOT( Width > 0 ) ) THEN
               BEGIN
                  NFAB := Eval( P )
               END
            ELSE
               BEGIN

                  Score := -INF;
                  I := 0;

                  IF ( P^.LX > 1 ) THEN
                     BEGIN
                        P^.BX := 1;            
                        IF ( Width > 1 ) THEN
                           BEGIN
                              Locate_Refutations( P );
                              Order( P )
                           END
                     END
                  ELSE
                     BEGIN
                        IF ( Predicted ) THEN
                           BEGIN
                              Predicted := FALSE;
                              IF ( P^.IX > 1 ) THEN
                                 BEGIN
                                    Score := P^.Move_List[P^.MLI[P^.BX]].
                                                Score_Cell;
                                    I := P^.IX - 1
                                 END
                           END
                     END;

                  NewPly := Create( P );

                  REPEAT

                     I := I + 1;

                     P^.IX := I;

                     IF ( ( Plot ) AND ( NOT( Speculating ) OR
                          ( Report ) ) ) THEN
                        BEGIN
                           X := ((P^.LX * 3) - 2); 
                           IF ( (P^.LX = 1) AND (I = 1) ) THEN
                              BEGIN
                                 GotoXY(1, 24);
                                 ClrEol
                              END;
                           IF ( NOT( X > 78 ) ) THEN
                              BEGIN
                                 GotoXY(X, 24);
                                 Write(I)
                              END
                     (* END
                     ELSE
                        IF ( (Trace) AND ( NOT((Plot) OR (Speculating)) ) )
                           THEN
                           BEGIN
                              Pre_Trace( P )  *)      
                           END;

                     Make( P );

                     Value := -NFAB( NewPly, -Beta, -Max( Alpha, Score ),
                                                   Decrement( P, Depth ) );

                     IF (( Tournament ) AND ( Clock ))
                     THEN
                        BEGIN
                           Value := P^.Move_List[P^.MLI[I]].Score_Cell
                        END;

                     P^.Move_List[P^.MLI[I]].Score_Cell := Value;

                     Undo( P );

                  (* IF ( (Trace) AND NOT((Plot) OR (Speculating)) ) THEN
                        BEGIN
                           Post_Trace( P )          
                        END;  *)

             (*  Test for KeyBoard Interrupt  *)

                     IF ( ( Speculating ) AND NOT ( Interupt ) ) THEN
                        BEGIN
                           Interupt := KeyPressed;
                           IOstat := IORESULT
                        END;

             (*  If Move is an Improvement, Save Move and Better Score  *)

                     IF ( ( Value > Score ) AND
                          ( NOT(( Clock ) OR ( Interupt )) ) ) THEN
                        BEGIN
                           Score := Value;
                           P^.BX := I;
                           Save_Refutation( P )
                        END;

             (*  Build Refutation Table from Triangular Work Array  *)

                     IF ( ( P^.LX = 1 ) AND
                          ( NOT(( Clock ) OR ( Interupt )) ) ) THEN
                        BEGIN
                           Build_Refutation_Table( P )
                        END;

             (*  Test for end of Iteration Sequence at Level P^.LX  *)

                  UNTIL ( ( Score >= Beta ) OR ( I = Width )
                          OR ( Interupt ) OR ( Clock ) );

             (*  If Plotting Game-Tree on Screen, Blank-Out Display  *)

                  IF ( ( Plot ) AND ( NOT( Speculating ) OR
                       ( Report ) ) ) THEN
                     BEGIN
                        IF ( NOT( X > 78 ) ) THEN
                           BEGIN
                              GotoXY(X, 24);
                              Write('   ')
                           END
                     END;

             (*  Put KILLERS into List  *)

                  IF ( (( Score >= Beta ) AND ( P^.LX > 1 )) AND
                       NOT(( Interupt ) OR ( Clock )) ) THEN
                     BEGIN
                        Save_Killer( P );
                        IF ( ( Report ) OR ( HardCopy ) ) THEN
                           BEGIN
                              SA[P^.LX].Cutoff := SA[P^.LX].Cutoff + 1;
                              Cutoffs := Cutoffs + 1
                           END
                     END;

             (*  End of KILLER Sequence  *)

             (*  If Tournament Play, Check Clock  *)

                  IF ( ( Tournament ) AND ( NOT ( P^.LX > Iteration ) ) AND
                       ( NOT(( Clock ) OR ( Speculating )) ) ) THEN
                     BEGIN
                        Clock := ( Check_Clock(MoveClock, VHV) )
                     END;

             (*  Adjust Ply-1 Score for Tournament Clock Interrupt  *)

                  IF ( ( Tournament ) AND ( P^.LX = 1 ) AND
                       ( NOT ( I > 1 ) ) AND
                       ( Clock ) AND ( NOT ( Speculating ) ) )
                  THEN
                     BEGIN
                        P^.BX := 1;
                        Score := Last_Score;
                        P^.Move_List[P^.MLI[1]].Score_Cell := Score
                     END;

             (*  Adjust Ply-1 Score if keyboard Interrupt occured  *)

                  IF ( ( Interupt ) AND ( P^.LX = 1 ) )  
                  THEN
                     BEGIN
                        Score := Last_Score
                     END;

             (*  Back Score up to previous level of the Game-Tree  *)

                  NFAB := Score;
                  Drop( NewPly )

               END
         END

   END;

(********************************************************************)

PROCEDURE Read_Reply( VAR R : STR80 );

   VAR

      I : INTEGER;

   BEGIN

      FOR I := 1 TO 80 DO
         R[I] := ' ';
   (* I := 1;
      WHILE ( NOT EOLN( Input ) AND ( I <= 80 ) ) DO
         BEGIN
            READ( R[ I ] );
            I := I + 1
         END;
      READLN; *)
      READLN( R );
      StripBlanks( R );
      UpCaseStr( R )

   END;

(********************************************************************)

FUNCTION Com_Scan( R : STR80; VAR I : INTEGER; C : CHAR ) : BOOLEAN;

   VAR

      FOUND : BOOLEAN;

   BEGIN

      FOUND := FALSE;
      I := 0;
      CASE C OF

           '0'..'9',
           'A'..'Z',
                '?' : BEGIN    (*  Check for specific letter or number  *)
                         I := POS( C, R );
                         FOUND := NOT( I = 0 )
                      END;

                'a' : BEGIN    (*  Check for alphabetic character  *)
                         WHILE NOT((I = LENGTH(R)) OR (FOUND)) DO
                            BEGIN
                               I := I + 1;
                               FOUND := (UpCase(R[I]) IN ALPHABET)
                            END
                      END;

                'n' : BEGIN    (*  Check for numeric character  *)
                         WHILE NOT((I = LENGTH(R)) OR (FOUND)) DO
                            BEGIN
                               I := I + 1;
                               FOUND := (R[I] IN NUMERALS)
                            END
                      END

      END;
      Com_Scan := FOUND

   END;

(********************************************************************)

FUNCTION DISABLED : BOOLEAN;

   VAR

      I : INTEGER;

   BEGIN

      DISABLED := (((Com_Scan(Response,I,'K')) AND
                  (COPY(Response, I, 4) = 'KILL')) OR
                  ((Com_Scan(Response,I,'N')) AND
                  ( NOT(COPY(Response, I, 3) = 'NOW')) AND
                  ( NOT(Response[I - 1] IN ['E', 'I', 'R'] ) AND
                  ( Response[I + 1] IN ['''', 'A', 'O', 'T'] ))) OR
                  ((Com_Scan(Response,I,'D')) AND
                  (Response[I+1] IN ['E','I']) AND
                  (Response[I+2] IN ['A','S']) AND
                  ( NOT(Response[I+3] = 'P') )) OR
                  ((Com_Scan(Response,I,'O')) AND
                  ( Response[I + 1] = 'F' ) AND
                  ( Response[I + 2] = 'F' )) OR
                  ((Com_Scan(Response,I,'O')) AND
                  ( Copy(Response,(I-2),4) = 'STOP' )) OR
                  ((Com_Scan(Response,I,'C')) AND
                  ( Copy(Response,I,6) = 'CANCEL' )) OR
                  ((Com_Scan(Response,I,'S')) AND
                  ( Copy(Response,I,4) = 'STOP' )))

   END;

(*********************************************************************)

PROCEDURE Read_LogFile( VAR LogFile : LogFileType );

   VAR

      P : POINTER;
      LogRcd : LogRcdType;
      N : INTEGER;
      
   BEGIN
      N := 0;
      P := Create( NIL );
      WHILE ( NOT (EOF( LogFile )) AND ( StatRcd.CompletionCode = 0 )) DO
         BEGIN
            P^.IX := 1;
            REPEAT
               READ( LogFile, LogRcd );
               IOstat := CheckIO( 'READING LOGFILE' );
               IF ( IOstat = 0 ) THEN
                  BEGIN
                     MI := MI + 1;
                     MvArr[ MI ] := LogRcd;
                     Move := LogRcd.Move;
                     P^.Move_List[P^.MLI[1]] := Move;
                     Move_Cntr := (( MI DIV 2 ) + ( MI MOD 2 ));
                     Make( P )
                  END
               ELSE
                  BEGIN
                     N := N + 1
                  END
            UNTIL (( IOstat = 0 ) OR ( EOF( LogFile )) OR 
                   ( N = Retry_Limit ))
         END;
      Drop( P )

   END;

(*********************************************************************)

PROCEDURE Write_LogFile( VAR LogFile : LogFileType );

   VAR

      I, N : INTEGER;

   BEGIN

      IF ( StatRcd.CompletionCode = 0 ) THEN
         BEGIN
            N := 0;
            WRITELN( 'SAVING GAME MOVE HISTORY TO BE CONTINUED LATER.' );
            WRITELN( 'STANDBY.' );
            FOR I := 1 TO MI DO
                BEGIN
                   REPEAT
                      N := N + 1;
                      WRITE( LogFile, MvArr[ I ] );
                      IOstat := CheckIO( 'WRITING LOGFILE' )
                   UNTIL (( IOstat = 0 ) OR ( N = Retry_Limit ))
                END;
            WRITELN( 'DONE: GAME SAVED.' )
         END

   END;

(*********************************************************************)

PROCEDURE Continue_Game( VAR StatRcd : StatRcdType );

   VAR

      OName : Str20;
      RM : INTEGER;

   BEGIN
      IF (( StatRcd.CompletionCode = 0 ) AND
          ( NOT ( Restore ) )) THEN
         BEGIN
            LastReply := Response;
            OName := Opponent;
            WRITELN;
            IF ( COPY(OName, 1, 1 ) = ',' ) THEN
               BEGIN
                  DELETE( OName, 1, 2 )
               END;
            IF ( OName = StatRcd.OpntName ) AND
               NOT( StatRcd.OpntName = '' ) THEN
               BEGIN
                  WRITE( OName, ', WE HAVE AN ' );
                  WRITELN( 'UNFINISHED GAME ',
                           'WHICH WE ')
               END
            ELSE
               BEGIN
                  IF ( LENGTH( OName ) > 1 ) THEN
                     WRITE( OName, ' ' );
                  WRITE( 'THERE IS AN UNFINISHED' );
                  WRITELN( ' GAME THAT WAS ' );
                  IF ( LENGTH( StatRcd.OpntName ) > 1 ) THEN
                     BEGIN
                        WRITE( 'PLAYED BY ' );
                        WRITE( 'AN OPPONENT NAMED ' );
                        WRITE( StatRcd.OpntName, ' ' )
                     END
               END;
            IF NOT( StatRcd.LogDate.Yr = 80 ) THEN
               BEGIN
                  WRITE( 'PLAYED ON ',
                         DayArr[StatRcd.LogDate.DOW],
                         ' ', StatRcd.LogDate.Dy:2, ' ',
                         MoArr[StatRcd.LogDate.Mo] )
               END
            ELSE
               BEGIN
                  WRITE( 'PLAYED FOR ', StatRcd.NrMoves,
                         ' MOVES BEFORE ADJOURNING' )
               END;
            WRITELN( '.' );
            WRITELN( 'SHALL WE CONTINUE THE ',
                     'UNFINISHED GAME?', Alarm );
            WRITELN;
            Read_Reply( Response );
            WRITELN;
            WRITELN( 'ACKNOWLEDGED.' );
            WRITELN;
            IF (( Disabled ) OR ( COPY( Response, 1, 1 ) = 'N' )) THEN
               BEGIN
                  IF ( NOT ( MX = OX ) ) THEN
                     BEGIN
                        Close_StatFile( StatFile );
                        WRITELN('SETTING UP FOR ',
                                'A NEW GAME.');
                        WRITELN
                     END;
                  TermCode := 7;
                  StatRcd.CompletionCode := 7
               END
         END;
      IF (( StatRcd.CompletionCode = 0 ) AND
          ( NOT ( TermCode = 7 ) )) THEN
         BEGIN
            WRITELN( 'PLEASE STANDBY WHILE I REVIEW ',
                     'THE GAME.');
            WRITELN;
            Open_LogFile( LogFile, 'I' );
            IF ( IOstat = 0 ) THEN
               BEGIN
                  Read_LogFile( LogFile );
                  Close_LogFile( LogFile );
                  WRITELN('DONE: CONTINUING ',
                          'PREVIOUS GAME.');
                  WRITELN;
                  WITH StatRcd DO
                     BEGIN
                        CompletionCode := 0;
                        Move_Cntr := NrMoves;
                        OX := OpntSide;
                        MX := OX + (( OX - 1 ) * -2 ) + 1;
                        ElapsedClock := ElapsedTime;
                        AllocatedTime := AllocTime;
                        AllocatedMoves := AllocMoves;
                        StartClock := StartTime.TSc;
                        StopClock := StopTime.TSc;
                        RM := ( AllocatedMoves + 1 ) - Move_Cntr;
                        IF ( RM = 0 ) THEN
                           RM := 1;
                        MoveClock := MchnClock / RM;
                        Level := LevelInd;
                        Set_Level( Level );
                        Tournament := TournFlag;
                        Time := TimeFlag;
                        HardCopy := HardCopyFlag;
                        Report := RptFlag;
                        Hide := HideFlag;
                        Chronical := ChronFlag;
                        Show := ShowFlag;
                        Plot := PlotFlag
                     END;
                  TermCode := 6;
                  WRITELN( 'YOU WERE PLAYING ',
                           MEN[ OX ], '.' );
                  WRITELN( 'IT IS ', MEN[ OX ],
                           '''S TURN TO MOVE.' )
               END
            ELSE
               BEGIN
                  WRITELN( 'I CANNOT RETRIEVE OUR PREVIOUS ',
                           'GAME.' );
                  WRITELN( 'STARTING A NEW GAME.' );
                  TermCode := 7;
                  StatRcd.CompletionCode := 7
               END
         END

   END;

(*********************************************************************)

PROCEDURE Read_StatFile( VAR StatFile : StatFileType;
                         VAR StatRcd : StatRcdType );

   VAR

      N : INTEGER;

   BEGIN

      N := 0;
      REPEAT
         IF (( StatRcd.CompletionCode = 7 ) OR
             ( TermCode = 7 )) THEN
            BEGIN
               IOstat := 0
            END
         ELSE
            BEGIN
               READ( StatFile, StatRcd );
               IOstat := CheckIO( 'READING STATFILE' );
               IF ( EOF( StatFile ) ) THEN
                  BEGIN
                     IOstat := 0
                  END;
               IF (( IOstat = 0 ) AND
                   ( StatRcd.CompletionCode = 0 )) THEN
                  BEGIN
                     Continue_Game( StatRcd )
                  END
               ELSE
                  BEGIN
                     N := N + 1
                  END
            END

      UNTIL (( IOstat = 0 ) OR ( EOF( StatFile )) OR ( N = Retry_Limit ))

   END;

(********************************************************************)

PROCEDURE Write_StatFile( VAR StatFile : StatFileType;
                          VAR StatRcd : StatRcdType );

   VAR

      D : DateRcdType;
      T : TimeRcdType;
      OName : Str20;
      N : INTEGER;

   BEGIN

      StatRcd.CompletionCode := TermCode;
      IF ( StatRcd.CompletionCode = 0 ) THEN
         BEGIN
            OName := Opponent;
            IF ( COPY(OName, 1, 1 ) = ',' ) THEN
               BEGIN
                  DELETE( OName, 1, 2 )
               END;
            GetDateTime( D, T );
            WITH StatRcd DO
               BEGIN
                  OpntName := OName;
                  OpntSide := OX;
                  LevelInd := Level;
                  NrMoves := Move_Cntr;
                  OpntClock := 0;
                  MchnClock := AllocatedTime - ElapsedClock;
                  ElapsedTime := ElapsedClock;
                  AllocTime := AllocatedTime;
                  AllocMoves := AllocatedMoves;
                  LogDate := D;
                  StopTime := T;
                  Score := 0;
                  TournFlag := Tournament;
                  TimeFlag := Time;
                  HardCopyFlag := HardCopy;
                  RptFlag := Report;
                  HideFlag := Hide;
                  ChronFlag := Chronical;
                  ShowFlag := Show;
                  PlotFlag := Plot
               END;
            WRITELN( 'SAVING GAME SETTINGS.' );
            N := 0;
            REPEAT
               BEGIN
                  WRITE( StatFile, StatRcd );
                  IOstat := CheckIO( 'WRITING STATFILE' );
                  IF ( NOT ( IOstat = 0 ) ) THEN
                     BEGIN
                        N := N + 1
                     END
               END
            UNTIL (( IOstat = 0 ) OR ( N = Retry_Limit ))
         END

   END;

(********************************************************************)

PROCEDURE Save_Game;

   BEGIN

      IF ( MI > 0 ) THEN
         BEGIN
            StatRcd.CompletionCode := 0;
            Open_StatFile( StatFile, 'O' );
            IF ( IOstat = 0 ) THEN
               BEGIN
                  Write_StatFile( StatFile, StatRcd );
                  Close_StatFile( StatFile );
                  Open_LogFile( LogFile, 'O' );
                  IF ( IOstat = 0 ) THEN
                     BEGIN
                        Write_LogFile( LogFile );
                        Close_LogFile( LogFile )
                     END
               END
         END
      ELSE
         BEGIN
            WRITELN( 'I CANNOT SAVE THE GAME - NO MOVES HAVE ' );
            WRITELN( 'BEEN TRANSACTED.' );
            WRITELN
         END

   END;

(********************************************************************)

PROCEDURE Restore_Game;

   BEGIN

      IF (( Chronical ) OR ( Restore )) THEN
         BEGIN
            Open_StatFile( StatFile, 'I' );
            Read_StatFile( StatFile, StatRcd );
            Close_StatFile( StatFile );
            Erase_Files;
            Initialize_StatRcd( StatRcd )
         END

   END;

(*********************************************************************)

PROCEDURE Display_Board;

   VAR

      XR, XF, XS, XM, XP : INTEGER;
      Cell : Str3;

   BEGIN   (* DisplayBoard *)

      WRITELN;
      WRITELN('  +----+----+----+----+----+----+----+----+');
      FOR XR := 8 DOWNTO 1 DO
          BEGIN
             FOR XF := 1 TO 8 DO
                 BEGIN
                    XS := Board[XF, XR].Piece_Cell.Side_Cell;
                    XM := Board[XF, XR].Piece_Cell.Man_Cell;
                    IF (( XS = 0 ) OR
                        ( Piece_Table[XS, XM].Status_Cell = 'C' )) THEN
                       BEGIN
                          Cell := ''
                       END
                    ELSE
                       BEGIN
                          IF ( XS = WHITE ) THEN
                             BEGIN
                                Cell := 'W'
                             END
                          ELSE
                             BEGIN
                                Cell := 'B'
                             END;
                          XP := Piece_Table[XS, XM].Move_Pattern;
                          CASE XP OF

                               1 : BEGIN
                                      Cell := Cell+'K'
                                   END;
                               2 : BEGIN
                                      Cell := Cell+'Q'
                                   END;
                               3 : BEGIN
                                      Cell := Cell+'R'
                                   END;
                               4 : BEGIN
                                      Cell := Cell+'N'
                                   END;
                               5 : BEGIN
                                      Cell := Cell+'B'
                                   END;
                               6 : BEGIN
                                      Cell := Cell+'P'
                                   END

                          END
                       END;
                    IF ( XF = 1 ) THEN
                       BEGIN
                          WRITE( XR : 1 )
                       END;
                    WRITE( ' |', Cell : 3 )
                 END;
             WRITELN(' |');
             WRITELN('  +----+----+----+----+----+----+----+----+')
          END;
      WRITELN('    A    B    C    D    E    F    G    H');
      WRITELN

   END;    (* DisplayBoard *)

(********************************************************************)

FUNCTION Final( Response : STR80 ) : BOOLEAN;

   VAR

      I : INTEGER;

   BEGIN

      Final := (((Com_Scan( Response, I, 'N')) AND
             (( Response[I+1] = 'O' ) OR
              ( LENGTH( Response ) = 1 )) AND
              (( LENGTH( Response ) < 9 ) OR
               ((Com_Scan( Response, I, 'S')) AND
                ( Copy(Response,I-2,4) = 'ELSE' )))) OR
             (LENGTH( Response ) = 0) OR ( Done ) OR
             (((Com_Scan( Response, I, 'T')) AND
               ( Copy(Response,I,4) = 'THAT' ) AND
               ((( Com_Scan( Response, I, 'L')) AND
                 ( Response[I-1] = 'A' ) AND
                 ( Response[I+1] = 'L' )) OR
                (( Com_Scan( Response, I, 'I')) AND
                 ( Response[I+1] = 'T' ))) OR
              ((Com_Scan( Response, I, 'L')) AND
               ( Copy(Response,I,3) = 'LET' ) AND
               ( Com_Scan( Response, I, 'Y')) AND
               ( Copy(Response,I-3,4) = 'PLAY' )))) OR
             ( MX = OX ) OR ( Restore ))

   END;

(*********************************************************************)

PROCEDURE Process_Request( VAR Response : STR80 );

   VAR

      I, J, N : INTEGER;
      C : CHAR;
      Event : POSITION;


   FUNCTION QUANTITY : BOOLEAN;

      VAR   I1, N1 : INTEGER;

      BEGIN

         Quantity := FALSE;
         N := 0;
         IF ( Com_Scan( Response, I, 'n' ) ) THEN
            BEGIN
               Quantity := TRUE;
               VAL( Response[I], N, J );
               IF ( J = 0 ) THEN
                  BEGIN
                     IF ( Com_Scan( Copy( Response, (I+1),
                          (Length(Response) - I) ), I1, 'n' ) ) THEN
                        BEGIN
                           I1 := I + I1;
                           VAL( Response[I1], N1, J );
                           IF ( J = 0 ) THEN
                              N := (N * 10) + N1
                        END
                  END
            END

      END;


   BEGIN

      Restore := FALSE;
      Pnct := '.';
      REPEAT
         BEGIN
            Get_Move( Response, Event );
            IF (( Event.Move_Cell.From_Cell.File_Cell > 0 ) AND
                ( Event.Move_Cell.From_Cell.Rank_Cell > 0 ) AND
                ( Event.Move_Cell.To_Cell.File_Cell > 0 ) AND
                ( Event.Move_Cell.To_Cell.Rank_Cell > 0 )) THEN
               BEGIN
                  OX := MX;
                  LastReply := Response
               END
            ELSE
               BEGIN
               IF (NOT( Event.Move_Cell.From_Cell.File_Cell > 0 )) THEN
                  BEGIN
                     WRITELN;
                     WRITELN( 'ACKNOWLEDGED.' );
                     WRITELN;
               IF (( Com_Scan( Response, I, 'V' )) AND
                  ((( Response[I - 2] = 'L' ) OR
                   ( Response[I - 1] = 'L' )) AND
                   ( NOT( Response[I - 1] = 'O' ) ))) THEN
                  BEGIN
                     IF ( Com_Scan( Response, I, 'n' ) ) THEN
                        BEGIN
                           VAL( Response[I], N, J );
                           IF ( J = 0 ) THEN
                             BEGIN
                                Set_Level( N )
                             END
                        END
                     ELSE
                        BEGIN
                           IF ((Com_Scan( Response, I, 'T' )) AND
                               (Com_Scan( Response, I, 'O' )) AND
                               (Com_Scan( Response, I, 'U' )) AND
                               (Com_Scan( Response, I, 'R' )) AND
                               (Com_Scan( Response, I, 'N' )) AND
                               (Com_Scan( Response, I, 'A' )) AND
                               (Com_Scan( Response, I, 'M' )) AND
                               (Com_Scan( Response, I, 'E' ))) THEN
                              BEGIN
                                 Set_Level( 9 )
                              END
                        END
                  END
               ELSE
                  IF ((( Com_Scan( Response, I, 'Q' ) ) AND
                       ( Copy( Response, I, 4 ) = 'QUIT' )) OR
                      (( Com_Scan( Response, I, 'S' ) ) AND
                       ( Copy( Response, (I - 2), 6 ) = 'RESIGN' )) OR
                      (( Com_Scan( Response, I, 'C' ) ) AND
                       ( Copy( Response, I, 5 ) = 'CONCE' )) OR
                      (( Com_Scan( Response, I, 'T' ) ) AND
                       ( Copy( Response, I, 9 ) = 'TERMINATE' ))) THEN
                     BEGIN
                        IF (( Com_Scan( Response, I, '?' ) ) OR
                            (( Com_Scan( Response, I, 'D' ) ) AND
                             ( Copy( Response, I, 3 ) = 'DO ' )) OR
                            (( Com_Scan( Response, I, 'A' ) ) AND
                             ( Copy( Response, I, 3 ) = 'ARE' )) OR
                            (( Com_Scan( Response, I, 'W' ) ) AND
                             ( Copy( Response, I, 3 ) = 'WHY' ))) THEN
                           BEGIN
                              WRITELN;
                              WRITELN( 'I DO NOT CONCEDE.' );
                              WRITELN
                           END
                        ELSE
                           BEGIN
                              IF ((( Com_Scan( Response, I, 'C' ))
                                 AND ( Copy( Response, I, 5 )
                                       = 'CONCE' )) OR
                                  (( Com_Scan( Response, I, 'S' ))
                                 AND ( Copy( Response, I - 2, 6 )
                                       = 'RESIGN' ))) THEN
                                 BEGIN
                                    TermCode := 3;
                                    CD := 'R'
                                 END
                              ELSE
                                 BEGIN
                                    TermCode := 5;
                                    CD := 'T'
                                 END;
                              IF ( HardCopy ) THEN
                                 BEGIN
                                    IF ( MX = WHITE ) THEN
                                       BEGIN
                                          Produce_Report( Machine_Move,
                                                          Void_Move,
                                                          ET, CD )
                                       END
                                    ELSE
                                       BEGIN
                                          Produce_Report( Void_Move,
                                                          Void_Move,
                                                          ET, CD )
                                       END
                                 END;
                              Done := TRUE
                           END
                     END
                  ELSE
                     IF (( Com_Scan( Response, I, 'H' ) ) AND
                         (( COPY( Response, I, 3 ) = 'HID' ) OR
                          ( COPY( Response, I, 3 ) = 'HOR' ))) THEN
                        BEGIN
                           IF ( Response[I+4] = 'Z' ) THEN
                              BEGIN
                                 IF Quantity THEN
                                    BEGIN
                                       Horizon := N;
                                       Quiescence := N;
                                       WRITELN;
                                       WRITELN( 'SEARCH HORIZON ',
                                                'SET AT ',
                                                N, ' PLY.' );
                                       WRITELN
                                    END
                              END
                           ELSE
                              BEGIN
                                 IF ( COPY( Response, I, 3 ) = 'HID' )
                                 THEN
                                    BEGIN
                                       IF DISABLED THEN
                                          BEGIN
                                             Hide := FALSE;
                                             WRITELN;
                                             WRITELN('DEACTIVATING ',
                                                     'HIDDEN PLOT ',
                                                     'LINE DURRING',
                                                     ' MOVE ',
                                                     'SPECULATION.');
                                             WRITELN
                                          END
                                       ELSE
                                          BEGIN
                                             Hide := TRUE;
                                             WRITELN;
                                             WRITELN('ACTIVATING ',
                                                     'HIDDEN PLOT ',
                                                     'LINE DURRING',
                                                     ' MOVE ',
                                                     'SPECULATION.');
                                             WRITELN
                                          END
                                    END
                              END
                        END
                     ELSE
                     BEGIN
                     IF ((( Com_Scan( Response, I, 'F' ) ) AND
                          ( Copy( response, I, 4 ) = 'FULL' )) AND
                         (( Com_Scan( Response, I, 'W' ) ) AND
                          ( Copy( response, I, 5 ) = 'WIDTH' )))THEN
                        BEGIN
                        IF Quantity THEN
                           BEGIN
                              IF ( N > Horizon ) THEN
                                 BEGIN
                                    WRITELN;
                                    WRITELN( 'UNABLE TO COMPLY.' );
                                    WRITELN
                                 END
                              ELSE
                                 BEGIN
                                    Full_Depth := N;
                                    WRITELN;
                                    WRITELN( 'FULL WIDTH SEARCH SET ',
                                             'AT ', N, ' PLY.' );
                                    WRITELN;
                                    I := (Full_Depth + ((Quiescence
                                               - Full_Depth) DIV 2))
                                 END
                           END
                        END
                     ELSE
                      BEGIN
                        IF ( (( Com_Scan( Response, I, 'R' ) ) AND
                              ( COPY( Response, I, 6 ) = 'REPORT' )) OR
                             (( Com_Scan( Response, I, 'E' ) ) AND
                              (COPY(Response, (I-1), 6) = 'REPORT')) OR
                             (( Com_Scan( Response, I, 'P' ) ) AND
                              (COPY(Response, (I-2), 6) = 'REPORT')) OR
                             (( Com_Scan( Response, I, 'O' ) ) AND
                              (COPY(Response, (I-3), 6) = 'REPORT')) OR
                             (( Com_Scan( Response, I, 'T' ) ) AND
                              (COPY(Response, (I-3), 6) = 'REPORT')) AND
                             NOT(( Com_Scan( Response, I, 'L' ) ) AND
                                 ( Response[I + 1] = 'O' ) ) ) THEN
                           BEGIN
                            IF DISABLED THEN
                             BEGIN
                              Report := FALSE;
                              Time := Tournament;
                              WRITELN;
                              WRITELN( 'DEACTIVATING MOVE STATISTICS ',
                                       'REPORTING.' );
                              WRITELN
                             END
                            ELSE
                             BEGIN
                              Report := TRUE;
                              Time := TRUE;
                              WRITELN;
                              WRITELN( 'REPORTING MOVE STATISTICS.' );
                              WRITELN;
                              IF (((Com_Scan(Response,I,'P'))
                                 AND (COPY(Response,I,5)
                                      = 'PRINT')) OR
                                  ((Com_Scan(Response,I,'Y'))
                                 AND ((COPY(Response, (I - 7), 8)
                                      = 'HARDCOPY') OR
                                      (COPY(Response, (I - 8), 9)
                                      = 'HARD COPY'))) OR
                                  ((Com_Scan(Response,I,'H'))
                                 AND (COPY(Response,I,4)
                                      = 'HARD')))
                              THEN
                                 BEGIN
                                    IF DISABLED THEN
                                      BEGIN
                                         HardCopy := FALSE;
                                         WRITELN('DEACTIVATING ',
                                                 'HARDCOPY ',
                                                 'OF MOVE ',
                                                 'STATISTICS.');
                                         WRITELN
                                      END
                                    ELSE
                                      BEGIN
                                         HardCopy := TRUE;
                                         WRITELN('PRODUCING ',
                                                 'HARDCOPY ',
                                                 'OF MOVE ',
                                                 'STATISTICS.');
                                         WRITELN
                                      END
                                 END
                             END
                           END
                        ELSE
                           BEGIN
                              IF (( Com_Scan( Response, I, 'X' )) AND
                                 NOT( Response[I + 1] = 'T' )) THEN
                                 BEGIN
                                    IF Quantity THEN
                                       BEGIN
                                          Quiescence := N;
                                          WRITELN;
                                          WRITELN( 'MAXIMUM DEPTH',
                                                   ' OF SEARCH',
                                                   ' SET AT ',
                                                   N:1, ' PLY.' );
                                          WRITELN;
                                          I := (Full_Depth + ((Quiescence
                                                   - Full_Depth) DIV 2))
                                       END
                                 END
                              ELSE
                                 BEGIN
                                    IF (((Com_Scan(Response, I, 'R')) AND
                                       (Response[I + 1] IN ['M', 'T']) AND
                                       (Response[I - 3] = 'A') AND
                                       (Response[I - 2] = 'L'))
                                       OR
                                       ((Com_Scan(Response, I, 'A')) AND
                                       (Response[I + 1] = 'L') AND
                                       (Response[I + 3] = 'R') AND
                                       (Response[I + 4] IN ['M', 'T'])))
                                       THEN
                                       BEGIN
                                         IF DISABLED THEN
                                            BEGIN
                                               Alert := FALSE;
                                               WRITELN;
                                               WRITELN('DEACTIVATING ',
                                                       'AUDIBLE ',
                                                       'MOVE ALERT.');
                                               WRITELN
                                            END
                                          ELSE
                                            BEGIN
                                               Alert := TRUE;
                                               WRITELN;
                                               WRITELN('ACTIVATING ',
                                                       'AUDIBLE ',
                                                       'MOVE ALERT.');
                                               WRITELN
                                            END
                                       END
                                    ELSE
                                       BEGIN
                                       IF (((Com_Scan(Response,I,'T')) AND
                                           (Copy(Response,I,5) =
                                                             'TRAC')) OR
                                          ((Com_Scan(Response,I,'T'))  AND
                                          (Com_Scan(Response, I, 'R')) AND
                                          (Com_Scan(Response, I, 'A')) AND
                                          (Response[I + 1] = 'C') AND
                                          (Response[I + 2] = 'E')))
                                          THEN
                                          BEGIN
                                            IF DISABLED THEN
                                               BEGIN
                                                  Trace := FALSE;
                                                  WRITELN;
                                                  WRITELN('TRACING OF ',
                                                          'GAME TREE ',
                                                          'CANCELLED.');
                                                  WRITELN
                                               END
                                            ELSE
                                               BEGIN
                                                  Trace := TRUE;
                                                  Report := TRUE;
                                                  Time := TRUE;
                                                  Plot := FALSE;
                                                  WRITELN;
                                                  WRITELN('TRACING GAME ',
                                                          'TREE.');
                                                  WRITELN
                                               END
                                           END
                                       ELSE
                                          BEGIN
                                        IF ((((Com_Scan(Response,I,'T')) AND
                                           (COPY(Response,I,4)='TIME')) OR
                                           ((Com_Scan(Response,I,'I')) AND
                                           (COPY(Response, (I-1), 4) =
                                                               'TIME')) OR
                                           ((Com_Scan(Response,I,'M')) AND
                                           (COPY(Response, (I-2), 4) =
                                                               'TIME')) OR
                                           ((Com_Scan(Response,I,'E')) AND
                                           (COPY(Response, (I-3), 4) =
                                                               'TIME'))) AND
                                           NOT (((Com_Scan(Response,I,'B'))
                                             AND ((COPY(Response, I, 5)
                                                  = 'BOARD'))) OR
                                              ((Com_Scan(Response,I,'O'))
                                             AND ((COPY(Response,(I-1),5)
                                                  = 'BOARD'))) OR
                                              ((Com_Scan(Response,I,'R'))
                                             AND ((COPY(Response,(I-3),5)
                                                  = 'BOARD')))))
                                           THEN
                                            IF DISABLED THEN
                                             BEGIN
                                                Time := FALSE;
                                                WRITELN;
                                                WRITELN('DISABLING ',
                                                        'ELAPSED TIME ',
                                                        'REPORTING.');
                                                WRITELN
                                             END
                                            ELSE
                                             BEGIN
                                                Time := TRUE;
                                                WRITELN;
                                                WRITELN('REPORTING ',
                                                        'ELAPSED TIME.');
                                                WRITELN
                                             END
                                          ELSE
                                           BEGIN
                                        IF ((Com_Scan(Response,I,'R')) AND
                                        (Copy(Response,(I-4),6)='ALTERN'))
                                             THEN
                                              IF DISABLED THEN
                                               BEGIN
                                                Alternate := FALSE;
                                                WRITELN;
                                                WRITELN('DEACTIVATING ',
                                                        'ALTERNATING ',
                                                        'INTERVAL MOVE ',
                                                        'ORDERING.');
                                                WRITELN
                                               END
                                              ELSE
                                               BEGIN
                                                Alternate := TRUE;
                                                WRITELN;
                                                WRITELN('ACTIVATING ',
                                                        'ALTERNATING ',
                                                        'INTERVAL MOVE ',
                                                        'ORDERING.');
                                                WRITELN
                                               END
                                            ELSE
                                             BEGIN
                                          IF (((Com_Scan(Response,I,'P'))
                                             AND (COPY(Response,I,5)
                                                  = 'PRINT')) OR
                                              ((Com_Scan(Response,I,'H'))
                                             AND (COPY(Response,I,4)
                                                  = 'HARD')))
                                             THEN
                                              BEGIN
                                               IF DISABLED THEN
                                                BEGIN
                                                  HardCopy := FALSE;
                                                  WRITELN('DEACTIVATING ',
                                                          'HARDCOPY ',
                                                          'OF MOVE ',
                                                          'STATISTICS.');
                                                  WRITELN
                                                END
                                               ELSE
                                                BEGIN
                                                  HardCopy := TRUE;
                                                  WRITELN;
                                                  WRITELN('PRODUCING ',
                                                          'HARDCOPY ',
                                                          'OF MOVE ',
                                                          'STATISTICS.');
                                                  WRITELN
                                                END
                                              END
                                            ELSE
                                              BEGIN
                                          IF (((Com_Scan(Response,I,'L'))
                                             AND (COPY(Response,(I-1),4)
                                                  = 'PLOT')) OR
                                              ((Com_Scan(Response,I,'P'))
                                             AND (COPY(Response, I, 4)
                                                  = 'PLOT')))
                                             THEN
                                              BEGIN
                                              IF DISABLED THEN
                                                BEGIN
                                                Plot := FALSE;
                                                WRITELN;
                                                WRITELN('PLOTTING ',
                                                        'GAME TREE ',
                                                        'ON SCREEN ',
                                                        'DEACTIVATED.');
                                                WRITELN
                                                END
                                              ELSE
                                                BEGIN
                                                Plot := TRUE;
                                                Trace := FALSE;
                                                WRITELN;
                                                WRITELN('PLOTTING ',
                                                        'GAME TREE ',
                                                        'ON SCREEN.');
                                                WRITELN
                                                END
                                              END
                                            ELSE
                                               BEGIN
                                          IF (((Com_Scan(Response,I,'X'))
                                             AND (COPY(Response,I-1,6) =
                                                  'EXTEND'))
                                             OR
                                              ((Com_Scan(Response,I,'Q'))
                                             AND (COPY(Response,I-1,6) =
                                                  'QUIESC')))
                                             THEN
                                                BEGIN
                                                IF DISABLED THEN
                                                 BEGIN
                                                  Extended := FALSE;
                                                  WRITELN;
                                                  WRITELN('DEACTIVATING ',
                                                          'QUIESCENT ',
                                                          'SEARCH.');
                                                  WRITELN
                                                 END
                                                ELSE
                                                 BEGIN
                                                  Extended := TRUE;
                                                  WRITELN;
                                                  WRITELN('ACTIVATING ',
                                                          'QUIESCENT ',
                                                          'SEARCH.');
                                                  WRITELN
                                                 END
                                               END
                                            ELSE
                                               BEGIN
                                          IF (((Com_Scan(Response,I,'U'))
                                             AND (COPY(Response,I-4,8) =
                                                  'SPECULAT'))
                                             OR
                                             ((Com_Scan(Response,I,'S'))
                                             AND (COPY(Response,I,8) =
                                                  'SPECULAT')))
                                             THEN
                                                BEGIN
                                                IF DISABLED THEN
                                                 BEGIN
                                                  Cogitate := FALSE;
                                                  WRITELN;
                                                  WRITELN('DEACTIVATING ',
                                                          'SPECULATION ',
                                                          'ON OPPONENT ',
                                                          'MOVE ',
                                                          'RESPONSE ',
                                                          'OPTIONS.' );
                                                  WRITELN
                                                 END
                                                ELSE
                                                 BEGIN
                                                  Cogitate := TRUE;
                                                  WRITELN;
                                                  WRITELN('ACTIVATING ',
                                                          'SPECULATION ',
                                                          'ON OPPONENT ',
                                                          'MOVE ',
                                                          'RESPONSE ',
                                                          'OPTIONS.' );
                                                  WRITELN
                                                 END
                                                END
                                             ELSE
                                                BEGIN
                                          IF (((Com_Scan(Response,I,'S'))
                                             AND (COPY(Response, I, 3) =
                                                  'SAV')) OR
                                              ((Com_Scan(Response,I,'V'))
                                             AND (COPY(Response,(I-2),3)
                                                  = 'SAV')))
                                             THEN
                                                 BEGIN
                                                 IF DISABLED THEN
                                                  BEGIN
                                                  Chronical := FALSE;
                                                  Save := FALSE;
                                                  WRITELN;
                                                  WRITELN('DEACTIVATING ',
                                                          'AUTOMATIC ',
                                                          'GAME ',
                                                          'ARCHIVING.');
                                                  WRITELN
                                                  END
                                                 ELSE
                                                  BEGIN
                                                   IF ( Move_Cntr = 0 )
                                                   THEN
                                                   BEGIN
                                                    Chronical := TRUE;
                                                    Map := TRUE;
                                                    Save := FALSE;
                                                    WRITELN;
                                                    WRITELN('ACTIVATING ',
                                                          'AUTOMATIC ',
                                                          'GAME ',
                                                          'ARCHIVING.');
                                                    WRITELN
                                                   END
                                                   ELSE
                                                   BEGIN
                                                    Done := TRUE;
                                                    Save := TRUE;
                                                    TermCode := 0;
                                                    CD := 'T';
                                                    WRITELN;
                                                    WRITELN('SAVING ',
                                                          'GAME AND ',
                                                          'TERMINATING.');
                                                    WRITELN
                                                   END
                                                  END
                                                 END
                                                ELSE
                                                 BEGIN
                                          IF (((Com_Scan(Response,I,'R'))
                                             AND ((COPY(Response, I, 7) =
                                                   'RESTORE') OR
                                                  (COPY(Response, I, 7) =
                                                   'RESTART'))) OR
                                              ((Com_Scan(Response,I,'O'))
                                             AND ((COPY(Response,(I-4),7)
                                                  = 'RESTORE') OR
                                                  (COPY(Response,(I-1),7)
                                                  = 'CONTINU'))) OR
                                              ((Com_Scan(Response,I,'C'))
                                             AND ((COPY(Response, I, 7)
                                                  = 'CONTINU'))))
                                             THEN
                                                  BEGIN
                                                   Restore := TRUE;
                                                   Restore_Game;
                                                   Restore := ( NOT
                                                    ( TermCode = 7 ));
                                                   IF ( NOT ( Restore ) )
                                                   THEN
                                                    BEGIN
                                                     WRITELN( 'UNSUCCESS',
                                                              'FUL.' );
                                                     WRITELN
                                                    END
                                                  END
                                                 ELSE
                                                  BEGIN
                                          IF ( ((Com_Scan(Response,I,'M'))
                                             AND ((COPY(Response, I, 3) =
                                                   'MAP'))) OR
                                              ((Com_Scan(Response,I,'A'))
                                             AND ((COPY(Response,(I-1),3)
                                                  = 'MAP'))) OR
                                              ((Com_Scan(Response,I,'P'))
                                             AND ((COPY(Response,(I-2),3)
                                                  = 'MAP'))) )
                                             THEN
                                                   BEGIN
                                                    IF DISABLED THEN
                                                     BEGIN
                                                      Map := FALSE;
                                                      Chronical := FALSE;
                                                      WRITELN('DISABLING',
                                                              ' MOVE ',
                                                              'MAPPING.');
                                                      WRITELN
                                                     END
                                                    ELSE
                                                     BEGIN
                                                      Map := TRUE;
                                                      Chronical := TRUE;
                                                      WRITELN('ENABLING',
                                                              ' MOVE ',
                                                              'MAPPING.');
                                                      WRITELN
                                                     END
                                                   END
                                                  ELSE
                                                   BEGIN
                                          IF (((Com_Scan(Response,I,'B'))
                                             AND ((COPY(Response, I, 5)
                                                  = 'BOARD'))) OR
                                              ((Com_Scan(Response,I,'O'))
                                             AND ((COPY(Response,(I-1),5)
                                                  = 'BOARD'))) OR
                                              ((Com_Scan(Response,I,'R'))
                                             AND ((COPY(Response,(I-3),5)
                                                  = 'BOARD'))))
                                             THEN
                                                    BEGIN
                                          IF (((Com_Scan(Response,I,'D'))
                                             AND ((COPY(Response, I, 6) =
                                                   'DISPLA'))) OR
                                              ((Com_Scan(Response,I,'Y'))
                                             AND ((COPY(Response,(I-6),7)
                                                  = 'DISPLAY'))) OR
                                              ((Com_Scan(Response,I,'S'))
                                             AND ((COPY(Response, I, 4)
                                                  = 'SHOW') OR
                                                  (COPY(Response, I, 3)
                                                  = 'SEE') OR
                                                  (COPY(Response, I, 6)
                                                  = 'SCREEN'))) OR
                                              ((Com_Scan(Response,I,'K'))
                                             AND ((COPY(Response,(I-3),4)
                                                  = 'LOOK'))) OR
                                              ((Com_Scan(Response,I,'U'))
                                             AND ((COPY(Response, I, 2)
                                                  = 'UP') OR
                                                  (COPY(Response,(I-1),3)
                                                  = 'PUT'))) OR
                                              ((Com_Scan(Response,I,'V'))
                                             AND ((COPY(Response, I, 4)
                                                  = 'VIEW'))))
                                             THEN
                                                     BEGIN
                                                     IF DISABLED THEN
                                                      BEGIN
                                                       Show := FALSE;
                                                      WRITELN('DISABLING',
                                                            ' AUTOMATIC ',
                                                            'BOARD ',
                                                            'DISPLAY.');
                                                       WRITELN
                                                      END
                                                     ELSE
                                                      BEGIN
                                          IF (((Com_Scan(Response,I,'U'))
                                             AND ((COPY(Response,(I-1),9)
                                                  = 'AUTOMATIC'))) OR
                                              ((Com_Scan(Response,I,'C'))
                                             AND ((COPY(Response,(I-8),9)
                                                  = 'AUTOMATIC'))) OR
                                              ((Com_Scan(Response,I,'V'))
                                             AND ((COPY(Response,(I-1),5)
                                                  = 'EVERY'))) OR
                                              ((Com_Scan(Response,I,'R'))
                                             AND ((COPY(Response,(I-3),5)
                                                  = 'EVERY'))) OR
                                              ((Com_Scan(Response,I,'E'))
                                             AND ((COPY(Response, I, 5)
                                                  = 'EVERY') OR
                                                  (COPY(Response, I, 4)
                                                  = 'EACH'))) OR
                                              ((Com_Scan(Response,I,'H'))
                                             AND ((COPY(Response,(I-3),4)
                                                  = 'EACH'))) OR
                                              ((Com_Scan(Response,I,'C'))
                                             AND ((COPY(Response,(I-2),4)
                                                  = 'EACH'))))
                                             THEN
                                                       BEGIN
                                                       Show := TRUE;
                                                       WRITELN('ENABLING',
                                                            ' AUTOMATIC ',
                                                            'BOARD ',
                                                            'DISPLAY.');
                                                       WRITELN
                                                       END
                                                      ELSE
                                                       BEGIN
                                                       Display_Board
                                                       END
                                                      END
                                                     END
                                                    END
                                                   ELSE
                                                      BEGIN
                                          IF (((Com_Scan(Response,I,'R'))
                                             AND ((COPY(Response,(I),5)
                                                  = 'RAPID'))) OR
                                              ((Com_Scan(Response,I,'I'))
                                             AND (((COPY(Response,(I-3),5)
                                                  = 'RAPID') OR
                                                 (COPY(Response,(I-1),5)
                                                  = 'LIGHT') OR
                                                 (COPY(Response,(I-1),5)
                                                  = 'LIMIT')))) OR
                                              ((Com_Scan(Response,I,'G'))
                                             AND ((COPY(Response,(I-2),5)
                                                  = 'LIGHT'))) OR
                                              ((Com_Scan(Response,I,'T'))
                                             AND (((COPY(Response,(I-4),5)
                                                  = 'LIGHT') OR
                                                 (COPY(Response,(I-4),5)
                                                  = 'LIMIT')))))
                                                  THEN
                                                    BEGIN
                                                     IF DISABLED THEN
                                                      BEGIN
                                                       Timed := FALSE;
                                                      Tournament := FALSE;
                                                      WRITELN('DISABLING',
                                                            ' RAPID ',
                                                            'CHESS ',
                                                            'PLAY.');
                                                       WRITELN
                                                      END
                                                     ELSE
                                                      BEGIN
                                                       Timed := TRUE;
                                                       Tournament := TRUE;
                                                       IF (Quantity) THEN
                                                          BEGIN
                                                            AllocatedTime
                                                              := (N * 60);
                                                            WRITELN(
                                                            'GAME TIME ',
                                                            'LIMIT SET ',
                                                            'AT ', N,
                                                            ' MINUTES.' )
                                                          END
                                                       ELSE
                                                          BEGIN
                                                            AllocatedTime
                                                              := 300;
                                                             WRITELN(
                                                             'ENABLING',
                                                             ' RAPID ',
                                                             'CHESS ',
                                                             'PLAY FOR A',
                                                             ' 5 MINUTE',
                                                             ' GAME.')
                                                          END;
                                                       WRITELN
                                                      END
                                                    END
                                                  ELSE
                                                    BEGIN
                                                       WRITELN(
                                                       'NONSEQUITUR.');
                                                       WRITELN
                                                    END
                                                   END
                                                  END
                                                 END
                                                END
                                               END
                                              END
                                             END
                                            END
                                           END
                                          END
                                         END
                                       END
                                 END
                           END
                       END
                      END
                     END;
               IF ( NOT (( Done ) OR ( Restore )) ) THEN
                  BEGIN
                     WRITELN;
                     WRITELN( 'WHAT ELSE?' );
                     WRITELN;
                     Read_Reply( Response )
                  END
               END
         END
      UNTIL (Final( Response ));
      IF NOT (( Done ) OR ( MX = OX ) OR ( Restore )) THEN
         BEGIN
            WRITELN;
            WRITELN( 'ACKNOWLEDGED.' )
         END

   END;

(*********************************************************************)

PROCEDURE Move_Selector( P : POINTER );

   VAR

      D, I, L, Limit, Width : INTEGER;
      Alpha, Beta : INTEGER;
      P1 : POINTER;

   PROCEDURE ReOrder( P2 : POINTER; ID : INTEGER );

      VAR

          H, I : INTEGER;

      BEGIN

      (* Test Reordering Interval and Reorder Accordingly *)

          IF ( ( P2^.WX > 1 ) AND ( P2^.BX > 1 ) ) THEN
             BEGIN
                IF ( (Alternate) AND ( NOT((ID MOD 2) = 0) ) ) THEN
                   BEGIN
                      P2^.IX := P2^.BX
                   END
                ELSE
                   BEGIN
                      H := P2^.Move_List[P2^.MLI[P2^.BX]].Score_Cell;
                      P2^.Move_List[P2^.MLI[P2^.BX]].Score_Cell := +INF;
                      Sort( P2, P2^.WX );
                      P2^.IX := 1;
                      P2^.BX := 1;
                      P2^.Move_List[P2^.MLI[P2^.BX]].Score_Cell := H 
                   END
             END
          ELSE
             BEGIN
                P2^.IX := 1;
                P2^.BX := 1   
             END

      END;


   BEGIN

      Interupt := FALSE;
      Clock := FALSE;

            (* Establish Depth of Full_Width Search *)

      IF ( ( Full_Depth > 5 ) AND ( Extended ) AND 
           ( NOT(( Move_Cntr > 4 ) OR ( Tournament )) ) )               
      THEN                                                    
         BEGIN                                                
            Limit := 5                                        
         END                                                  
      ELSE                                                    
         BEGIN                                                
            Limit := Full_Depth                               
         END;                                                 
                  
      IF ( ( Move_Cntr = 0 ) AND NOT( Tournament ) AND ( Full_Depth > 2 ) )
      THEN
         BEGIN
            IF ( MX = White )
            THEN
               BEGIN
                  Limit := 2
               END
            ELSE
               BEGIN
                  Limit := 3
               END
         END;

      IF ( NOT( Cogitate ) OR ( Speculating ) OR NOT( Predicted ) ) THEN
         BEGIN

            (* Reset Finite State Vectors *)
           
            IC := 1;                         
            Value := 0;
            Last_Score := 0;
            Err := +INF;
            Completed := FALSE;
            Reset_Accumulators;
            Clear_Attack_Cells;
            Clear_Killers;
            Clear_Refutation_List;
            Clear_PC_List;
            P1 := Create( NIL );
            P1^.SX := OX;
            P1^.LX := 0;
            Piece_Table[MX, 1].Pseudo_Status := ' ';
            Width := Generate( P1 );
            Drop( P1 )

         END;

            (* If Tournament Play, Get Move Clock *)

      IF ( Tournament ) THEN
         BEGIN
            IF NOT( Predicted ) THEN
               BEGIN
                  Last_Score := -INF;
                  Shallow_Score := -INF
               END;
            IF NOT( Speculating ) THEN
               BEGIN
                  MoveClock := Compute_Clock
               END
         END;
      IF ( (( Tournament ) OR ( Time ) OR ( HardCopy )) AND
           ( NOT( Speculating ) ) ) THEN
         BEGIN
            TIMER('B', HR, MN, SEC, HUND, ET)
         END;

            (* Call Iterative_Deepening Fail_Soft Alpha_Beta *)

      FOR D := ( IC + 1 ) TO Limit DO
          BEGIN
             IF ( NOT(( Completed ) OR ( Interupt )) ) THEN
                BEGIN
                   IF ( NOT( Predicted ) ) THEN                         
                      BEGIN
                         Clear_TC_Cells;
                         Clear_TD_Cells;
                         Clear_LVL_Cells( P )
                      END;
                   Iteration := D;
                   Alpha := Value - Err;
                   Beta := Value + Err;

                   Value := NFAB( P, Alpha, Beta, D );

                   IF (( Tournament ) AND
                       ( NOT(( Clock ) OR ( Speculating )) ))
                   THEN
                      BEGIN
                         Clock := ( Check_Clock( MoveClock, Value ) );
                      END;

                   IF (( Value >= Beta ) AND NOT( Interupt )) THEN
                      BEGIN
                         IF ( Clock ) THEN
                            BEGIN
                               P^.BX := 1;
                               Value := Last_Score;
                               P^.Move_List[P^.MLI[P^.BX]].Score_Cell
                                     := Last_Score
                            END
                         ELSE
                            BEGIN
                               Clear_TC_Cells;
                               Clear_TD_Cells;
                               Clear_LVL_Cells( P );
                               P^.IX := 1;
                               P^.BX := 1;
                               IF ( Level < 3 ) THEN
                                  BEGIN
                                     Value := -INF
                                  END;

                               Value := NFAB( P, Value, +INF, D );

                               IF ( Interupt ) THEN
                                  BEGIN
                                     Value := 0;
                                     Err := +INF
                                  END;

                               IF NOT( Speculating ) THEN
                                  BEGIN
                                     IF (( Tournament ) AND ( NOT( Clock ) ))
                                     THEN
                                        BEGIN
                                           Clock := 
                                              Check_Clock( MoveClock, Value )
                                        END;
                                     Failed_High := Failed_High + 1
                                  END
                            END
                      END;

                   IF (( Value <= Alpha ) AND NOT( Interupt )) THEN
                      BEGIN
                         IF ( Clock ) THEN
                            BEGIN
                               P^.BX := 1;
                               Value := Last_Score;
                               P^.Move_List[P^.MLI[P^.BX]].Score_Cell
                                     := Last_Score
                            END
                         ELSE
                            BEGIN
                               Clear_TC_Cells;
                               Clear_TD_Cells;
                               Clear_LVL_Cells( P );  
                               P^.IX := 1;
                               P^.BX := 1;
                               IF ( Level < 3 ) THEN
                                  BEGIN
                                     Value := +INF
                                  END;
       
                               Value := NFAB( P, -INF, Value, D ); 

                               IF ( Interupt ) THEN
                                  BEGIN
                                     Value := 0;
                                     Err := +INF
                                  END;
                                       
                               IF NOT( Speculating ) THEN
                                  BEGIN
                                     IF ( (Tournament) AND (NOT(Clock)) )
                                     THEN
                                        BEGIN
                                           Clock := 
                                           Check_Clock( MoveClock, Value )
                                        END;
                                     Failed_Low := Failed_Low + 1
                                  END
                            END

                      END;

            (* If Speculative Search was not Interrupted then Reorder *)

                   IF ( NOT( Interupt ) ) THEN
                      BEGIN
                         IC := D;                          
                         ReOrder( P, D )
                      END;

            (* Adjust Aspiration A-B Window *)

                   IF ( ( NOT( Speculating ) AND ( Adjust ) ) AND
                        ( Est < PNV ) AND
                        ((( Failed_High > 1 ) OR ( Failed_Low > 1 )) AND
                         (( Failed_High > 0 ) AND ( Failed_Low > 0 ))) )
                   THEN
                      BEGIN
                         IF ( Fail_Cntr > 10 ) THEN
                            BEGIN
                               Fail_Cntr := 0;
                               Est := Est + Err_Adj;
                               IF ( Report ) THEN
                                  BEGIN
                                     WRITELN;
                                     WRITELN('A-B ASPIRATION WINDOW ',
                                             'INCREASED.',
                                             Alarm );
                                     WRITELN
                                  END
                            END
                         ELSE
                            BEGIN
                               Fail_Cntr := Fail_Cntr + 1;
                               Decay_Cntr := 0
                            END
                      END
                   ELSE
                      BEGIN
                         IF ( Fail_Cntr > 0 ) THEN
                            BEGIN
                               IF ( Decay_Cntr < Decay_Interval ) THEN
                                  BEGIN
                                     Decay_Cntr := Decay_Cntr + 1
                                  END
                               ELSE
                                  BEGIN
                                     Fail_Cntr := Fail_Cntr - 1;
                                     Decay_Cntr := 0
                                  END
                            END
                      END;

            (* Test for Search Complete *)

                   IF ( NOT( Interupt ) ) THEN
                      BEGIN
                         Err := Est;
                         Last_Score := Value;
                         Completed := ( SA[D].Gened = 0 );
                         IF ( ( Tournament ) AND ( NOT( Completed ) ) )
                         THEN
                            BEGIN
                               Completed := ( NOT(Speculating) AND (Clock) );
                               IF ( Iteration = 2 ) THEN
                                  BEGIN
                                     Shallow_Score := Value
                                  END
                            END;
                         IF ( NOT( Completed ) ) THEN
                            BEGIN
                               Completed := (( King( P, OX ) IN Jeopardy ) OR
                                             ( King( P, MX ) IN Jeopardy ))
                            END
                      END
                END
          END;        (* End of Iterative-Deepening Iteration Sequence *)

      IF ( NOT( Speculating ) ) THEN
         BEGIN

            (* Return Best Move to Calling Sequence *)

            IF ( NOT( P^.Move_List[P^.MLI[1]].Score_Cell = Value ) )
            THEN
               BEGIN
                  P^.MLI[1] := P^.MLI[P^.BX];
                  P^.Move_List[P^.MLI[1]].Score_Cell := Value;
                  IF ( Report AND NOT( Alternate ) ) THEN
                     BEGIN
                       Write( Alarm, Alarm );
                       Writeln( '!!! WARNING:  ORDERING ERROR !!!', Alarm )
                     END
               END;
            IF ( (Alternate) AND (P^.WX > 1) AND ( NOT ((D MOD 2) = 0) ) )
            THEN
               BEGIN
                  P^.MLI[1] := P^.MLI[P^.BX];
                  P^.Move_List[P^.MLI[1]].Score_Cell := Value;
               END;
            P^.IX := 1;
            P^.BX := 1;
            TotalScored := TotalScored + Scored;
            TotalCutoffs := TotalCutoffs + Cutoffs;
            Last_Move_Score := Value;

            (* Set Status Indicator *)

            Piece_Table[MX, 1].Status_Cell :=
               Piece_Table[MX, 1].Pseudo_Status

         END;

      Predicted := FALSE                                 

   END;

(********************************************************************)

PROCEDURE Speculate( P : POINTER; VAR Move : POSITION );

   VAR

      W, X1, Y1, X2, Y2, S, M : INTEGER;
      Event, Move_Hold : POSITION;
      P1 : POINTER;
      Hold : BOOLEAN;

   BEGIN

      Predicted := FALSE;
      Speculating := FALSE;
      Move := Void_Move;
      IF (( Cogitate ) AND
         NOT(( RL[P^.MLI[1], 2].Move_Cell.From_Cell.File_Cell = 0 ) OR
             ( P^.WX = 0 ))) THEN
         BEGIN
            Event := RL[P^.MLI[1], 2];
            X1 := Event.Move_Cell.From_Cell.File_Cell;
            Y1 := Event.Move_Cell.From_Cell.Rank_Cell;
            X2 := Event.Move_Cell.To_Cell.File_Cell;
            Y2 := Event.Move_Cell.To_Cell.Rank_Cell;
            IF (( X1 > 0 ) AND ( Y1 > 0 )) THEN
               BEGIN
                  S := Board[X1, Y1].Piece_Cell.Side_Cell;
                  M := Board[X1, Y1].Piece_Cell.Man_Cell;
                  IF ( Board[X2, Y2].Piece_Cell.Side_Cell = MX )
                  THEN
                     BEGIN
                        IF ( M = 1 ) THEN
                           BEGIN
                              Event.Mode_Cell := 'M'
                           END
                        ELSE
                           BEGIN
                              Event.Mode_Cell := 'C'
                           END
                     END
                  ELSE
                     BEGIN
                        Event.Mode_Cell := ' '
                     END;
                  Move_Hold := Opponent_Move;
                  Opponent_Move := Event;
                  P1 := Create( NIL );
                  P1^.SX := OX;
                  P1^.LX := 0;
                  P1^.IX := 1;
                  P1^.Move_List[P1^.MLI[1]] := Event;
                  Make( P1 );
                  IF ((Piece_Table[S, M].Pseudo_Status = 'P') OR
                      (P1^.Move_List[P1^.MLI[1]].Mode_Cell = 'P'))
                  THEN
                     BEGIN
                        Event.Mode_Cell := 'P'
                     END;
                  IF ( NOT ( King( NIL, MX ) IN Check ) ) THEN
                     BEGIN
                        IF (( Report ) AND ( Plot ) AND ( NOT ( HIDE ) ))
                        THEN
                           BEGIN
                              WRITELN( 'SPECULATING WHILE WAITING.' );
                              WRITELN
                           END;
                        Piece_Table[OX, 1].Pseudo_Status := ' ';
                        Hold := Plot;
                        IF ( Hide ) THEN
                           BEGIN
                              Plot := FALSE
                           END;
                        Speculating := TRUE;
                        Move_Selector( P );
                        Speculating := FALSE;
                        Plot := Hold;
                        Move := Event         
                     END;
                  P1^.SX := OX;
                  P1^.IX := 1;
                  P1^.Move_List[P1^.MLI[1]] := Event;
                  Undo( P1 );
                  Drop( P1 );
                  Opponent_Move := Move_Hold
               END
         END
   END;

(********************************************************************)

(*==================================================================*)
(*              L E V E L   2   A L G O R I T H M S                 *)
(*==================================================================*)

(********************************************************************)

PROCEDURE Retriever( P : POINTER );

   VAR

      X1, Y1, X2, Y2, A1, B1, A2, B2 : INTEGER;
      R, W, I, J, S, M, N : INTEGER;
      Event, Guess : POSITION;
      Finished, Legal, Reply, Repeated : BOOLEAN;
      P1 : POINTER;

   PROCEDURE Promote_Pawn;

      VAR

         PP : INTEGER;

      BEGIN
         WRITELN( 'WHICH PIECE WOULD YOU LIKE?' );
         WRITELN( Alarm );
         Read_Reply( Response );
         IF (( COM_SCAN( Response, I, 'Q' )) AND
             ( Copy( Response, I, 5 ) = 'QUEEN' ))
         THEN
            BEGIN
               Piece_Table[ S, M ].Move_Pattern := 2
            END
         ELSE
            IF (( COM_SCAN( Response, I, 'R' )) AND
                ( Copy( Response, I, 4 ) = 'ROOK' ))
            THEN
               BEGIN
                  Piece_Table[ S, M ].Move_Pattern := 3
               END
            ELSE
               IF (( COM_SCAN( Response, I, 'K' )) AND
                   ( Copy( Response, I, 6 ) = 'KNIGHT' ))
               THEN
                  BEGIN
                     Piece_Table[ S, M ].Move_Pattern := 4
                  END
               ELSE
                  IF (( COM_SCAN( Response, I, 'N' )) AND
                      ( Copy( Response, (I-1), 6 ) = 'KNIGHT' ))
                  THEN
                     BEGIN
                        Piece_Table[ S, M ].Move_Pattern := 4
                     END
                  ELSE
                     IF (( COM_SCAN( Response, I, 'B' )) AND
                         ( Copy( Response, I, 6 ) = 'BISHOP' ))
                     THEN
                        BEGIN
                           Piece_Table[ S, M ].Move_Pattern := 5
                        END
                     ELSE
                        BEGIN
                           Piece_Table[ S, M ].Move_Pattern := 2
                        END;

         PP := Piece_Table[ S, M ].Move_Pattern;
         V[S, 2] := V[S, 2] - 1;
         V[S, PP] := V[S, PP] + 1;

         WRITELN;
         WRITELN( 'ACKNOWLEDGED.' );
         WRITELN;
         WRITELN( 'YOUR PAWN HAS BEEN PROMOTED TO A ', Piece[PP], '.' );
         WRITELN

      END;


   BEGIN

      Completed := FALSE;
      Predicted := FALSE;
      Finished := FALSE;
      Legal := FALSE;
      Reply := FALSE;
      Repeated := FALSE;
      A1 := 0;
      A2 := 0;
      B1 := 0;
      B2 := 0;
      IF ( MX = OX ) THEN
         BEGIN
            OX := MX + (( MX - 1 ) * -2 ) + 1;
            Reply := TRUE;
            IF ( TermCode = 7 ) THEN
               BEGIN
                  TermCode := 0;
                  Response := LastReply
               END
            ELSE
               BEGIN
                  IF ( TermCode = 6 ) THEN
                     BEGIN
                        TermCode := 0;
                        Reply := FALSE
                     END
               END
         END
      ELSE
         BEGIN
            WRITELN;
            IF ( TermCode = 6 ) THEN
               BEGIN
                  TermCode := 0;
                  Repeated := TRUE;
                  WRITE( 'AWAITING ' )
               END
         END;
      WHILE NOT Finished DO
         BEGIN
            IF NOT ( Reply ) THEN
               BEGIN
                  WRITE ( 'YOUR MOVE', Opponent );
                  WRITELN( Pnct );
                  IF Alert THEN
                     WRITELN( Alarm )
                  ELSE
                     WRITELN;
                  Pnct := '.';
                  IF (( Cogitate ) AND NOT( Repeated )) THEN
                     BEGIN
                        Speculate( P, Guess );
                        A1 := Guess.Move_Cell.From_Cell.File_Cell;
                        B1 := Guess.Move_Cell.From_Cell.Rank_Cell;
                        A2 := Guess.Move_Cell.To_Cell.File_Cell;
                        B2 := Guess.Move_Cell.To_Cell.Rank_Cell;
                        Repeated := TRUE
                     END;
                  Read_Reply( Response )
               END;
            Reply := FALSE;
            Get_Move( Response, Event );
            X1 := Event.Move_Cell.From_Cell.File_Cell;
            Y1 := Event.Move_Cell.From_Cell.Rank_Cell;
            X2 := Event.Move_Cell.To_Cell.File_Cell;
            Y2 := Event.Move_Cell.To_Cell.Rank_Cell;
            IF (( X1 > 0 ) AND ( Y1 > 0 ) AND
                ( X2 > 0 ) AND ( Y2 > 0 )) THEN
               BEGIN
                  S := Board[X1, Y1].Piece_Cell.Side_Cell;
                  M := Board[X1, Y1].Piece_Cell.Man_Cell;
                  IF ( S = MX ) THEN
                     BEGIN
                        WRITELN;
                        WRITE ( 'UNABLE TO COMPLY - ' );
                        WRITELN ( 'THAT IS MY PIECE.' );
                        WRITELN;
                        WRITE ( 'AWAITING ' );
                        Pnct := '.'
                     END
                  ELSE
                     BEGIN
                        IF ( S = 0 ) THEN
                           BEGIN
                              WRITELN;
                              WRITE ('THERE IS NO PIECE ON BOARD CELL ');
                              WRITE ( File_Vector[X1] );
                              WRITE ( Y1 : 1 );
                              WRITELN ( '.' );
                              WRITELN;
                              WRITE ( 'AWAITING ' );
                              Pnct := '.'
                           END
                        ELSE
                           BEGIN
                              P1 := Create( NIL );
                              P1^.LX := 0;
                              Piece_Table[OX, 1].Pseudo_Status := ' ';
                              P1^.SX := MX;
                              W := Generate( P1 );
                              P1^.SX := OX;
                              Legal := Validate_Move ( Event, P1, M );
                              IF ( Legal ) THEN
                                 BEGIN
                                    IF ( Board[X2, Y2].
                                         Piece_Cell.Side_Cell = MX )
                                    THEN
                                       BEGIN
                                          Event.Mode_Cell := 'C'
                                       END
                                    ELSE
                                       BEGIN
                                          Event.Mode_Cell := ' '
                                       END;
                                    Opponent_Move := Event;
                                    P1^.IX := 1;
                                    P1^.Move_List[P1^.MLI[1]] := Event;
                                    Make( P1 );
                                    IF ((Piece_Table[S, M].
                                         Pseudo_Status = 'P') OR
                                        (P1^.Move_List[P1^.MLI[1]].
                                         Mode_Cell = 'P'))
                                    THEN
                                       BEGIN
                                          Event.Mode_Cell := 'P'
                                       END;
                                    IF ( King( NIL, OX ) IN Check ) THEN
                                       BEGIN
                                         WRITELN;
                                         WRITELN ( 'MOVE PROHIBITED - ',
                                                   'KING IN CHECK.' );
                                         WRITELN;
                                         WRITE ( 'AWAITING ' );
                                         Pnct := '.';
                                         P1^.SX := OX;
                                         P1^.IX := 1;
                                         P1^.Move_List[P1^.MLI[1]]:=Event;
                                         Event.Mode_Cell := ' ';
                                         Undo( P1 )
                                       END
                                    ELSE
                                       BEGIN
                                          Finished := TRUE;
                                          Piece_Table[OX, 1].Status_Cell
                                            := Piece_Table[OX, 1].
                                               Pseudo_Status;
                                          Piece_Table[OX, 1].Pseudo_Status
                                            := ' ';
                                          IF (( Cogitate) AND
                                              ( A1 > 0 ) AND ( B1 > 0 ))
                                          THEN
                                             BEGIN
                                                Predicted := (( A1 = X1 )
                                                          AND ( B1 = Y1 )
                                                          AND ( A2 = X2 )
                                                          AND ( B2 = Y2 ))
                                             END;
                                          WRITELN;
                                          WRITE ( 'ACKNOWLEDGED' );
                                          IF ( Predicted ) THEN
                                             WRITE(' - MOVE PREDICTED');
                                          WRITELN( '.' );
                                          WRITELN;
                                          IF (( Event.Mode_Cell = 'P' ) OR
                                              ( Piece_Table[S, M].
                                                Pseudo_Status = 'P' ) OR
                                              ( Piece_Table[S, M].
                                                Status_Cell = 'P' ) OR
                                              ( P1^.Move_List[P1^.MLI[1]].
                                                Mode_Cell = 'P' ))
                                          THEN
                                             BEGIN
                                                Promote_Pawn
                                             END;
                                          P1^.SX := OX;
                                          Piece_Table[MX, 1].
                                             Pseudo_Status := ' ';
                                          W := Generate ( P1 );
                                          IF NOT(Piece_Table[MX, 1].
                                             Status_Cell = 'C') THEN
                                             BEGIN
                                                Piece_Table[MX, 1].
                                                   Status_Cell :=
                                                Piece_Table[MX, 1].
                                                   Pseudo_Status
                                             END;
                                          IF ( Map ) THEN
                                             BEGIN
                                                MapToMvArr( Event )
                                             END;
                                          IF ( Show ) THEN
                                             BEGIN
                                                Display_Board
                                             END;
                                          IF ( HardCopy ) THEN
                                             BEGIN
                                                IF (( Piece_Table[MX, 1].
                                                      Status_Cell = '!' )
                                                   AND
                                                   ( NOT ( CD IN
                                                  ['M', 'R', 'S', 'T'] )))
                                                THEN
                                                   BEGIN
                                                      CD :=
                                                      CHR( OX + ORD('0') )
                                                   END;
                                                IF ( MX = WHITE ) THEN
                                                   BEGIN
                                                      Produce_Report(
                                                         Machine_Move,
                                                         Opponent_Move,
                                                         ET, CD );
                                                      CD := ' '
                                                   END
                                             END
                                       END
                                 END
                              ELSE
                                 BEGIN
                                    WRITELN;
                                    WRITELN ( 'MOVE PROHIBITED.' );
                                    WRITELN;
                                    WRITE ( 'AWAITING ' );
                                    Pnct := '.'
                                 END;
                              Drop( P1 )
                           END
                     END
               END
            ELSE
               BEGIN
                  IF ( Event.Mode_Cell = 'I' ) THEN
                     BEGIN
                        WRITELN;
                        WRITE ( 'AWAITING ' );
                        Pnct := '.'
                     END
                  ELSE
                     BEGIN
                        IF ((( COM_SCAN( Response, I, 'A' )) AND
                             ( Copy( Response, I, 9 ) =
                                              'ATTENTION' )) OR
                         (* (( Copy( Response, LENGTH( Response ) - 2, 3 )
                                = 'HAL' ) OR
                             ( Copy( Response, LENGTH( Response ) - 3, 3 )
                                = 'HAL' )) OR *)
                            (( COM_SCAN( Response, I, 'H' )) AND
                             ( Copy( Response, I, 3 ) = 'HAL' )) AND
                             ( NOT((COM_SCAN( Response, I, 'K' )) OR
                            ((COM_SCAN( Response, I, 'Q' )) AND
                             ( Response[ I + 2 ] = 'E')))) AND
                             ( NOT((COM_SCAN( Response, I, 'C' )) AND
                                   (COM_SCAN( Response, I, 'A' )) AND
                                   (COM_SCAN( Response, I, 'S' )) AND
                                   (COM_SCAN( Response, I, 'T' )) AND
                                   (COM_SCAN( Response, I, 'L' )))))
                        THEN
                           BEGIN
                              IF ((COM_SCAN( Response, I, 'T')) AND
                                  ( Copy( Response, (I - 1), 9 ) =
                                  'ATTENTION' )) THEN
                                 BEGIN
                                    IF ((COM_SCAN(Response,I,'H')) AND
                                       (Copy(Response, I, 3) = 'HAL'))
                                    THEN
                                       BEGIN
                                          WRITELN;
                                          WRITE ( 'HAL - ACKNOWLEDGED: ');
                                          WRITE ( 'AWAITING ' );
                                          WRITELN ( 'INSTRUCTIONS.' )
                                       END
                                    ELSE
                                       BEGIN
                                          WRITELN;
                                          WRITELN ( 'YES.' )
                                       END;
                                    IF Alert THEN
                                       WRITELN( Alarm )
                                    ELSE
                                       WRITELN;
                                    Read_Reply( Response )
                                 END
                              ELSE
                                 BEGIN
                                    IF ( LENGTH( Response ) < 5 )
                                    THEN
                                       BEGIN
                                          WRITELN;
                                          WRITELN ( 'HAL.' );
                                          IF Alert THEN
                                             WRITELN( Alarm )
                                          ELSE
                                             WRITELN;
                                          Read_Reply( Response )
                                       END
                                    ELSE
                                       BEGIN
                                          IF ( COM_SCAN( Response,
                                                         I, 'H' ))
                                          THEN
                                             Delete( Response, I, 3 )
                                       END
                                 END;
                              Process_Request ( Response );
                              Finished := Done;
                              IF ( MX = OX ) THEN
                                 BEGIN
                                    OX := MX + (( MX - 1 ) * -2 ) + 1;
                                    Reply := TRUE
                                 END;
                              IF  NOT (( Finished ) OR ( Reply )) THEN
                                 BEGIN
                                    WRITELN;
                                    WRITE ( 'AWAITING ' );
                                    Pnct := '.'
                                 END
                           END
                        ELSE
                           BEGIN
                              IF (( COM_SCAN( Response, I, 'C' )) AND
                                  ( COM_SCAN( Response, I, 'A' )) AND
                                  ( COM_SCAN( Response, I, 'S' )) AND
                                  ( COM_SCAN( Response, I, 'T' )) AND
                                  ( COM_SCAN( Response, I, 'L' )))
                              THEN
                                 BEGIN
                                    IF (COM_SCAN( Response, I, 'K'))
                                    THEN
                                       BEGIN
                                          Response := 'O-O';
                                          Reply := TRUE
                                       END
                                    ELSE
                                       BEGIN
                                          IF (COM_SCAN(Response,I,'Q'))
                                          THEN
                                             BEGIN
                                                Response := 'O-O-O';
                                                Reply := TRUE
                                             END
                                          ELSE
                                             BEGIN
                                                R := Piece_Table[OX, 1].
                                                     Position_Cell.
                                                     Rank_Cell;
                                                IF ((Board[2, R].
                                                     Piece_Cell.
                                                     Side_Cell = 0) AND
                                                    (Board[2, R].
                                                     Attack_Cell=' ') AND
                                                    (Board[3, R].
                                                     Piece_Cell.
                                                     Side_Cell = 0) AND
                                                    (Board[3, R].
                                                     Attack_Cell=' ') AND
                                                    (Board[4, R].
                                                     Piece_Cell.
                                                     Side_Cell = 0) AND
                                                    (Board[4, R].
                                                     Attack_Cell=' ') AND
                                                    (Board[6, R].
                                                     Piece_Cell.
                                                     Side_Cell = 0) AND
                                                    (Board[6, R].
                                                     Attack_Cell=' ') AND
                                                    (Board[7, R].
                                                     Piece_Cell.
                                                     Side_Cell = 0) AND
                                                    (Board[7, R].
                                                     Attack_Cell=' ') AND
                                                    (NOT(Piece_Table[OX, 3].
                                                    Pseudo_Status = 'C')) AND
                                                    (Piece_Table[OX, 3].
                                                     Moves = 0) AND
                                                    (NOT(Piece_Table[OX, 4].
                                                    Pseudo_Status = 'C')) AND
                                                    (Piece_Table[OX, 4].
                                                     Moves = 0))
                                                THEN
                                                   BEGIN
                                                   WRITELN;
                                                   WRITELN('WHICH SIDE?');
                                                   WRITELN;
                                                   Read_Reply( Response );
                                                   IF (COM_SCAN(Response,
                                                                   I,'Q'))
                                                   THEN
                                                      BEGIN
                                                      Response := 'O-O-O';
                                                      Reply := TRUE
                                                      END
                                                   ELSE
                                                      BEGIN
                                                      Response := 'O-O';
                                                      Reply := TRUE
                                                      END
                                                   END
                                                ELSE
                                                   BEGIN
                                                     IF ((Board[6, R].
                                                        Piece_Cell.
                                                        Side_Cell = 0) AND
                                                        (Board[6, R].
                                                      Attack_Cell=' ') AND
                                                        (Board[7, R].
                                                        Piece_Cell.
                                                        Side_Cell = 0) AND
                                                        (Board[7, R].
                                                        Attack_Cell=' '))
                                                     THEN
                                                        BEGIN
                                                      Response := 'O-O';
                                                        Reply := TRUE
                                                        END
                                                     ELSE
                                                        BEGIN
                                                      Response := 'O-O-O';
                                                        Reply := TRUE
                                                        END
                                                   END
                                             END
                                       END
                                 END
                              ELSE
                                 BEGIN
                                    WRITELN;
                                    WRITELN ( 'ARE YOU CONCEDING?' );
                                    WRITELN;
                                    Read_Reply( Response );
                                    N := 0;
                                    WHILE ( LENGTH( Response ) = 0 ) DO
                                       BEGIN
                                          WRITELN;
                                          IF ( N < 2 ) THEN
                                             BEGIN
                                                N := N + 1;
                                                WRITE ( 'WHAT DOES ' );
                                                WRITE ( 'THAT MEAN?' )
                                             END
                                          ELSE
                                             BEGIN
                                                N := 0;
                                                WRITE ( 'PLEASE ' );
                                                WRITE ( 'RESPOND.' )
                                             END;
                                          WRITELN;
                                          Read_Reply( Response )
                                       END;
                                    I := Pos( 'N', Response );
                                    IF ( I > 0 ) THEN
                                       BEGIN
                                          WRITELN;
                                          WRITE ( 'WHAT WAS ' );
                                          Pnct := '?'
                                       END
                                    ELSE
                                       BEGIN
                                          IF (COM_SCAN(Response,I,'n'))
                                          THEN
                                             BEGIN
                                                Reply := TRUE
                                             END
                                          ELSE
                                             BEGIN
                                               TermCode := 3;
                                               CD := 'R';
                                               IF ( HardCopy ) THEN
                                                  BEGIN
                                                     IF ( MX = WHITE )
                                                     THEN
                                                        BEGIN
                                                          Produce_Report(
                                                            Machine_Move,
                                                            Void_Move,
                                                                ET, CD )
                                                        END
                                                     ELSE
                                                        BEGIN
                                                          Produce_Report(
                                                               Void_Move,
                                                               Void_Move,
                                                               ET, CD )
                                                        END
                                                  END;
                                               WRITELN;
                                               WRITELN('ACKNOWLEDGED.');
                                               WRITELN;
                                               Done := TRUE;
                                               Finished := TRUE
                                             END
                                       END
                                 END
                           END
                     END
               END
         END;
      IF ((( Plot ) AND NOT( Done )) AND ( NOT( Predicted ) OR
          (( Predicted ) AND ( Tournament )))) THEN
         BEGIN
            WRITELN ( 'THINKING.' );
            WRITELN
         END

   END;

(*********************************************************************)

PROCEDURE Mover( P : POINTER );

   VAR

      I, W, FF, FR, TF, TR, MS, MM, MP, PP, OS, OM, OP : INTEGER;
      Opponent_King, Machine_King, RFF, RTF : INTEGER;
      MST, OST : CHAR;		
      M : POSITION;

   BEGIN

      I := 0;
      W := 0;
      RptScored := Scored;
      RptCutoffs := Cutoffs;
      IF ( NOT( Piece_Table[MX, 1].Status_Cell = 'C' ) ) THEN
         BEGIN
            REPEAT			
		   MST := ' ';			
               I := I + 1;
               RL[P^.MLI[1]] := RL[P^.MLI[I]];
		   M := P^.Move_List[P^.MLI[I]];                          
               FF := M.Move_Cell.From_Cell.File_Cell;
               FR := M.Move_Cell.From_Cell.Rank_Cell;
               IF ( NOT ( FF = 0 ) ) THEN
                  BEGIN
                     P^.Move_List[P^.MLI[1]] := M;
	               MS := Board[FF, FR].Piece_Cell.Side_Cell;
                     MM := Board[FF, FR].Piece_Cell.Man_Cell;
                     MP := Piece_Table[MS, MM].Move_Pattern;
                     Piece_Table[MX, 1].Status_Cell := ' ';
                     P^.IX := 1;       
                     P^.SX := MX;
                     Make( P );
                     Opponent_King := King( P, OX );
			   OST := Piece_Table[OX, 1].Pseudo_Status;	
			   Machine_King := King( NIL, MX );
			   MST := Piece_Table[MX, 1].Pseudo_Status;	
			   IF (( Machine_King IN Check ) OR ( Opponent_King IN StaleMate ))THEN  
                        BEGIN
                           Undo( P )
                        END				
                  END
               ELSE
                  BEGIN
                     WRITELN;
                     WRITELN( '*** MOVER WARNING - FROM MOVE EMPTY ***' );
                     WRITELN( Alarm )
                  END
		UNTIL ( NOT( ( I < P^.WX ) AND ( ( MST = '!' ) OR
				 ( Opponent_King IN StaleMate ) ) ) );

		IF ( NOT( FF = 0 ) ) THEN
               BEGIN
			IF ( MST = '!' ) THEN			
                     BEGIN
                        P^.Move_List[P^.MLI[1]].Mode_Cell := 'E';
                        P^.Move_List[P^.MLI[1]].Score_Cell := -Threshold;
                        Done := TRUE
                     END
                  ELSE
                     BEGIN
                        TF := M.Move_Cell.To_Cell.File_Cell;
                        TR := M.Move_Cell.To_Cell.Rank_Cell;
                        MS := Board[TF, TR].Piece_Cell.Side_Cell;
                        MM := Board[TF, TR].Piece_Cell.Man_Cell;
                        PP := Piece_Table[MS, MM].Move_Pattern;
                        IF ( Report ) THEN                             
                           BEGIN
                              Report_Statistics;
                              IF ( I > 1 ) THEN
                                 BEGIN
                                    WRITELN( 'ATTENTION.' )
                                 END
                           END
                        ELSE
                           BEGIN
                              IF ( Plot ) THEN
                                 BEGIN
                                    WRITELN
                                 END
                           END;
                        IF (( M.Mode_Cell = 'C' ) OR
                            ( P^.LVL.To_Position.Side_Cell > 0 )) THEN
                           BEGIN
                              OS := P^.LVL.To_Position.Side_Cell;
                              OM := P^.LVL.To_Position.Man_Cell;
                              OP := Piece_Table[OS, OM].Move_Pattern;
                              WRITE( 'I SHALL CAPTURE' );
                              IF ( P^.LVL.ENP = 'E' ) THEN
                                 WRITELN( ' EN PASSANT.' )
                              ELSE
                                 WRITELN( '.' )
                           END;
                        IF (( M.Mode_Cell = 'K' ) AND ( MM = 1 )) THEN
                           BEGIN
                              RFF := 8;
                              RTF := 6;
                              WRITELN( 'I SHALL CASTLE, KING SIDE.' )
                           END;
                        IF (( M.Mode_Cell = 'Q' ) AND ( MM = 1 )) THEN
                           BEGIN
                              RFF := 1;
                              RTF := 4;
                              WRITELN( 'I SHALL CASTLE, QUEEN SIDE.' )
                           END;
                        IF ( Show ) THEN
                           BEGIN
                              WRITE( 'I SHALL ' )
                           END;
                        WRITE( 'MOVE MY ', Piece[MP],
                                 ' FROM SQUARE ', File_Vector[FF], FR : 1,
                                 ' TO SQUARE ', File_Vector[TF], TR : 1 );
                        IF ( Show ) THEN
                           BEGIN
                              WRITELN( '.' )
                           END
                        ELSE
                           BEGIN
                              WRITELN( ' PLEASE.' )
                           END;
                        IF (( M.Mode_Cell IN ['K', 'Q'] ) AND
                            ( MM = 1 )) THEN
                           BEGIN
                              IF ( Show ) THEN
                                 BEGIN
                                    WRITE( 'THEN, I SHALL ' )
                                 END;
                              WRITE( 'MOVE MY ROOK FROM SQUARE ',
                                     File_Vector[RFF], FR : 1,
                                     ' TO SQUARE ', File_Vector[RTF],
                                     TR : 1 );
                              IF ( Show ) THEN
                                 BEGIN
                                    WRITELN( '.' )
                                 END
                              ELSE
                                 BEGIN
                                    WRITELN( ' PLEASE.' )
                                 END
                           END;
                        IF (( M.Mode_Cell = 'C' ) OR
                            ( P^.LVL.To_Position.Side_Cell > 0 )) THEN
                           BEGIN
                              WRITE( Piece[MP], ' TAKES ', Piece[OP] );
                              IF (P^.LVL.ENP = 'E') THEN
                                 BEGIN
                                   WRITE( ' ON CELL ' );
                                   WRITELN( File_Vector[TF], FR : 1, '.' )
                                 END
                              ELSE
                                 BEGIN
                                   WRITELN( '.' )
                                 END;
                              IF (( OP = 2 ) AND ( OP <> MP ) AND
                                  ( MP <> 1 ) AND ( Piece_Table[MS, 2].
                                    Status_Cell <> 'C' )) THEN
                                 WRITELN( 'THANK YOU!' )
                           END;
                        IF (( Piece_Table[MS, MM].Pseudo_Status = 'P' ) OR
                            ( M.Mode_Cell = 'P' ) OR
                            ((( TR = 1 ) OR ( TR = 8 )) AND ( MP > 5 )))
                        THEN
                           BEGIN
                              WRITELN( 'PROMOTING PAWN TO A ',
                                        Piece[PP], '.' );
                              Piece_Table[MS, MM].Pseudo_Status := ' '
                           END;
     				Piece_Table[OX, 1].Status_Cell := OST;	
                        IF ( Opponent_King IN CheckMate ) THEN
                           BEGIN
                              TermCode := 1;
                              Move_Cntr := Move_Cntr + 1;
                              CD := 'M';
                              IF (( HardCopy ) AND
                                  ( MX = WHITE )) THEN
                                 BEGIN
                                    Produce_Report( M, Void_Move,
                                                    ET, CD )
                                 END;
                              WRITELN;
                              WRITELN( 'CHECKMATE!' );
                              WRITELN( Alarm );
                              IF ( Move_Cntr < 25 ) THEN
                                 BEGIN
                                    WRITE( 'YOUR GAME NEEDS ' );
                                    WRITELN( 'SOME IMPROVEMENT.' );
                                    WRITELN
                                 END;
                              Done := TRUE
                           END;
                        IF ( Opponent_King IN Check ) THEN
                           BEGIN
                              CD := CHR( MX + ORD('0') );
                              WRITELN;
                              WRITELN( 'CHECK!' );
                              WRITELN
                           END;
                        IF ( Opponent_King IN StaleMate ) THEN
                           BEGIN
                              TermCode := 2;
                              Move_Cntr := Move_Cntr + 1;
                              CD := 'S';
                              IF (( HardCopy ) AND
                                  ( MX = WHITE )) THEN
                                 BEGIN
                                    Produce_Report( M, Void_Move,
                                                    ET, CD )
                                 END;
                              WRITELN;
                              WRITELN( 'STALEMATE.' );
                              WRITELN( Alarm );
                              Done := TRUE
                           END;
                        IF (( Time ) OR ( Tournament )
                           OR ( HardCopy )) THEN
                           BEGIN
                              TIMER('E', HR, MN, SEC, HUND, ET);
                              ElapsedClock := ElapsedClock + ET;
                              IF ( Time ) THEN
                                 BEGIN
                                    WRITE( 'I REQUIRED ' );
                                    IF ( HR > 0 ) THEN
                                       BEGIN
                                          WRITE( HR:1, ' HOUR' );
                                          IF ( HR > 1 ) THEN
                                             WRITE( 'S, ' )
                                          ELSE
                                             WRITE( ', ' )
                                       END;
                                    IF ( MN > 0 ) THEN
                                       BEGIN
                                          WRITE( MN:1, ' MINUTE' );
                                          IF ( MN > 1 ) THEN
                                             WRITE( 'S AND ' )
                                          ELSE
                                             WRITE( ' AND ' )
                                       END;
                                    WRITELN( SEC:1, '.', HUND:1,
                                             ' SECONDS OF THOUGHT TIME');
                                    WRITE( 'TO FORMULATE ');
                                    WRITE('THE FOREGOING ');
                                    WRITELN('MOVE RESPONSE.')
                                 END;
                              Machine_Move := M;
                              IF (( HardCopy ) AND ( MX = BLACK )) THEN
                                 BEGIN
                                    Produce_Report( Machine_Move,
                                                    Opponent_Move,
                                                    ET, CD );
                                    CD := ' '
                                 END
                           END;
                        Move_Cntr := Move_Cntr + 1;
                        IF ( Map ) THEN
                           BEGIN
                              MapToMvArr( M )
                           END;
                        IF ( Show ) THEN
                           BEGIN
                              Display_Board
                           END;
                        IF ( Move_Cntr = 7 ) THEN
                           BEGIN
                              Piece_Table[MX, 2].Development := 2;
                              Piece_Table[OX, 2].Development := 2
                           END;
                        IF ( Move_Cntr = 15 ) THEN
                           BEGIN
                              AI := AI * 2;
                              Piece_Table[MX, 1].Development := 0;
                              Piece_Table[OX, 1].Development := 0
                           END
                     END
               END
            ELSE
               BEGIN
                  P^.Move_List[P^.MLI[1]].Mode_Cell := 'E';
                  Done := TRUE;
                  IF ( Piece_Table[MX, 1].Status_Cell = '!' ) THEN
                     BEGIN
                        P^.Move_List[P^.MLI[1]].Score_Cell := -Threshold
                     END
                  ELSE
                     BEGIN
                        WRITELN;
                        WRITELN( 'I HAVE NO MOVE.' );
                        WRITELN
                     END
               END
         END

   END;


(*********************************************************************)

PROCEDURE Initiate_Dialogue;

   VAR

      I, J, N, S : INTEGER;
      C : CHAR;
      Event : POSITION;

   BEGIN

      IF ( NOT ( Active ) ) THEN
         BEGIN
            Active := TRUE;
            WRITELN;
            WRITELN;
            WRITELN( 'CHESS AUTOMATON CP3 Vr ', Version, ' - HAL ACTIVE.' );
            WRITELN;
            WRITELN;
            WRITELN( 'I RESPOND TO HAL.' );
            WRITELN( 'PLEASE IDENTIFY YOURSELF.' );
            WRITELN;
            Read_Reply( Response );
            WHILE ( Response [ LENGTH( Response ) ] = '.' ) DO
               BEGIN
                  DELETE ( Response, LENGTH( Response ), 1 )
               END;
            Opponent := Response;
            IF (LENGTH(Opponent) > 1) THEN
               BEGIN
                  Opponent := ', '+Opponent
               END;
            Pnct := '.';
            WRITELN;
            WRITELN( 'GREETINGS', Opponent, Pnct );
            WRITELN;
            WRITE( 'PLEASE COMMUNICATE YOUR MOVES IN ' );
            WRITELN( 'STANDARD CHESS NOTATION.' );
            WRITELN;
            WRITE( 'WHITE''S MAJOR PIECES ARE ON ' );
            WRITELN( 'SQUARES A1 THROUGH H1.' );
            WRITELN( 'WHITE''S PAWNS ARE ON SQUARES A2 THROUGH H2.' );
            WRITELN;
            WRITE( 'BLACK''S MAJOR PIECES ARE ON ' );
            WRITELN( 'SQUARES A8 THROUGH H8.' );
            WRITELN( 'BLACK''S PAWNS ARE ON SQUARES A7 THROUGH H7.' )
         END;
      WRITELN;
      WRITELN( 'ARE THERE ANY INSTRUCTIONS?' );
      WRITELN;
      Read_Reply( Response );
      LastReply := Response;
      IF NOT((( Com_Scan( Response, I, 'N' )) AND
         (( Response[I+1] = 'O' ) OR
         ( LENGTH( Response ) = 1 )) AND
         ( LENGTH( Response ) < 9 )) OR
         ( LENGTH( Response ) = 0 )) THEN
         BEGIN
            IF (( Com_Scan( Response, I, 'Y' ) ) AND
               (( Response[I+1] = 'E' ) OR
                ( LENGTH( Response ) = 1 )))
            THEN
               BEGIN
                  WRITELN;
                  WRITELN( 'EXPLAIN.' );
                  WRITELN;
                  Read_Reply( Response )
               END;
            Get_Move( Response, Event );
            IF (( Event.Move_Cell.From_Cell.File_Cell > 0 ) AND
                ( Event.Move_Cell.From_Cell.Rank_Cell > 0 ) AND
                ( Event.Move_Cell.To_Cell.File_Cell > 0 ) AND
                ( Event.Move_Cell.To_Cell.Rank_Cell > 0 )) THEN
               BEGIN
                  S := Board[Event.Move_Cell.From_Cell.File_Cell,
                             Event.Move_Cell.From_Cell.Rank_Cell].
                             Piece_Cell.Side_Cell;
                  IF ( S = Black ) THEN
                     BEGIN
                        WRITELN;
                        WRITE ( 'UNABLE TO COMPLY - ' );
                        WRITELN ( 'WHITE MUST MOVE FIRST.' )
                     END
                  ELSE
                     BEGIN
                        MX := Black;
                        OX := Black
                     END
               END
            ELSE
               BEGIN
                  MX := Black;
                  OX := White;
                  Process_Request ( Response )
               END
         END;
      IF (( Chronical ) AND ( NOT ( Restore ) )) THEN
         BEGIN
            Restore_Game
         END;
      IF (( TermCode = 6 ) OR ( Restore )) THEN
         BEGIN
            P^.SX := MX;
            Restore := FALSE       
         END
      ELSE
         BEGIN
            IF NOT ( MX = OX ) THEN
               BEGIN
                  WRITELN;
                  WRITELN('WHICH PIECES WOULD YOU LIKE: WHITE OR BLACK?');
                  WRITELN;
                  Read_Reply( Response );
                  IF (( Com_Scan( Response, I, 'B' ) ) AND
                      ( Com_Scan( Response, I, 'K' ) )) THEN
                     BEGIN
                        MX := White;
                        OX := Black
                     END
                  ELSE
                     BEGIN
                        MX := Black;
                        OX := White
                     END;
                  WRITELN;
                  WRITELN( 'ACKNOWLEDGED.' );
                  WRITELN;
                  WRITELN( 'YOUR MEN ARE ', MEN[OX], ' AND MINE ARE ',
                           MEN[MX], '.' );
                  WRITELN;
                  WRITELN( 'LET''S PLAY CHESS.' );
                  WRITELN
               END;
            P^.SX := MX;
            IF ( MX = White ) THEN
               BEGIN
                  WRITELN( 'THINKING.' );
                  WRITELN
               END
         END

   END;

(********************************************************************)

(*==================================================================*)
(*              L E V E L   1   A L G O R I T H M S                 *)
(*==================================================================*)

(********************************************************************)

PROCEDURE Initiator( VAR P : POINTER );

   BEGIN

      Create_Root_Node( P );
      Initialize_State_Vectors;
      Reset_Accumulators;
      Clear_Killers;
      Clear_Refutation_List;
      Clear_PC_List;
      Clear_TC_Cells;
      Clear_TD_Cells;
      Initialize_Pieces;
      Build_Piece_Table;
      Generate_Move_Vectors;
      Setup_Board;
      Initiate_Dialogue;
      IF ( HardCopy ) THEN
         BEGIN
            Open_Printer
         END;
      IF (( MX = Black ) OR ( TermCode = 6 )) THEN
         BEGIN
            Retriever( P )
         END

   END;

(********************************************************************)

PROCEDURE Processor( VAR P : POINTER );

   BEGIN

      WHILE NOT Done DO
         BEGIN
            Move_Selector( P );
            IF ( NOT( Done ) ) THEN
               BEGIN
                  Mover( P )
               END;
            IF ( NOT( Done ) ) THEN
               BEGIN
                  Retriever( P )
               END
         END

   END;

(********************************************************************)

PROCEDURE Terminator( VAR P : POINTER );

   VAR

      I : INTEGER;

   BEGIN

      MSG := '';
      Move := P^.Move_list[P^.MLI[1]];
      IF ( Move.Mode_Cell = 'E' ) THEN
         BEGIN
            CD := 'M';
            MSG := 'YOU HAVE PLACED ME IN CHECKMATE.'+Alarm;
            IF NOT( Piece_Table[MX, 1].Status_Cell = '!' ) THEN
               BEGIN
                  P^.SX := OX;
                  I := Generate( P );
                  P^.SX := MX;
                  Piece_Table[MX, 1].Status_Cell :=
                  Piece_Table[MX, 1].Pseudo_Status;
                  IF NOT( Piece_Table[MX, 1].Status_Cell = '!' ) THEN
                     BEGIN
                        CD := 'S';
                        MSG := 'STALEMATE.'+Alarm
                     END
               END;
            IF (( HardCopy ) AND ( MX = BLACK )) THEN
               BEGIN
                  Produce_Report( Void_Move, Opponent_Move, ET, CD )
               END
         END;
      IF ( LENGTH( Msg ) > 0 ) THEN
         BEGIN
            WRITELN;
            WRITELN ( Msg );
            WRITELN
         END;
      IF ( HardCopy ) THEN
         BEGIN
            EOP
         END;
      Drop( P );
      IF (( Move_Cntr > 4 ) OR ( Move.Mode_Cell = 'E' )) THEN
         BEGIN
            WRITE ('THANK YOU FOR A ');
            IF ( Move_Cntr > 14 ) THEN
               BEGIN
                  WRITE ('VERY ')
               END
            ELSE
               BEGIN
                  WRITE ('BRIEF, BUT ')
               END;
            WRITELN ('ENJOYABLE GAME.');
            WRITELN;
            IF ( NOT ( Save ) ) THEN
               BEGIN
                  WRITELN ('WOULD YOU CARE FOR ANOTHER? ');
                  WRITELN;
                  Read_Reply ( Response );
                  WRITELN;
                  WRITELN('ACKNOWLEDGED.');
                  WRITELN
               END;
            IF (( Save ) OR ( Com_Scan ( Response, I, 'N' ) )) THEN
               BEGIN
                  WRITELN ('PERHAPS WE SHALL PLAY AGAIN.');
                  WRITELN;
                  WRITELN;
                  IF (( Chronical ) OR ( Save )) THEN
                     BEGIN
                        IF (( CD IN [ 'D', 'M', 'R', 'S', 'T' ] ) AND
                             ( NOT ( TermCode IN [ 1 .. 4 ] ) )) THEN
                           BEGIN

                              CASE CD OF

                                   'D' : BEGIN
                                            TermCode := 4
                                         END;
                                   'M' : BEGIN
                                            TermCode := 1
                                         END;
                                   'R' : BEGIN
                                            TermCode := 3
                                         END;
                                   'S' : BEGIN
                                            TermCode := 2
                                         END;
                                   'T' : BEGIN
                                            TermCode := 0
                                         END

                              END

                           END;

                        IF ((( TermCode IN [ 0, 5 ] ) AND
                             ( Move_Cntr > 0 )) OR (ErrorFlag)) THEN
                           BEGIN
                              TermCode := 0;
                              Save_Game    
                           END;
                        WRITELN;
                        WRITELN
                     END;
			WRITE ('CHESS AUTOMATON CP3 Vr ');
                  WRITELN (Version, ' - HAL CLEAR.');
                  WRITELN;
                  WRITELN
               END
            ELSE
               BEGIN
                  Done := False
               END
         END
      ELSE
         BEGIN
            IF (( Save ) AND ( Move_Cntr > 0 )) THEN
               BEGIN
                  TermCode := 0;
                  Save_Game
               END;
            WRITELN;
            WRITELN ('CHESS AUTOMATON CP3V', Version, ' - HAL CLEAR.');
            WRITELN;
            WRITELN
         END;
      IF ( HardCopy ) THEN
         BEGIN
            Close_Printer
         END

   END;

(********************************************************************)

(*==================================================================*)
(*              L E V E L   0   A L G O R I T H M S                 *)
(*==================================================================*)

(********************************************************************)

BEGIN               (* Executive Nucleus *)

   Initialize_Boolean;
   REPEAT
      Initiator( P );
      Processor( P );
      Terminator( P )
   UNTIL Done

END.

(********************************************************************)

(*==================================================================*)
(*              E N D   O F   P R O G R A M                         *)
(*==================================================================*)
