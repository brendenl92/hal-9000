/*
(*                                                                  *)
(********************************************************************)
(*                                                                  *)
(*                             H A L                                *)
(*                                                                  *)
(*             Heuristic Associative Linear-algorithm               *)
(*             -         -           -                              *)
(*                                                                  *)
(*==================================================================*)
(*                                                                  *)
(*                  C H E S S   P R O G R A M   3                   *)
(*                  -           -               -                   *)
(********************************************************************)
(*                                                                  *)
(*------------------------------------------------------------------*)
(*                                                                  *)
(* Author:  Stephen F. Wheeler, Ph.D.   A.I. Computer Scientist     *)
(*                                                                  *)
(* Language:  Turbo Pascal v6.0                                     *)
(*                                                                  *)
(*------------------------------------------------------------------*)
(*                                                                  *)
(* Translated by: Brenden Lyons                                     *)
(*                                                                  *)
(* Language: C++                                                    *)
(*                                                                  *)
(*------------------------------------------------------------------*)
(*                                                                  *)
(* Date Written:                September 8, 1990                   *)
(*                                                                  *)
(* Date of Last Revision:       August 25, 2014                     *)
(*                                                                  *)
(* Revisions in This Version:                                       *)
(*                                                                  *)
(*  1) Translated to C++                                            *)
(*                                                                  *)
(*                                                                  *)
(*------------------------------------------------------------------*)
(*                  C P 3 V 8 . 9 . 9 . 7 . 2                       *)
(*------------------------------------------------------------------*)
(*                                                                  *)
(*==================================================================*)
(*                                                                  *)
(*        C O M P U T E R   C H E S S   A U T O M A T O N           *)
(*                                                                  *)
(*    THIS PROGRAM UTILIZES THE ITERATIVE-DEEPENING NEGAMAX         *)
(*    FAILSOFT ALPHA-BETA SEARCH ALGORITHM WITH QUIESCENCE, THE     *)
(*    KILLER HEURISTIC AND REFUTATION TABLES.                       *)
(*                                                                  *)
(*==================================================================*)
(*                                                                  *)
(********************************************************************)
(*                                                                  *)
(*              P R O G R A M   D E C L A R A T I V E S             *)
(*                                                                  *)
(*                       B E G I N    H E R E                       *)
(*                                                                  *)
(********************************************************************)
*/
 
#include <string>
#include <iostream>

using namespace std;

/*
(********************************************************************)
(*              P R O G R A M   C O N S T A N T S                   *)
(********************************************************************)
*/

string Version = "8.9.9.7.2";

unsigned int Pawn = 128;               //(*Pawn Heuristic Weight *)
unsigned int White = 1;                //(*Side Index for White *)
unsigned int Black = 2;                // (*Side Index for Black *)
unsigned int Default_Level = 2;        //(*Default Skill Level *)
unsigned int FW_Limit = 3;             //(*Full Width Search Limit *)
unsigned int QS_Limit = 8;             //(*Quiescent Search Depth Limit *)
unsigned int MaxSearch = 20;           //(*Maximum Non_Tournament Search Limit *)
unsigned int MaxDepth = 30;            //(*Maximum Tree Size *)
unsigned int MaxBreadth = 100;         //(*Maximum Number of Elements in each Ply of Tree *)
unsigned int Check_Extension = 2;      //(*Search Extension Limit for In_Check Condition *)
unsigned int Killers = 4;              //(*Number of Killers at each Level of Active List *)
unsigned int Killer_Value = 2;         //(*Killer Initiation Value *)
unsigned int Killer_Augment = 3;       //(*Killer Statistical Augmentation Value *)
unsigned int Killer_Decay = 1;         //(*Killer Statistical Decay Value *)
unsigned int Decay_Interval = 2;       //(*Decay Interval for a - b Window Fail Control *)
unsigned int INF = INT_MAX;            //(*Pseudo Infinity *)
unsigned int High_Value = 19999;       //(*High Value *)
unsigned int Very_High_Value = 25555;  //(*Short Pseudo Infinity *)
unsigned int VHV = Very_High_Value;    //(*Short Pseudo Infinity *)
unsigned int Marker1 = 21111;          //(*Pseudo Infinity Marker Value *)
unsigned int Marker2 = 22222;          //(*Pseudo Infinity Marker Value *)
unsigned int Marker3 = 23333;          //(*Pseudo Infinity Marker Value *)
unsigned int Marker4 = 24444;          //(*Pseudo Infinity Marker Value *)
unsigned int Marker5 = 25555;          //(*Pseudo Infinity Marker Value *)
unsigned int Marker6 = 26666;          //(*Pseudo Infinity Marker Value *)
unsigned int Marker7 = 27777;          //(*Pseudo Infinity Marker Value *)
unsigned int Marker8 = 28888;          //(*Pseudo Infinity Marker Value *)
unsigned int Marker9 = 29999;          //(*Pseudo Infinity Marker Value *)
unsigned int MaxLines = 56;            //(*Maximum number of lines per page *)
unsigned int Alarm = #7;               //(*Audible alarm code *)
unsigned int LineFeed = #10;           //(*Line feed printer control code *)
unsigned int FormFeed = #12;           //(*Form feed printer control code *)
unsigned int Retry_Limit = 2;          //(*I / O Error Retry Limit *)

string Printer = "PRN";                //(*System logical print device *)

string DrvPth = "CHESS";               // (* System logical drive and path *)

string LogDrv = "LOG.CP3";             //  (*Game move history file *)

string StatDrv = "STAT.CP3";           //  (*Game statistics file *)

string ConfigDrv = "CONFIG.CP3";       // (*System configuration file *)

typedef string STR80[80];
typedef string STR40[40];
typedef string STR20[20];
typedef string STR10[10];
typedef string STR9[9];
typedef string STR6[6];
typedef string STR5[5];
typedef string STR4[4];
typedef string STR3[3];
typedef string STR2[2];
typedef string STR1[1];